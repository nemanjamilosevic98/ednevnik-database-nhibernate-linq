﻿namespace eDnevnik
{
    partial class FormaRazredniPocetna
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormaRazredniPocetna));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            this.panelPrijavljeniNastavnik = new System.Windows.Forms.Panel();
            this.button1 = new System.Windows.Forms.Button();
            this.labelImePrezimeRazrednog = new System.Windows.Forms.Label();
            this.txt1 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btnRasporedCasova = new System.Windows.Forms.Button();
            this.btnPregledOcena = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.btnOpravdajCasove = new System.Windows.Forms.Button();
            this.numericUpDown1 = new System.Windows.Forms.NumericUpDown();
            this.btnIzmeniUcenika = new System.Windows.Forms.Button();
            this.btnUkloniUcenika = new System.Windows.Forms.Button();
            this.btnDodajUcenika = new System.Windows.Forms.Button();
            this.labelMojeOdeljenjeSmer = new System.Windows.Forms.Label();
            this.labelMojeOdeljenjeNaziv = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.dataGridMojiUcenici = new System.Windows.Forms.DataGridView();
            this.groupBoxOcene = new System.Windows.Forms.GroupBox();
            this.labelOceneUcenikPrezime = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.labelOceneUcenikIme = new System.Windows.Forms.Label();
            this.dataGridOcene = new System.Windows.Forms.DataGridView();
            this.Column7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panelPrijavljeniNastavnik.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridMojiUcenici)).BeginInit();
            this.groupBoxOcene.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridOcene)).BeginInit();
            this.SuspendLayout();
            // 
            // panelPrijavljeniNastavnik
            // 
            this.panelPrijavljeniNastavnik.Controls.Add(this.button1);
            this.panelPrijavljeniNastavnik.Controls.Add(this.labelImePrezimeRazrednog);
            this.panelPrijavljeniNastavnik.Controls.Add(this.txt1);
            this.panelPrijavljeniNastavnik.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelPrijavljeniNastavnik.Location = new System.Drawing.Point(0, 0);
            this.panelPrijavljeniNastavnik.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.panelPrijavljeniNastavnik.Name = "panelPrijavljeniNastavnik";
            this.panelPrijavljeniNastavnik.Size = new System.Drawing.Size(938, 47);
            this.panelPrijavljeniNastavnik.TabIndex = 5;
            // 
            // button1
            // 
            this.button1.Dock = System.Windows.Forms.DockStyle.Right;
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.ForeColor = System.Drawing.SystemColors.Highlight;
            this.button1.Image = ((System.Drawing.Image)(resources.GetObject("button1.Image")));
            this.button1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button1.Location = new System.Drawing.Point(808, 0);
            this.button1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(130, 47);
            this.button1.TabIndex = 9;
            this.button1.Text = "     Odjava";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // labelImePrezimeRazrednog
            // 
            this.labelImePrezimeRazrednog.AutoSize = true;
            this.labelImePrezimeRazrednog.BackColor = System.Drawing.Color.Transparent;
            this.labelImePrezimeRazrednog.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelImePrezimeRazrednog.ForeColor = System.Drawing.Color.MidnightBlue;
            this.labelImePrezimeRazrednog.Location = new System.Drawing.Point(171, 15);
            this.labelImePrezimeRazrednog.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelImePrezimeRazrednog.Name = "labelImePrezimeRazrednog";
            this.labelImePrezimeRazrednog.Size = new System.Drawing.Size(91, 17);
            this.labelImePrezimeRazrednog.TabIndex = 3;
            this.labelImePrezimeRazrednog.Text = "ImePrezime";
            // 
            // txt1
            // 
            this.txt1.AutoSize = true;
            this.txt1.BackColor = System.Drawing.Color.Transparent;
            this.txt1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt1.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.txt1.Location = new System.Drawing.Point(2, 15);
            this.txt1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.txt1.Name = "txt1";
            this.txt1.Size = new System.Drawing.Size(150, 17);
            this.txt1.TabIndex = 2;
            this.txt1.Text = "Razredni starešina:";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.btnRasporedCasova);
            this.groupBox2.Controls.Add(this.btnPregledOcena);
            this.groupBox2.Controls.Add(this.groupBox1);
            this.groupBox2.Controls.Add(this.btnIzmeniUcenika);
            this.groupBox2.Controls.Add(this.btnUkloniUcenika);
            this.groupBox2.Controls.Add(this.btnDodajUcenika);
            this.groupBox2.Controls.Add(this.labelMojeOdeljenjeSmer);
            this.groupBox2.Controls.Add(this.labelMojeOdeljenjeNaziv);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Controls.Add(this.label12);
            this.groupBox2.Controls.Add(this.dataGridMojiUcenici);
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.ForeColor = System.Drawing.Color.MidnightBlue;
            this.groupBox2.Location = new System.Drawing.Point(27, 62);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupBox2.Size = new System.Drawing.Size(884, 327);
            this.groupBox2.TabIndex = 12;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Moje odeljenje";
            // 
            // btnRasporedCasova
            // 
            this.btnRasporedCasova.BackColor = System.Drawing.Color.PowderBlue;
            this.btnRasporedCasova.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRasporedCasova.ForeColor = System.Drawing.Color.MidnightBlue;
            this.btnRasporedCasova.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnRasporedCasova.Location = new System.Drawing.Point(712, 269);
            this.btnRasporedCasova.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnRasporedCasova.Name = "btnRasporedCasova";
            this.btnRasporedCasova.Size = new System.Drawing.Size(134, 40);
            this.btnRasporedCasova.TabIndex = 39;
            this.btnRasporedCasova.Text = "Raspored   časova";
            this.btnRasporedCasova.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnRasporedCasova.UseVisualStyleBackColor = false;
            this.btnRasporedCasova.Click += new System.EventHandler(this.btnRasporedCasova_Click);
            // 
            // btnPregledOcena
            // 
            this.btnPregledOcena.BackColor = System.Drawing.Color.PowderBlue;
            this.btnPregledOcena.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPregledOcena.ForeColor = System.Drawing.Color.MidnightBlue;
            this.btnPregledOcena.Location = new System.Drawing.Point(517, 269);
            this.btnPregledOcena.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnPregledOcena.Name = "btnPregledOcena";
            this.btnPregledOcena.Size = new System.Drawing.Size(131, 44);
            this.btnPregledOcena.TabIndex = 38;
            this.btnPregledOcena.Text = "Pregled ocena učenika";
            this.btnPregledOcena.UseVisualStyleBackColor = false;
            this.btnPregledOcena.Click += new System.EventHandler(this.btnPregledOcena_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.btnOpravdajCasove);
            this.groupBox1.Controls.Add(this.numericUpDown1);
            this.groupBox1.ForeColor = System.Drawing.Color.MidnightBlue;
            this.groupBox1.Location = new System.Drawing.Point(698, 46);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupBox1.Size = new System.Drawing.Size(169, 198);
            this.groupBox1.TabIndex = 37;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Pravdanje časova";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.MidnightBlue;
            this.label2.Location = new System.Drawing.Point(11, 51);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(155, 13);
            this.label2.TabIndex = 38;
            this.label2.Text = "Broj časova za pravdanje:";
            // 
            // btnOpravdajCasove
            // 
            this.btnOpravdajCasove.BackColor = System.Drawing.Color.PowderBlue;
            this.btnOpravdajCasove.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnOpravdajCasove.ForeColor = System.Drawing.Color.MidnightBlue;
            this.btnOpravdajCasove.Location = new System.Drawing.Point(35, 143);
            this.btnOpravdajCasove.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnOpravdajCasove.Name = "btnOpravdajCasove";
            this.btnOpravdajCasove.Size = new System.Drawing.Size(113, 28);
            this.btnOpravdajCasove.TabIndex = 38;
            this.btnOpravdajCasove.Text = "Opravdaj časove";
            this.btnOpravdajCasove.UseVisualStyleBackColor = false;
            this.btnOpravdajCasove.Click += new System.EventHandler(this.btnOpravdajCasove_Click);
            // 
            // numericUpDown1
            // 
            this.numericUpDown1.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.numericUpDown1.ForeColor = System.Drawing.SystemColors.Highlight;
            this.numericUpDown1.Location = new System.Drawing.Point(46, 94);
            this.numericUpDown1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.numericUpDown1.Name = "numericUpDown1";
            this.numericUpDown1.Size = new System.Drawing.Size(90, 19);
            this.numericUpDown1.TabIndex = 0;
            this.numericUpDown1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // btnIzmeniUcenika
            // 
            this.btnIzmeniUcenika.BackColor = System.Drawing.Color.PowderBlue;
            this.btnIzmeniUcenika.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnIzmeniUcenika.ForeColor = System.Drawing.Color.MidnightBlue;
            this.btnIzmeniUcenika.Location = new System.Drawing.Point(351, 280);
            this.btnIzmeniUcenika.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnIzmeniUcenika.Name = "btnIzmeniUcenika";
            this.btnIzmeniUcenika.Size = new System.Drawing.Size(131, 33);
            this.btnIzmeniUcenika.TabIndex = 36;
            this.btnIzmeniUcenika.Text = "Izmeni učenika";
            this.btnIzmeniUcenika.UseVisualStyleBackColor = false;
            this.btnIzmeniUcenika.Click += new System.EventHandler(this.btnIzmeniUcenika_Click);
            // 
            // btnUkloniUcenika
            // 
            this.btnUkloniUcenika.BackColor = System.Drawing.Color.PowderBlue;
            this.btnUkloniUcenika.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUkloniUcenika.ForeColor = System.Drawing.Color.MidnightBlue;
            this.btnUkloniUcenika.Location = new System.Drawing.Point(189, 280);
            this.btnUkloniUcenika.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnUkloniUcenika.Name = "btnUkloniUcenika";
            this.btnUkloniUcenika.Size = new System.Drawing.Size(131, 33);
            this.btnUkloniUcenika.TabIndex = 35;
            this.btnUkloniUcenika.Text = "Ukloni učenika";
            this.btnUkloniUcenika.UseVisualStyleBackColor = false;
            this.btnUkloniUcenika.Click += new System.EventHandler(this.btnUkloniUcenika_Click);
            // 
            // btnDodajUcenika
            // 
            this.btnDodajUcenika.BackColor = System.Drawing.Color.PowderBlue;
            this.btnDodajUcenika.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDodajUcenika.ForeColor = System.Drawing.Color.MidnightBlue;
            this.btnDodajUcenika.Location = new System.Drawing.Point(26, 280);
            this.btnDodajUcenika.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnDodajUcenika.Name = "btnDodajUcenika";
            this.btnDodajUcenika.Size = new System.Drawing.Size(131, 33);
            this.btnDodajUcenika.TabIndex = 31;
            this.btnDodajUcenika.Text = "Dodaj učenika";
            this.btnDodajUcenika.UseVisualStyleBackColor = false;
            this.btnDodajUcenika.Click += new System.EventHandler(this.btnDodajUcenika_Click);
            // 
            // labelMojeOdeljenjeSmer
            // 
            this.labelMojeOdeljenjeSmer.AutoSize = true;
            this.labelMojeOdeljenjeSmer.BackColor = System.Drawing.Color.Transparent;
            this.labelMojeOdeljenjeSmer.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelMojeOdeljenjeSmer.ForeColor = System.Drawing.SystemColors.Highlight;
            this.labelMojeOdeljenjeSmer.Location = new System.Drawing.Point(306, 30);
            this.labelMojeOdeljenjeSmer.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelMojeOdeljenjeSmer.Name = "labelMojeOdeljenjeSmer";
            this.labelMojeOdeljenjeSmer.Size = new System.Drawing.Size(33, 13);
            this.labelMojeOdeljenjeSmer.TabIndex = 34;
            this.labelMojeOdeljenjeSmer.Text = "smer";
            // 
            // labelMojeOdeljenjeNaziv
            // 
            this.labelMojeOdeljenjeNaziv.AutoSize = true;
            this.labelMojeOdeljenjeNaziv.BackColor = System.Drawing.Color.Transparent;
            this.labelMojeOdeljenjeNaziv.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelMojeOdeljenjeNaziv.ForeColor = System.Drawing.SystemColors.Highlight;
            this.labelMojeOdeljenjeNaziv.Location = new System.Drawing.Point(105, 30);
            this.labelMojeOdeljenjeNaziv.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelMojeOdeljenjeNaziv.Name = "labelMojeOdeljenjeNaziv";
            this.labelMojeOdeljenjeNaziv.Size = new System.Drawing.Size(34, 13);
            this.labelMojeOdeljenjeNaziv.TabIndex = 33;
            this.labelMojeOdeljenjeNaziv.Text = "odelj";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.MidnightBlue;
            this.label1.Location = new System.Drawing.Point(241, 30);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(39, 13);
            this.label1.TabIndex = 32;
            this.label1.Text = "Smer:";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.Color.Transparent;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.MidnightBlue;
            this.label12.Location = new System.Drawing.Point(24, 30);
            this.label12.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(64, 13);
            this.label12.TabIndex = 31;
            this.label12.Text = "Odeljenje:";
            // 
            // dataGridMojiUcenici
            // 
            this.dataGridMojiUcenici.BackgroundColor = System.Drawing.Color.White;
            this.dataGridMojiUcenici.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.MidnightBlue;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridMojiUcenici.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle5;
            this.dataGridMojiUcenici.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridMojiUcenici.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column2,
            this.Column3,
            this.Column4,
            this.Column5,
            this.Column6});
            this.dataGridMojiUcenici.EnableHeadersVisualStyles = false;
            this.dataGridMojiUcenici.GridColor = System.Drawing.Color.MediumBlue;
            this.dataGridMojiUcenici.Location = new System.Drawing.Point(4, 60);
            this.dataGridMojiUcenici.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.dataGridMojiUcenici.Name = "dataGridMojiUcenici";
            this.dataGridMojiUcenici.ReadOnly = true;
            this.dataGridMojiUcenici.RowHeadersVisible = false;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.ControlLight;
            dataGridViewCellStyle6.ForeColor = System.Drawing.Color.MidnightBlue;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.Color.LightCyan;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.Color.MidnightBlue;
            this.dataGridMojiUcenici.RowsDefaultCellStyle = dataGridViewCellStyle6;
            this.dataGridMojiUcenici.RowTemplate.Height = 24;
            this.dataGridMojiUcenici.Size = new System.Drawing.Size(674, 184);
            this.dataGridMojiUcenici.TabIndex = 30;
            this.dataGridMojiUcenici.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridMojiUcenici_CellClick);
            this.dataGridMojiUcenici.SelectionChanged += new System.EventHandler(this.dataGridMojiUcenici_SelectionChanged);
            // 
            // groupBoxOcene
            // 
            this.groupBoxOcene.Controls.Add(this.labelOceneUcenikPrezime);
            this.groupBoxOcene.Controls.Add(this.label3);
            this.groupBoxOcene.Controls.Add(this.label4);
            this.groupBoxOcene.Controls.Add(this.labelOceneUcenikIme);
            this.groupBoxOcene.Controls.Add(this.dataGridOcene);
            this.groupBoxOcene.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBoxOcene.ForeColor = System.Drawing.Color.MidnightBlue;
            this.groupBoxOcene.Location = new System.Drawing.Point(27, 404);
            this.groupBoxOcene.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupBoxOcene.Name = "groupBoxOcene";
            this.groupBoxOcene.Padding = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupBoxOcene.Size = new System.Drawing.Size(700, 219);
            this.groupBoxOcene.TabIndex = 34;
            this.groupBoxOcene.TabStop = false;
            this.groupBoxOcene.Text = "Ocene";
            // 
            // labelOceneUcenikPrezime
            // 
            this.labelOceneUcenikPrezime.AutoSize = true;
            this.labelOceneUcenikPrezime.BackColor = System.Drawing.Color.Transparent;
            this.labelOceneUcenikPrezime.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelOceneUcenikPrezime.ForeColor = System.Drawing.Color.MidnightBlue;
            this.labelOceneUcenikPrezime.Location = new System.Drawing.Point(62, 134);
            this.labelOceneUcenikPrezime.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelOceneUcenikPrezime.Name = "labelOceneUcenikPrezime";
            this.labelOceneUcenikPrezime.Size = new System.Drawing.Size(65, 17);
            this.labelOceneUcenikPrezime.TabIndex = 32;
            this.labelOceneUcenikPrezime.Text = "prezime";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label3.Location = new System.Drawing.Point(33, 105);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(132, 17);
            this.label3.TabIndex = 31;
            this.label3.Text = "Prezime učenika:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label4.Location = new System.Drawing.Point(46, 37);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(90, 17);
            this.label4.TabIndex = 10;
            this.label4.Text = "Ime učenik:";
            // 
            // labelOceneUcenikIme
            // 
            this.labelOceneUcenikIme.AutoSize = true;
            this.labelOceneUcenikIme.BackColor = System.Drawing.Color.Transparent;
            this.labelOceneUcenikIme.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelOceneUcenikIme.ForeColor = System.Drawing.Color.MidnightBlue;
            this.labelOceneUcenikIme.Location = new System.Drawing.Point(62, 67);
            this.labelOceneUcenikIme.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelOceneUcenikIme.Name = "labelOceneUcenikIme";
            this.labelOceneUcenikIme.Size = new System.Drawing.Size(33, 17);
            this.labelOceneUcenikIme.TabIndex = 10;
            this.labelOceneUcenikIme.Text = "ime";
            // 
            // dataGridOcene
            // 
            this.dataGridOcene.BackgroundColor = System.Drawing.Color.White;
            this.dataGridOcene.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle7.BackColor = System.Drawing.Color.MidnightBlue;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle7.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridOcene.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle7;
            this.dataGridOcene.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridOcene.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column7,
            this.Column8,
            this.Column9,
            this.Column10});
            this.dataGridOcene.EnableHeadersVisualStyles = false;
            this.dataGridOcene.GridColor = System.Drawing.Color.MediumBlue;
            this.dataGridOcene.Location = new System.Drawing.Point(196, 17);
            this.dataGridOcene.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.dataGridOcene.Name = "dataGridOcene";
            this.dataGridOcene.ReadOnly = true;
            this.dataGridOcene.RowHeadersVisible = false;
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle8.BackColor = System.Drawing.SystemColors.ControlLight;
            dataGridViewCellStyle8.ForeColor = System.Drawing.Color.MidnightBlue;
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.Color.LightCyan;
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.Color.MidnightBlue;
            this.dataGridOcene.RowsDefaultCellStyle = dataGridViewCellStyle8;
            this.dataGridOcene.RowTemplate.Height = 24;
            this.dataGridOcene.Size = new System.Drawing.Size(482, 187);
            this.dataGridOcene.TabIndex = 30;
            // 
            // Column7
            // 
            this.Column7.HeaderText = "ID Ocene";
            this.Column7.Name = "Column7";
            this.Column7.ReadOnly = true;
            this.Column7.Width = 130;
            // 
            // Column8
            // 
            this.Column8.HeaderText = "Predmet";
            this.Column8.Name = "Column8";
            this.Column8.ReadOnly = true;
            // 
            // Column9
            // 
            this.Column9.HeaderText = "Tip ocene";
            this.Column9.Name = "Column9";
            this.Column9.ReadOnly = true;
            this.Column9.Width = 130;
            // 
            // Column10
            // 
            this.Column10.HeaderText = "Ocena";
            this.Column10.Name = "Column10";
            this.Column10.ReadOnly = true;
            // 
            // Column1
            // 
            this.Column1.HeaderText = "ID Ucenika";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            this.Column1.Width = 110;
            // 
            // Column2
            // 
            this.Column2.HeaderText = "Ime";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            // 
            // Column3
            // 
            this.Column3.HeaderText = "Prezime";
            this.Column3.Name = "Column3";
            this.Column3.ReadOnly = true;
            // 
            // Column4
            // 
            this.Column4.HeaderText = "Opravdani";
            this.Column4.Name = "Column4";
            this.Column4.ReadOnly = true;
            // 
            // Column5
            // 
            this.Column5.HeaderText = "Neopravdani";
            this.Column5.Name = "Column5";
            this.Column5.ReadOnly = true;
            // 
            // Column6
            // 
            this.Column6.HeaderText = "Ocena iz vladanja";
            this.Column6.Name = "Column6";
            this.Column6.ReadOnly = true;
            this.Column6.Width = 140;
            // 
            // FormaRazredniPocetna
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(938, 632);
            this.Controls.Add(this.groupBoxOcene);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.panelPrijavljeniNastavnik);
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Name = "FormaRazredniPocetna";
            this.Text = "e-Dnevnik";
            this.Load += new System.EventHandler(this.FormaRazredniPocetna_Load);
            this.panelPrijavljeniNastavnik.ResumeLayout(false);
            this.panelPrijavljeniNastavnik.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridMojiUcenici)).EndInit();
            this.groupBoxOcene.ResumeLayout(false);
            this.groupBoxOcene.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridOcene)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelPrijavljeniNastavnik;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label labelImePrezimeRazrednog;
        private System.Windows.Forms.Label txt1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.DataGridView dataGridMojiUcenici;
        private System.Windows.Forms.Button btnDodajUcenika;
        private System.Windows.Forms.Label labelMojeOdeljenjeSmer;
        private System.Windows.Forms.Label labelMojeOdeljenjeNaziv;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Button btnIzmeniUcenika;
        private System.Windows.Forms.Button btnUkloniUcenika;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnOpravdajCasove;
        private System.Windows.Forms.NumericUpDown numericUpDown1;
        private System.Windows.Forms.Button btnPregledOcena;
        private System.Windows.Forms.GroupBox groupBoxOcene;
        private System.Windows.Forms.Label labelOceneUcenikPrezime;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label labelOceneUcenikIme;
        private System.Windows.Forms.DataGridView dataGridOcene;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column7;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column8;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column9;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column10;
        private System.Windows.Forms.Button btnRasporedCasova;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column6;
    }
}