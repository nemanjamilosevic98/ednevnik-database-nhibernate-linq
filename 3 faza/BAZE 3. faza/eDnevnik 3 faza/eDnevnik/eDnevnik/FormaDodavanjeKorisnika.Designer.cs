﻿namespace eDnevnik
{
    partial class FormaDodavanjeKorisnika
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtImePrezimeAdministratora = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txtIme = new System.Windows.Forms.TextBox();
            this.txtPrezime = new System.Windows.Forms.TextBox();
            this.txtPol = new System.Windows.Forms.TextBox();
            this.txtJmbg = new System.Windows.Forms.TextBox();
            this.txtUsername = new System.Windows.Forms.TextBox();
            this.txtPassword = new System.Windows.Forms.TextBox();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.checkBoxUcenik = new System.Windows.Forms.CheckBox();
            this.checkBoxRoditelj = new System.Windows.Forms.CheckBox();
            this.checkBoxNastavnik = new System.Windows.Forms.CheckBox();
            this.checkBoxAdmin = new System.Windows.Forms.CheckBox();
            this.panelNastavnik = new System.Windows.Forms.Panel();
            this.txtsss = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.panelUcenika = new System.Windows.Forms.Panel();
            this.txtVladanje = new System.Windows.Forms.TextBox();
            this.txtNeopravdani = new System.Windows.Forms.TextBox();
            this.txtOpravdani = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.comboBoxOdeljenjeUcenika = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.button = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.panelNastavnik.SuspendLayout();
            this.panelUcenika.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtImePrezimeAdministratora
            // 
            this.txtImePrezimeAdministratora.AutoSize = true;
            this.txtImePrezimeAdministratora.BackColor = System.Drawing.Color.Transparent;
            this.txtImePrezimeAdministratora.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtImePrezimeAdministratora.ForeColor = System.Drawing.Color.MidnightBlue;
            this.txtImePrezimeAdministratora.Location = new System.Drawing.Point(30, 37);
            this.txtImePrezimeAdministratora.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.txtImePrezimeAdministratora.Name = "txtImePrezimeAdministratora";
            this.txtImePrezimeAdministratora.Size = new System.Drawing.Size(33, 17);
            this.txtImePrezimeAdministratora.TabIndex = 4;
            this.txtImePrezimeAdministratora.Text = "Ime";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.MidnightBlue;
            this.label1.Location = new System.Drawing.Point(30, 84);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(66, 17);
            this.label1.TabIndex = 5;
            this.label1.Text = "Prezime";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.MidnightBlue;
            this.label2.Location = new System.Drawing.Point(30, 134);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(118, 17);
            this.label2.TabIndex = 6;
            this.label2.Text = "Datum rodjenja";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.MidnightBlue;
            this.label3.Location = new System.Drawing.Point(30, 180);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(75, 17);
            this.label3.TabIndex = 7;
            this.label3.Text = "Pol (M/Z)";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.MidnightBlue;
            this.label4.Location = new System.Drawing.Point(30, 234);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(50, 17);
            this.label4.TabIndex = 8;
            this.label4.Text = "JMBG";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.MidnightBlue;
            this.label5.Location = new System.Drawing.Point(30, 289);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(81, 17);
            this.label5.TabIndex = 9;
            this.label5.Text = "Username";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.MidnightBlue;
            this.label6.Location = new System.Drawing.Point(30, 335);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(77, 17);
            this.label6.TabIndex = 10;
            this.label6.Text = "Password";
            // 
            // txtIme
            // 
            this.txtIme.Location = new System.Drawing.Point(155, 37);
            this.txtIme.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.txtIme.Name = "txtIme";
            this.txtIme.Size = new System.Drawing.Size(117, 20);
            this.txtIme.TabIndex = 11;
            // 
            // txtPrezime
            // 
            this.txtPrezime.Location = new System.Drawing.Point(155, 81);
            this.txtPrezime.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.txtPrezime.Name = "txtPrezime";
            this.txtPrezime.Size = new System.Drawing.Size(117, 20);
            this.txtPrezime.TabIndex = 12;
            // 
            // txtPol
            // 
            this.txtPol.Location = new System.Drawing.Point(155, 177);
            this.txtPol.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.txtPol.MaxLength = 1;
            this.txtPol.Name = "txtPol";
            this.txtPol.Size = new System.Drawing.Size(117, 20);
            this.txtPol.TabIndex = 13;
            // 
            // txtJmbg
            // 
            this.txtJmbg.Location = new System.Drawing.Point(155, 231);
            this.txtJmbg.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.txtJmbg.MaxLength = 13;
            this.txtJmbg.Name = "txtJmbg";
            this.txtJmbg.Size = new System.Drawing.Size(117, 20);
            this.txtJmbg.TabIndex = 14;
            // 
            // txtUsername
            // 
            this.txtUsername.Location = new System.Drawing.Point(155, 286);
            this.txtUsername.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.txtUsername.Name = "txtUsername";
            this.txtUsername.Size = new System.Drawing.Size(117, 20);
            this.txtUsername.TabIndex = 15;
            // 
            // txtPassword
            // 
            this.txtPassword.Location = new System.Drawing.Point(155, 332);
            this.txtPassword.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.Size = new System.Drawing.Size(117, 20);
            this.txtPassword.TabIndex = 16;
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePicker1.Location = new System.Drawing.Point(155, 131);
            this.dateTimePicker1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(117, 20);
            this.dateTimePicker1.TabIndex = 17;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.checkBoxUcenik);
            this.groupBox1.Controls.Add(this.checkBoxRoditelj);
            this.groupBox1.Controls.Add(this.checkBoxNastavnik);
            this.groupBox1.Controls.Add(this.checkBoxAdmin);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.ForeColor = System.Drawing.Color.MidnightBlue;
            this.groupBox1.Location = new System.Drawing.Point(316, 29);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupBox1.Size = new System.Drawing.Size(374, 72);
            this.groupBox1.TabIndex = 18;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Tip korisnika:";
            // 
            // checkBoxUcenik
            // 
            this.checkBoxUcenik.AutoSize = true;
            this.checkBoxUcenik.Location = new System.Drawing.Point(302, 37);
            this.checkBoxUcenik.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.checkBoxUcenik.Name = "checkBoxUcenik";
            this.checkBoxUcenik.Size = new System.Drawing.Size(66, 17);
            this.checkBoxUcenik.TabIndex = 3;
            this.checkBoxUcenik.Text = "Ucenik";
            this.checkBoxUcenik.UseVisualStyleBackColor = true;
            this.checkBoxUcenik.CheckedChanged += new System.EventHandler(this.checkBoxUcenik_CheckedChanged);
            // 
            // checkBoxRoditelj
            // 
            this.checkBoxRoditelj.AutoSize = true;
            this.checkBoxRoditelj.Location = new System.Drawing.Point(204, 37);
            this.checkBoxRoditelj.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.checkBoxRoditelj.Name = "checkBoxRoditelj";
            this.checkBoxRoditelj.Size = new System.Drawing.Size(69, 17);
            this.checkBoxRoditelj.TabIndex = 2;
            this.checkBoxRoditelj.Text = "Roditelj";
            this.checkBoxRoditelj.UseVisualStyleBackColor = true;
            this.checkBoxRoditelj.CheckedChanged += new System.EventHandler(this.checkBoxRoditelj_CheckedChanged);
            // 
            // checkBoxNastavnik
            // 
            this.checkBoxNastavnik.AutoSize = true;
            this.checkBoxNastavnik.Location = new System.Drawing.Point(102, 37);
            this.checkBoxNastavnik.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.checkBoxNastavnik.Name = "checkBoxNastavnik";
            this.checkBoxNastavnik.Size = new System.Drawing.Size(83, 17);
            this.checkBoxNastavnik.TabIndex = 1;
            this.checkBoxNastavnik.Text = "Nastavnik";
            this.checkBoxNastavnik.UseVisualStyleBackColor = true;
            this.checkBoxNastavnik.CheckedChanged += new System.EventHandler(this.checkBoxNastavnik_CheckedChanged);
            // 
            // checkBoxAdmin
            // 
            this.checkBoxAdmin.AutoSize = true;
            this.checkBoxAdmin.Location = new System.Drawing.Point(22, 37);
            this.checkBoxAdmin.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.checkBoxAdmin.Name = "checkBoxAdmin";
            this.checkBoxAdmin.Size = new System.Drawing.Size(60, 17);
            this.checkBoxAdmin.TabIndex = 0;
            this.checkBoxAdmin.Text = "Admin";
            this.checkBoxAdmin.UseVisualStyleBackColor = true;
            this.checkBoxAdmin.CheckedChanged += new System.EventHandler(this.checkBoxAdmin_CheckedChanged);
            // 
            // panelNastavnik
            // 
            this.panelNastavnik.Controls.Add(this.txtsss);
            this.panelNastavnik.Controls.Add(this.label7);
            this.panelNastavnik.Location = new System.Drawing.Point(322, 105);
            this.panelNastavnik.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.panelNastavnik.Name = "panelNastavnik";
            this.panelNastavnik.Size = new System.Drawing.Size(362, 106);
            this.panelNastavnik.TabIndex = 19;
            this.panelNastavnik.Visible = false;
            // 
            // txtsss
            // 
            this.txtsss.Location = new System.Drawing.Point(96, 62);
            this.txtsss.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.txtsss.MaxLength = 100;
            this.txtsss.Name = "txtsss";
            this.txtsss.Size = new System.Drawing.Size(173, 20);
            this.txtsss.TabIndex = 20;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.MidnightBlue;
            this.label7.Location = new System.Drawing.Point(93, 26);
            this.label7.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(176, 17);
            this.label7.TabIndex = 20;
            this.label7.Text = "Stepen stručne spreme";
            // 
            // panelUcenika
            // 
            this.panelUcenika.Controls.Add(this.txtVladanje);
            this.panelUcenika.Controls.Add(this.txtNeopravdani);
            this.panelUcenika.Controls.Add(this.txtOpravdani);
            this.panelUcenika.Controls.Add(this.label11);
            this.panelUcenika.Controls.Add(this.label10);
            this.panelUcenika.Controls.Add(this.label9);
            this.panelUcenika.Controls.Add(this.comboBoxOdeljenjeUcenika);
            this.panelUcenika.Controls.Add(this.label8);
            this.panelUcenika.Location = new System.Drawing.Point(318, 122);
            this.panelUcenika.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.panelUcenika.Name = "panelUcenika";
            this.panelUcenika.Size = new System.Drawing.Size(372, 184);
            this.panelUcenika.TabIndex = 21;
            this.panelUcenika.Visible = false;
            // 
            // txtVladanje
            // 
            this.txtVladanje.Location = new System.Drawing.Point(243, 141);
            this.txtVladanje.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.txtVladanje.MaxLength = 1;
            this.txtVladanje.Name = "txtVladanje";
            this.txtVladanje.Size = new System.Drawing.Size(92, 20);
            this.txtVladanje.TabIndex = 26;
            // 
            // txtNeopravdani
            // 
            this.txtNeopravdani.Location = new System.Drawing.Point(243, 106);
            this.txtNeopravdani.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.txtNeopravdani.Name = "txtNeopravdani";
            this.txtNeopravdani.Size = new System.Drawing.Size(92, 20);
            this.txtNeopravdani.TabIndex = 25;
            // 
            // txtOpravdani
            // 
            this.txtOpravdani.Location = new System.Drawing.Point(243, 71);
            this.txtOpravdani.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.txtOpravdani.Name = "txtOpravdani";
            this.txtOpravdani.Size = new System.Drawing.Size(92, 20);
            this.txtOpravdani.TabIndex = 22;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.MidnightBlue;
            this.label11.Location = new System.Drawing.Point(59, 141);
            this.label11.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(138, 17);
            this.label11.TabIndex = 24;
            this.label11.Text = "Ocena iz vladanja";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.MidnightBlue;
            this.label10.Location = new System.Drawing.Point(59, 107);
            this.label10.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(151, 17);
            this.label10.TabIndex = 23;
            this.label10.Text = "Neopravdani časovi";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.MidnightBlue;
            this.label9.Location = new System.Drawing.Point(59, 72);
            this.label9.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(134, 17);
            this.label9.TabIndex = 22;
            this.label9.Text = "Opravdani časovi";
            // 
            // comboBoxOdeljenjeUcenika
            // 
            this.comboBoxOdeljenjeUcenika.FormattingEnabled = true;
            this.comboBoxOdeljenjeUcenika.Location = new System.Drawing.Point(243, 33);
            this.comboBoxOdeljenjeUcenika.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.comboBoxOdeljenjeUcenika.Name = "comboBoxOdeljenjeUcenika";
            this.comboBoxOdeljenjeUcenika.Size = new System.Drawing.Size(92, 21);
            this.comboBoxOdeljenjeUcenika.TabIndex = 21;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.MidnightBlue;
            this.label8.Location = new System.Drawing.Point(59, 37);
            this.label8.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(138, 17);
            this.label8.TabIndex = 20;
            this.label8.Text = "Odeljenje učenika";
            // 
            // button
            // 
            this.button.BackColor = System.Drawing.Color.PowderBlue;
            this.button.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button.ForeColor = System.Drawing.Color.MidnightBlue;
            this.button.Location = new System.Drawing.Point(431, 332);
            this.button.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.button.Name = "button";
            this.button.Size = new System.Drawing.Size(126, 37);
            this.button.TabIndex = 34;
            this.button.Text = "Dodaj korisnika";
            this.button.UseVisualStyleBackColor = false;
            this.button.Click += new System.EventHandler(this.button_Click);
            // 
            // FormaDodavanjeKorisnika
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(701, 394);
            this.Controls.Add(this.panelUcenika);
            this.Controls.Add(this.panelNastavnik);
            this.Controls.Add(this.button);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.dateTimePicker1);
            this.Controls.Add(this.txtPassword);
            this.Controls.Add(this.txtUsername);
            this.Controls.Add(this.txtJmbg);
            this.Controls.Add(this.txtPol);
            this.Controls.Add(this.txtPrezime);
            this.Controls.Add(this.txtIme);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtImePrezimeAdministratora);
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Name = "FormaDodavanjeKorisnika";
            this.Text = "e-Dnevnik";
            this.Load += new System.EventHandler(this.FormaDodavanjeKorisnika_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.panelNastavnik.ResumeLayout(false);
            this.panelNastavnik.PerformLayout();
            this.panelUcenika.ResumeLayout(false);
            this.panelUcenika.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label txtImePrezimeAdministratora;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtIme;
        private System.Windows.Forms.TextBox txtPrezime;
        private System.Windows.Forms.TextBox txtPol;
        private System.Windows.Forms.TextBox txtJmbg;
        private System.Windows.Forms.TextBox txtUsername;
        private System.Windows.Forms.TextBox txtPassword;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.CheckBox checkBoxUcenik;
        private System.Windows.Forms.CheckBox checkBoxRoditelj;
        private System.Windows.Forms.CheckBox checkBoxNastavnik;
        private System.Windows.Forms.CheckBox checkBoxAdmin;
        private System.Windows.Forms.Panel panelNastavnik;
        private System.Windows.Forms.TextBox txtsss;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Panel panelUcenika;
        private System.Windows.Forms.TextBox txtVladanje;
        private System.Windows.Forms.TextBox txtNeopravdani;
        private System.Windows.Forms.TextBox txtOpravdani;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox comboBoxOdeljenjeUcenika;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button button;
    }
}