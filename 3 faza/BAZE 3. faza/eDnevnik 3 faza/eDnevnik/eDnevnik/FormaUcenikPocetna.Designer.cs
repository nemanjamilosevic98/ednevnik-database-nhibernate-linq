﻿namespace eDnevnik
{
    partial class FormaUcenikPocetna
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormaUcenikPocetna));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            this.panelPrijavljeniNastavnik = new System.Windows.Forms.Panel();
            this.button1 = new System.Windows.Forms.Button();
            this.txtImePrezimeUcenika = new System.Windows.Forms.Label();
            this.txt1 = new System.Windows.Forms.Label();
            this.btnOcene = new System.Windows.Forms.Button();
            this.btnRasporedCasova = new System.Windows.Forms.Button();
            this.groupBoxOcene = new System.Windows.Forms.GroupBox();
            this.labelOceneUcenikPrezime = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.labelOceneUcenikIme = new System.Windows.Forms.Label();
            this.dataGridOcene = new System.Windows.Forms.DataGridView();
            this.Column7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBoxRaspored = new System.Windows.Forms.GroupBox();
            this.labelRasporedSmer = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.labelRasporedOdeljenje = new System.Windows.Forms.Label();
            this.dataGridRaspored = new System.Windows.Forms.DataGridView();
            this.Column11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column12 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column13 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column14 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column15 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panelPrijavljeniNastavnik.SuspendLayout();
            this.groupBoxOcene.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridOcene)).BeginInit();
            this.groupBoxRaspored.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridRaspored)).BeginInit();
            this.SuspendLayout();
            // 
            // panelPrijavljeniNastavnik
            // 
            this.panelPrijavljeniNastavnik.Controls.Add(this.button1);
            this.panelPrijavljeniNastavnik.Controls.Add(this.txtImePrezimeUcenika);
            this.panelPrijavljeniNastavnik.Controls.Add(this.txt1);
            this.panelPrijavljeniNastavnik.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelPrijavljeniNastavnik.Location = new System.Drawing.Point(0, 0);
            this.panelPrijavljeniNastavnik.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.panelPrijavljeniNastavnik.Name = "panelPrijavljeniNastavnik";
            this.panelPrijavljeniNastavnik.Size = new System.Drawing.Size(901, 46);
            this.panelPrijavljeniNastavnik.TabIndex = 4;
            // 
            // button1
            // 
            this.button1.Dock = System.Windows.Forms.DockStyle.Right;
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.ForeColor = System.Drawing.SystemColors.Highlight;
            this.button1.Image = ((System.Drawing.Image)(resources.GetObject("button1.Image")));
            this.button1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button1.Location = new System.Drawing.Point(771, 0);
            this.button1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(130, 46);
            this.button1.TabIndex = 10;
            this.button1.Text = "       Odjava";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // txtImePrezimeUcenika
            // 
            this.txtImePrezimeUcenika.AutoSize = true;
            this.txtImePrezimeUcenika.BackColor = System.Drawing.Color.Transparent;
            this.txtImePrezimeUcenika.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtImePrezimeUcenika.ForeColor = System.Drawing.Color.MidnightBlue;
            this.txtImePrezimeUcenika.Location = new System.Drawing.Point(61, 11);
            this.txtImePrezimeUcenika.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.txtImePrezimeUcenika.Name = "txtImePrezimeUcenika";
            this.txtImePrezimeUcenika.Size = new System.Drawing.Size(91, 17);
            this.txtImePrezimeUcenika.TabIndex = 3;
            this.txtImePrezimeUcenika.Text = "ImePrezime";
            // 
            // txt1
            // 
            this.txt1.AutoSize = true;
            this.txt1.BackColor = System.Drawing.Color.Transparent;
            this.txt1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt1.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.txt1.Location = new System.Drawing.Point(2, 11);
            this.txt1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.txt1.Name = "txt1";
            this.txt1.Size = new System.Drawing.Size(62, 17);
            this.txt1.TabIndex = 2;
            this.txt1.Text = "Učenik:";
            // 
            // btnOcene
            // 
            this.btnOcene.BackColor = System.Drawing.Color.PowderBlue;
            this.btnOcene.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnOcene.ForeColor = System.Drawing.Color.MidnightBlue;
            this.btnOcene.Image = ((System.Drawing.Image)(resources.GetObject("btnOcene.Image")));
            this.btnOcene.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnOcene.Location = new System.Drawing.Point(38, 59);
            this.btnOcene.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnOcene.Name = "btnOcene";
            this.btnOcene.Size = new System.Drawing.Size(166, 80);
            this.btnOcene.TabIndex = 32;
            this.btnOcene.Text = "  Ocene";
            this.btnOcene.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnOcene.UseVisualStyleBackColor = false;
            this.btnOcene.Click += new System.EventHandler(this.btnOcene_Click);
            // 
            // btnRasporedCasova
            // 
            this.btnRasporedCasova.BackColor = System.Drawing.Color.PowderBlue;
            this.btnRasporedCasova.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRasporedCasova.ForeColor = System.Drawing.Color.MidnightBlue;
            this.btnRasporedCasova.Image = ((System.Drawing.Image)(resources.GetObject("btnRasporedCasova.Image")));
            this.btnRasporedCasova.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnRasporedCasova.Location = new System.Drawing.Point(316, 59);
            this.btnRasporedCasova.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnRasporedCasova.Name = "btnRasporedCasova";
            this.btnRasporedCasova.Size = new System.Drawing.Size(163, 80);
            this.btnRasporedCasova.TabIndex = 33;
            this.btnRasporedCasova.Text = "Raspored   časova";
            this.btnRasporedCasova.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnRasporedCasova.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnRasporedCasova.UseVisualStyleBackColor = false;
            this.btnRasporedCasova.Click += new System.EventHandler(this.btnRasporedCasova_Click);
            // 
            // groupBoxOcene
            // 
            this.groupBoxOcene.Controls.Add(this.labelOceneUcenikPrezime);
            this.groupBoxOcene.Controls.Add(this.label1);
            this.groupBoxOcene.Controls.Add(this.label2);
            this.groupBoxOcene.Controls.Add(this.labelOceneUcenikIme);
            this.groupBoxOcene.Controls.Add(this.dataGridOcene);
            this.groupBoxOcene.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBoxOcene.ForeColor = System.Drawing.Color.MidnightBlue;
            this.groupBoxOcene.Location = new System.Drawing.Point(202, 162);
            this.groupBoxOcene.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupBoxOcene.Name = "groupBoxOcene";
            this.groupBoxOcene.Padding = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupBoxOcene.Size = new System.Drawing.Size(557, 284);
            this.groupBoxOcene.TabIndex = 34;
            this.groupBoxOcene.TabStop = false;
            this.groupBoxOcene.Text = "Ocene";
            // 
            // labelOceneUcenikPrezime
            // 
            this.labelOceneUcenikPrezime.AutoSize = true;
            this.labelOceneUcenikPrezime.BackColor = System.Drawing.Color.Transparent;
            this.labelOceneUcenikPrezime.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelOceneUcenikPrezime.ForeColor = System.Drawing.Color.MidnightBlue;
            this.labelOceneUcenikPrezime.Location = new System.Drawing.Point(386, 23);
            this.labelOceneUcenikPrezime.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelOceneUcenikPrezime.Name = "labelOceneUcenikPrezime";
            this.labelOceneUcenikPrezime.Size = new System.Drawing.Size(65, 17);
            this.labelOceneUcenikPrezime.TabIndex = 32;
            this.labelOceneUcenikPrezime.Text = "prezime";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label1.Location = new System.Drawing.Point(257, 23);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(132, 17);
            this.label1.TabIndex = 31;
            this.label1.Text = "Prezime učenika:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label2.Location = new System.Drawing.Point(40, 23);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(99, 17);
            this.label2.TabIndex = 10;
            this.label2.Text = "Ime učenika:";
            // 
            // labelOceneUcenikIme
            // 
            this.labelOceneUcenikIme.AutoSize = true;
            this.labelOceneUcenikIme.BackColor = System.Drawing.Color.Transparent;
            this.labelOceneUcenikIme.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelOceneUcenikIme.ForeColor = System.Drawing.Color.MidnightBlue;
            this.labelOceneUcenikIme.Location = new System.Drawing.Point(148, 23);
            this.labelOceneUcenikIme.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelOceneUcenikIme.Name = "labelOceneUcenikIme";
            this.labelOceneUcenikIme.Size = new System.Drawing.Size(33, 17);
            this.labelOceneUcenikIme.TabIndex = 10;
            this.labelOceneUcenikIme.Text = "ime";
            // 
            // dataGridOcene
            // 
            this.dataGridOcene.BackgroundColor = System.Drawing.Color.White;
            this.dataGridOcene.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.Color.MidnightBlue;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridOcene.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle6;
            this.dataGridOcene.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridOcene.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column7,
            this.Column8,
            this.Column9,
            this.Column10});
            this.dataGridOcene.EnableHeadersVisualStyles = false;
            this.dataGridOcene.GridColor = System.Drawing.Color.MediumBlue;
            this.dataGridOcene.Location = new System.Drawing.Point(43, 49);
            this.dataGridOcene.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.dataGridOcene.Name = "dataGridOcene";
            this.dataGridOcene.ReadOnly = true;
            this.dataGridOcene.RowHeadersVisible = false;
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle7.BackColor = System.Drawing.SystemColors.ControlLight;
            dataGridViewCellStyle7.ForeColor = System.Drawing.Color.MidnightBlue;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.Color.LightCyan;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.Color.MidnightBlue;
            this.dataGridOcene.RowsDefaultCellStyle = dataGridViewCellStyle7;
            this.dataGridOcene.RowTemplate.Height = 24;
            this.dataGridOcene.Size = new System.Drawing.Size(490, 210);
            this.dataGridOcene.TabIndex = 30;
            // 
            // Column7
            // 
            this.Column7.HeaderText = "ID Ocene";
            this.Column7.Name = "Column7";
            this.Column7.ReadOnly = true;
            this.Column7.Width = 130;
            // 
            // Column8
            // 
            this.Column8.HeaderText = "Predmet";
            this.Column8.Name = "Column8";
            this.Column8.ReadOnly = true;
            // 
            // Column9
            // 
            this.Column9.HeaderText = "Tip ocene";
            this.Column9.Name = "Column9";
            this.Column9.ReadOnly = true;
            this.Column9.Width = 130;
            // 
            // Column10
            // 
            this.Column10.HeaderText = "Ocena";
            this.Column10.Name = "Column10";
            this.Column10.ReadOnly = true;
            // 
            // groupBoxRaspored
            // 
            this.groupBoxRaspored.Controls.Add(this.labelRasporedSmer);
            this.groupBoxRaspored.Controls.Add(this.label8);
            this.groupBoxRaspored.Controls.Add(this.label9);
            this.groupBoxRaspored.Controls.Add(this.labelRasporedOdeljenje);
            this.groupBoxRaspored.Controls.Add(this.dataGridRaspored);
            this.groupBoxRaspored.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBoxRaspored.ForeColor = System.Drawing.Color.MidnightBlue;
            this.groupBoxRaspored.Location = new System.Drawing.Point(18, 172);
            this.groupBoxRaspored.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupBoxRaspored.Name = "groupBoxRaspored";
            this.groupBoxRaspored.Padding = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupBoxRaspored.Size = new System.Drawing.Size(850, 254);
            this.groupBoxRaspored.TabIndex = 35;
            this.groupBoxRaspored.TabStop = false;
            this.groupBoxRaspored.Text = "Raspored časova";
            // 
            // labelRasporedSmer
            // 
            this.labelRasporedSmer.AutoSize = true;
            this.labelRasporedSmer.BackColor = System.Drawing.Color.Transparent;
            this.labelRasporedSmer.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelRasporedSmer.ForeColor = System.Drawing.Color.MidnightBlue;
            this.labelRasporedSmer.Location = new System.Drawing.Point(254, 29);
            this.labelRasporedSmer.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelRasporedSmer.Name = "labelRasporedSmer";
            this.labelRasporedSmer.Size = new System.Drawing.Size(65, 17);
            this.labelRasporedSmer.TabIndex = 32;
            this.labelRasporedSmer.Text = "prezime";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label8.Location = new System.Drawing.Point(206, 29);
            this.label8.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(50, 17);
            this.label8.TabIndex = 31;
            this.label8.Text = "Smer:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label9.Location = new System.Drawing.Point(47, 29);
            this.label9.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(82, 17);
            this.label9.TabIndex = 10;
            this.label9.Text = "Odeljenje:";
            // 
            // labelRasporedOdeljenje
            // 
            this.labelRasporedOdeljenje.AutoSize = true;
            this.labelRasporedOdeljenje.BackColor = System.Drawing.Color.Transparent;
            this.labelRasporedOdeljenje.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelRasporedOdeljenje.ForeColor = System.Drawing.Color.MidnightBlue;
            this.labelRasporedOdeljenje.Location = new System.Drawing.Point(141, 29);
            this.labelRasporedOdeljenje.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelRasporedOdeljenje.Name = "labelRasporedOdeljenje";
            this.labelRasporedOdeljenje.Size = new System.Drawing.Size(33, 17);
            this.labelRasporedOdeljenje.TabIndex = 10;
            this.labelRasporedOdeljenje.Text = "ime";
            // 
            // dataGridRaspored
            // 
            this.dataGridRaspored.BackgroundColor = System.Drawing.Color.White;
            this.dataGridRaspored.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle8.BackColor = System.Drawing.Color.MidnightBlue;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle8.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridRaspored.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle8;
            this.dataGridRaspored.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridRaspored.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column11,
            this.Column12,
            this.Column13,
            this.Column14,
            this.Column15});
            this.dataGridRaspored.EnableHeadersVisualStyles = false;
            this.dataGridRaspored.GridColor = System.Drawing.Color.MediumBlue;
            this.dataGridRaspored.Location = new System.Drawing.Point(0, 62);
            this.dataGridRaspored.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.dataGridRaspored.Name = "dataGridRaspored";
            this.dataGridRaspored.ReadOnly = true;
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle9.BackColor = System.Drawing.SystemColors.ControlLight;
            dataGridViewCellStyle9.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle9.ForeColor = System.Drawing.Color.MidnightBlue;
            dataGridViewCellStyle9.SelectionBackColor = System.Drawing.Color.LightCyan;
            dataGridViewCellStyle9.SelectionForeColor = System.Drawing.Color.MidnightBlue;
            dataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridRaspored.RowHeadersDefaultCellStyle = dataGridViewCellStyle9;
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle10.BackColor = System.Drawing.SystemColors.ControlLight;
            dataGridViewCellStyle10.ForeColor = System.Drawing.Color.MidnightBlue;
            dataGridViewCellStyle10.SelectionBackColor = System.Drawing.Color.LightCyan;
            dataGridViewCellStyle10.SelectionForeColor = System.Drawing.Color.MidnightBlue;
            this.dataGridRaspored.RowsDefaultCellStyle = dataGridViewCellStyle10;
            this.dataGridRaspored.RowTemplate.Height = 24;
            this.dataGridRaspored.Size = new System.Drawing.Size(822, 187);
            this.dataGridRaspored.TabIndex = 30;
            // 
            // Column11
            // 
            this.Column11.HeaderText = "Ponedeljak";
            this.Column11.Name = "Column11";
            this.Column11.ReadOnly = true;
            this.Column11.Width = 150;
            // 
            // Column12
            // 
            this.Column12.HeaderText = "Utorak";
            this.Column12.Name = "Column12";
            this.Column12.ReadOnly = true;
            this.Column12.Width = 150;
            // 
            // Column13
            // 
            this.Column13.HeaderText = "Sreda";
            this.Column13.Name = "Column13";
            this.Column13.ReadOnly = true;
            this.Column13.Width = 150;
            // 
            // Column14
            // 
            this.Column14.HeaderText = "Cetvrtak";
            this.Column14.Name = "Column14";
            this.Column14.ReadOnly = true;
            this.Column14.Width = 150;
            // 
            // Column15
            // 
            this.Column15.HeaderText = "Petak";
            this.Column15.Name = "Column15";
            this.Column15.ReadOnly = true;
            this.Column15.Width = 150;
            // 
            // FormaUcenikPocetna
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(901, 457);
            this.Controls.Add(this.groupBoxRaspored);
            this.Controls.Add(this.btnRasporedCasova);
            this.Controls.Add(this.btnOcene);
            this.Controls.Add(this.panelPrijavljeniNastavnik);
            this.Controls.Add(this.groupBoxOcene);
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Name = "FormaUcenikPocetna";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "e-Dnevnik";
            this.Load += new System.EventHandler(this.FormaUcenikPocetna_Load);
            this.panelPrijavljeniNastavnik.ResumeLayout(false);
            this.panelPrijavljeniNastavnik.PerformLayout();
            this.groupBoxOcene.ResumeLayout(false);
            this.groupBoxOcene.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridOcene)).EndInit();
            this.groupBoxRaspored.ResumeLayout(false);
            this.groupBoxRaspored.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridRaspored)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelPrijavljeniNastavnik;
        private System.Windows.Forms.Label txtImePrezimeUcenika;
        private System.Windows.Forms.Label txt1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button btnOcene;
        private System.Windows.Forms.Button btnRasporedCasova;
        private System.Windows.Forms.GroupBox groupBoxOcene;
        private System.Windows.Forms.Label labelOceneUcenikPrezime;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label labelOceneUcenikIme;
        private System.Windows.Forms.DataGridView dataGridOcene;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column7;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column8;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column9;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column10;
        private System.Windows.Forms.GroupBox groupBoxRaspored;
        private System.Windows.Forms.Label labelRasporedSmer;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label labelRasporedOdeljenje;
        private System.Windows.Forms.DataGridView dataGridRaspored;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column11;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column12;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column13;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column14;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column15;
    }
}