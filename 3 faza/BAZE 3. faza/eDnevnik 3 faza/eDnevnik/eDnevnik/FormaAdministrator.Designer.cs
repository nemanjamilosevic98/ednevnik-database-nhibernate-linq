﻿namespace eDnevnik
{
    partial class FormaAdministrator
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormaAdministrator));
            this.panelPrijavljeniNastavnik = new System.Windows.Forms.Panel();
            this.button1 = new System.Windows.Forms.Button();
            this.txtImePrezimeAdministratora = new System.Windows.Forms.Label();
            this.txt1 = new System.Windows.Forms.Label();
            this.btnSviKorisnici = new System.Windows.Forms.Button();
            this.buttonUceniciOdeljenja = new System.Windows.Forms.Button();
            this.btnOdeljenja = new System.Windows.Forms.Button();
            this.btnRoditeljiDeca = new System.Windows.Forms.Button();
            this.buttonRazredneStaresine = new System.Windows.Forms.Button();
            this.buttonPredmeti = new System.Windows.Forms.Button();
            this.buttonPredstavniciOdeljenja = new System.Windows.Forms.Button();
            this.buttonVrsiociFunkcija = new System.Windows.Forms.Button();
            this.buttonPredmetiNaGodini = new System.Windows.Forms.Button();
            this.buttonBrojeviTelefona = new System.Windows.Forms.Button();
            this.buttonRasporediCasova = new System.Windows.Forms.Button();
            this.buttonDrziCas = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.panelPrijavljeniNastavnik.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelPrijavljeniNastavnik
            // 
            this.panelPrijavljeniNastavnik.Controls.Add(this.button1);
            this.panelPrijavljeniNastavnik.Controls.Add(this.txtImePrezimeAdministratora);
            this.panelPrijavljeniNastavnik.Controls.Add(this.txt1);
            this.panelPrijavljeniNastavnik.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelPrijavljeniNastavnik.Location = new System.Drawing.Point(0, 0);
            this.panelPrijavljeniNastavnik.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.panelPrijavljeniNastavnik.Name = "panelPrijavljeniNastavnik";
            this.panelPrijavljeniNastavnik.Size = new System.Drawing.Size(868, 50);
            this.panelPrijavljeniNastavnik.TabIndex = 4;
            // 
            // button1
            // 
            this.button1.Dock = System.Windows.Forms.DockStyle.Right;
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.ForeColor = System.Drawing.SystemColors.Highlight;
            this.button1.Image = ((System.Drawing.Image)(resources.GetObject("button1.Image")));
            this.button1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button1.Location = new System.Drawing.Point(718, 0);
            this.button1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(150, 50);
            this.button1.TabIndex = 5;
            this.button1.Text = "Odjava";
            this.button1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // txtImePrezimeAdministratora
            // 
            this.txtImePrezimeAdministratora.AutoSize = true;
            this.txtImePrezimeAdministratora.BackColor = System.Drawing.Color.Transparent;
            this.txtImePrezimeAdministratora.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtImePrezimeAdministratora.ForeColor = System.Drawing.Color.MidnightBlue;
            this.txtImePrezimeAdministratora.Location = new System.Drawing.Point(133, 17);
            this.txtImePrezimeAdministratora.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.txtImePrezimeAdministratora.Name = "txtImePrezimeAdministratora";
            this.txtImePrezimeAdministratora.Size = new System.Drawing.Size(91, 17);
            this.txtImePrezimeAdministratora.TabIndex = 3;
            this.txtImePrezimeAdministratora.Text = "ImePrezime";
            // 
            // txt1
            // 
            this.txt1.AutoSize = true;
            this.txt1.BackColor = System.Drawing.Color.Transparent;
            this.txt1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt1.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.txt1.Location = new System.Drawing.Point(11, 17);
            this.txt1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.txt1.Name = "txt1";
            this.txt1.Size = new System.Drawing.Size(109, 17);
            this.txt1.TabIndex = 2;
            this.txt1.Text = "Administrator:";
            // 
            // btnSviKorisnici
            // 
            this.btnSviKorisnici.BackColor = System.Drawing.Color.PowderBlue;
            this.btnSviKorisnici.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSviKorisnici.ForeColor = System.Drawing.Color.MidnightBlue;
            this.btnSviKorisnici.Location = new System.Drawing.Point(263, 79);
            this.btnSviKorisnici.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnSviKorisnici.Name = "btnSviKorisnici";
            this.btnSviKorisnici.Size = new System.Drawing.Size(111, 31);
            this.btnSviKorisnici.TabIndex = 32;
            this.btnSviKorisnici.Text = "Korisnici";
            this.btnSviKorisnici.UseVisualStyleBackColor = false;
            this.btnSviKorisnici.Click += new System.EventHandler(this.btnSviKorisnici_Click);
            // 
            // buttonUceniciOdeljenja
            // 
            this.buttonUceniciOdeljenja.BackColor = System.Drawing.Color.PowderBlue;
            this.buttonUceniciOdeljenja.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonUceniciOdeljenja.ForeColor = System.Drawing.Color.MidnightBlue;
            this.buttonUceniciOdeljenja.Location = new System.Drawing.Point(263, 141);
            this.buttonUceniciOdeljenja.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.buttonUceniciOdeljenja.Name = "buttonUceniciOdeljenja";
            this.buttonUceniciOdeljenja.Size = new System.Drawing.Size(111, 31);
            this.buttonUceniciOdeljenja.TabIndex = 33;
            this.buttonUceniciOdeljenja.Text = "Učenici";
            this.buttonUceniciOdeljenja.UseVisualStyleBackColor = false;
            this.buttonUceniciOdeljenja.Click += new System.EventHandler(this.buttonUceniciOdeljenja_Click);
            // 
            // btnOdeljenja
            // 
            this.btnOdeljenja.BackColor = System.Drawing.Color.PowderBlue;
            this.btnOdeljenja.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnOdeljenja.ForeColor = System.Drawing.Color.MidnightBlue;
            this.btnOdeljenja.Location = new System.Drawing.Point(263, 202);
            this.btnOdeljenja.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnOdeljenja.Name = "btnOdeljenja";
            this.btnOdeljenja.Size = new System.Drawing.Size(111, 31);
            this.btnOdeljenja.TabIndex = 34;
            this.btnOdeljenja.Text = "Odeljenja";
            this.btnOdeljenja.UseVisualStyleBackColor = false;
            this.btnOdeljenja.Click += new System.EventHandler(this.btnOdeljenja_Click);
            // 
            // btnRoditeljiDeca
            // 
            this.btnRoditeljiDeca.BackColor = System.Drawing.Color.PowderBlue;
            this.btnRoditeljiDeca.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRoditeljiDeca.ForeColor = System.Drawing.Color.MidnightBlue;
            this.btnRoditeljiDeca.Location = new System.Drawing.Point(263, 267);
            this.btnRoditeljiDeca.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnRoditeljiDeca.Name = "btnRoditeljiDeca";
            this.btnRoditeljiDeca.Size = new System.Drawing.Size(111, 31);
            this.btnRoditeljiDeca.TabIndex = 35;
            this.btnRoditeljiDeca.Text = "Roditelji";
            this.btnRoditeljiDeca.UseVisualStyleBackColor = false;
            this.btnRoditeljiDeca.Click += new System.EventHandler(this.btnRoditeljiDeca_Click);
            // 
            // buttonRazredneStaresine
            // 
            this.buttonRazredneStaresine.BackColor = System.Drawing.Color.PowderBlue;
            this.buttonRazredneStaresine.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonRazredneStaresine.ForeColor = System.Drawing.Color.MidnightBlue;
            this.buttonRazredneStaresine.Location = new System.Drawing.Point(263, 325);
            this.buttonRazredneStaresine.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.buttonRazredneStaresine.Name = "buttonRazredneStaresine";
            this.buttonRazredneStaresine.Size = new System.Drawing.Size(111, 45);
            this.buttonRazredneStaresine.TabIndex = 36;
            this.buttonRazredneStaresine.Text = "Razredne starešine";
            this.buttonRazredneStaresine.UseVisualStyleBackColor = false;
            this.buttonRazredneStaresine.Click += new System.EventHandler(this.buttonRazredneStaresine_Click);
            // 
            // buttonPredmeti
            // 
            this.buttonPredmeti.BackColor = System.Drawing.Color.PowderBlue;
            this.buttonPredmeti.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonPredmeti.ForeColor = System.Drawing.Color.MidnightBlue;
            this.buttonPredmeti.Location = new System.Drawing.Point(263, 398);
            this.buttonPredmeti.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.buttonPredmeti.Name = "buttonPredmeti";
            this.buttonPredmeti.Size = new System.Drawing.Size(111, 31);
            this.buttonPredmeti.TabIndex = 37;
            this.buttonPredmeti.Text = "Predmeti";
            this.buttonPredmeti.UseVisualStyleBackColor = false;
            this.buttonPredmeti.Click += new System.EventHandler(this.buttonPredmeti_Click);
            // 
            // buttonPredstavniciOdeljenja
            // 
            this.buttonPredstavniciOdeljenja.BackColor = System.Drawing.Color.PowderBlue;
            this.buttonPredstavniciOdeljenja.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonPredstavniciOdeljenja.ForeColor = System.Drawing.Color.MidnightBlue;
            this.buttonPredstavniciOdeljenja.Location = new System.Drawing.Point(742, 79);
            this.buttonPredstavniciOdeljenja.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.buttonPredstavniciOdeljenja.Name = "buttonPredstavniciOdeljenja";
            this.buttonPredstavniciOdeljenja.Size = new System.Drawing.Size(111, 31);
            this.buttonPredstavniciOdeljenja.TabIndex = 38;
            this.buttonPredstavniciOdeljenja.Text = "Predstavnici";
            this.buttonPredstavniciOdeljenja.UseVisualStyleBackColor = false;
            this.buttonPredstavniciOdeljenja.Click += new System.EventHandler(this.buttonPredstavniciOdeljenja_Click);
            // 
            // buttonVrsiociFunkcija
            // 
            this.buttonVrsiociFunkcija.BackColor = System.Drawing.Color.PowderBlue;
            this.buttonVrsiociFunkcija.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonVrsiociFunkcija.ForeColor = System.Drawing.Color.MidnightBlue;
            this.buttonVrsiociFunkcija.Location = new System.Drawing.Point(742, 141);
            this.buttonVrsiociFunkcija.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.buttonVrsiociFunkcija.Name = "buttonVrsiociFunkcija";
            this.buttonVrsiociFunkcija.Size = new System.Drawing.Size(111, 31);
            this.buttonVrsiociFunkcija.TabIndex = 39;
            this.buttonVrsiociFunkcija.Text = "Savet roditelja";
            this.buttonVrsiociFunkcija.UseVisualStyleBackColor = false;
            this.buttonVrsiociFunkcija.Click += new System.EventHandler(this.buttonVrsiociFunkcija_Click);
            // 
            // buttonPredmetiNaGodini
            // 
            this.buttonPredmetiNaGodini.BackColor = System.Drawing.Color.PowderBlue;
            this.buttonPredmetiNaGodini.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonPredmetiNaGodini.ForeColor = System.Drawing.Color.MidnightBlue;
            this.buttonPredmetiNaGodini.Location = new System.Drawing.Point(742, 195);
            this.buttonPredmetiNaGodini.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.buttonPredmetiNaGodini.Name = "buttonPredmetiNaGodini";
            this.buttonPredmetiNaGodini.Size = new System.Drawing.Size(111, 45);
            this.buttonPredmetiNaGodini.TabIndex = 40;
            this.buttonPredmetiNaGodini.Text = "Predmeti na godini";
            this.buttonPredmetiNaGodini.UseVisualStyleBackColor = false;
            this.buttonPredmetiNaGodini.Click += new System.EventHandler(this.buttonPredmetiNaGodini_Click);
            // 
            // buttonBrojeviTelefona
            // 
            this.buttonBrojeviTelefona.BackColor = System.Drawing.Color.PowderBlue;
            this.buttonBrojeviTelefona.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonBrojeviTelefona.ForeColor = System.Drawing.Color.MidnightBlue;
            this.buttonBrojeviTelefona.Location = new System.Drawing.Point(742, 260);
            this.buttonBrojeviTelefona.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.buttonBrojeviTelefona.Name = "buttonBrojeviTelefona";
            this.buttonBrojeviTelefona.Size = new System.Drawing.Size(111, 45);
            this.buttonBrojeviTelefona.TabIndex = 41;
            this.buttonBrojeviTelefona.Text = "Brojevi telefona";
            this.buttonBrojeviTelefona.UseVisualStyleBackColor = false;
            this.buttonBrojeviTelefona.Click += new System.EventHandler(this.buttonBrojeviTelefona_Click);
            // 
            // buttonRasporediCasova
            // 
            this.buttonRasporediCasova.BackColor = System.Drawing.Color.PowderBlue;
            this.buttonRasporediCasova.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonRasporediCasova.ForeColor = System.Drawing.Color.MidnightBlue;
            this.buttonRasporediCasova.Location = new System.Drawing.Point(742, 325);
            this.buttonRasporediCasova.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.buttonRasporediCasova.Name = "buttonRasporediCasova";
            this.buttonRasporediCasova.Size = new System.Drawing.Size(111, 45);
            this.buttonRasporediCasova.TabIndex = 42;
            this.buttonRasporediCasova.Text = "Raspored časova";
            this.buttonRasporediCasova.UseVisualStyleBackColor = false;
            this.buttonRasporediCasova.Click += new System.EventHandler(this.buttonRasporediCasova_Click);
            // 
            // buttonDrziCas
            // 
            this.buttonDrziCas.BackColor = System.Drawing.Color.PowderBlue;
            this.buttonDrziCas.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonDrziCas.ForeColor = System.Drawing.Color.MidnightBlue;
            this.buttonDrziCas.Location = new System.Drawing.Point(742, 397);
            this.buttonDrziCas.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.buttonDrziCas.Name = "buttonDrziCas";
            this.buttonDrziCas.Size = new System.Drawing.Size(111, 31);
            this.buttonDrziCas.TabIndex = 43;
            this.buttonDrziCas.Text = "Drži čas";
            this.buttonDrziCas.UseVisualStyleBackColor = false;
            this.buttonDrziCas.Click += new System.EventHandler(this.buttonDrziCas_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label1.Location = new System.Drawing.Point(11, 86);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(234, 17);
            this.label1.TabIndex = 44;
            this.label1.Text = "Upravljanje nalozima korisnika:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label2.Location = new System.Drawing.Point(11, 148);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(232, 17);
            this.label2.TabIndex = 45;
            this.label2.Text = "Spisak učenika po odeljenjima:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label3.Location = new System.Drawing.Point(11, 209);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(182, 17);
            this.label3.TabIndex = 46;
            this.label3.Text = "Upravljanje odeljenjima:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label4.Location = new System.Drawing.Point(11, 274);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(165, 17);
            this.label4.TabIndex = 47;
            this.label4.Text = "Spisak dece roditelja:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label5.Location = new System.Drawing.Point(11, 339);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(204, 17);
            this.label5.TabIndex = 48;
            this.label5.Text = "Upravljanje starešinstvima:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label6.Location = new System.Drawing.Point(11, 404);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(224, 17);
            this.label6.TabIndex = 49;
            this.label6.Text = "Spisak predmeta i predavača:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label7.Location = new System.Drawing.Point(445, 86);
            this.label7.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(280, 17);
            this.label7.TabIndex = 50;
            this.label7.Text = "Upravljanje predstavnicima odeljenja:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label8.Location = new System.Drawing.Point(445, 148);
            this.label8.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(275, 17);
            this.label8.TabIndex = 51;
            this.label8.Text = "Upravljanje funkcijama predstavnika:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label9.Location = new System.Drawing.Point(445, 209);
            this.label9.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(232, 17);
            this.label9.TabIndex = 52;
            this.label9.Text = "Spisak predmeta po godinama:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label10.Location = new System.Drawing.Point(445, 274);
            this.label10.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(254, 17);
            this.label10.TabIndex = 53;
            this.label10.Text = "Spisak brojeva telefona korisnika:";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label11.Location = new System.Drawing.Point(445, 339);
            this.label11.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(242, 17);
            this.label11.TabIndex = 54;
            this.label11.Text = "Upravljanje rasporedom časova:";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.Color.Transparent;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label12.Location = new System.Drawing.Point(445, 404);
            this.label12.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(298, 17);
            this.label12.TabIndex = 55;
            this.label12.Text = "Časovi koje nastavnici drže odeljenjima:";
            // 
            // FormaAdministrator
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(868, 467);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.buttonDrziCas);
            this.Controls.Add(this.buttonRasporediCasova);
            this.Controls.Add(this.buttonBrojeviTelefona);
            this.Controls.Add(this.buttonPredmetiNaGodini);
            this.Controls.Add(this.buttonVrsiociFunkcija);
            this.Controls.Add(this.buttonPredstavniciOdeljenja);
            this.Controls.Add(this.buttonPredmeti);
            this.Controls.Add(this.buttonRazredneStaresine);
            this.Controls.Add(this.btnRoditeljiDeca);
            this.Controls.Add(this.btnOdeljenja);
            this.Controls.Add(this.buttonUceniciOdeljenja);
            this.Controls.Add(this.btnSviKorisnici);
            this.Controls.Add(this.panelPrijavljeniNastavnik);
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Name = "FormaAdministrator";
            this.Text = "e-Dnevnik";
            this.panelPrijavljeniNastavnik.ResumeLayout(false);
            this.panelPrijavljeniNastavnik.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panelPrijavljeniNastavnik;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label txtImePrezimeAdministratora;
        private System.Windows.Forms.Label txt1;
        private System.Windows.Forms.Button btnSviKorisnici;
        private System.Windows.Forms.Button buttonUceniciOdeljenja;
        private System.Windows.Forms.Button btnOdeljenja;
        private System.Windows.Forms.Button btnRoditeljiDeca;
        private System.Windows.Forms.Button buttonRazredneStaresine;
        private System.Windows.Forms.Button buttonPredmeti;
        private System.Windows.Forms.Button buttonPredstavniciOdeljenja;
        private System.Windows.Forms.Button buttonVrsiociFunkcija;
        private System.Windows.Forms.Button buttonPredmetiNaGodini;
        private System.Windows.Forms.Button buttonBrojeviTelefona;
        private System.Windows.Forms.Button buttonRasporediCasova;
        private System.Windows.Forms.Button buttonDrziCas;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
    }
}