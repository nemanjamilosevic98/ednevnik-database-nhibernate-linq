﻿namespace eDnevnik
{
    partial class FormaRoditeljPocetna
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormaRoditeljPocetna));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            this.panelPrijavljeniNastavnik = new System.Windows.Forms.Panel();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.button1 = new System.Windows.Forms.Button();
            this.txtImePrezimeRoditelja = new System.Windows.Forms.Label();
            this.txt1 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btnOpravdajCasove = new System.Windows.Forms.Button();
            this.btnOcene = new System.Windows.Forms.Button();
            this.dataGridMojaDeca = new System.Windows.Forms.DataGridView();
            this.btnRasporedCasova = new System.Windows.Forms.Button();
            this.groupBoxOcene = new System.Windows.Forms.GroupBox();
            this.labelOceneUcenikPrezime = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.labelOceneUcenikIme = new System.Windows.Forms.Label();
            this.dataGridOcene = new System.Windows.Forms.DataGridView();
            this.Column7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBoxRaspored = new System.Windows.Forms.GroupBox();
            this.labelRasporedSmer = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.labelRasporedOdeljenje = new System.Windows.Forms.Label();
            this.dataGridRaspored = new System.Windows.Forms.DataGridView();
            this.groupBoxOpravdavanjeCasova = new System.Windows.Forms.GroupBox();
            this.label10 = new System.Windows.Forms.Label();
            this.buttonOpavdajCasoveUceniku = new System.Windows.Forms.Button();
            this.numericUpDownBrojCasova = new System.Windows.Forms.NumericUpDown();
            this.labelOpravdajPrezime = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.labelOpravdajIme = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.groupBoxFunkcija = new System.Windows.Forms.GroupBox();
            this.labelFjaKraj = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.labelFjaPocetak = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.labelFjaTip = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.groupBoxPredstavljanje = new System.Windows.Forms.GroupBox();
            this.labelPredtsavljaOdeljenjeSmer = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.labelPredstavljanjeKraj = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.labelPredstavljanjePocetak = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.labelPredstavljaOdeljenje = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.Column11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column12 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column13 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column14 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column15 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Odeljenje = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panelPrijavljeniNastavnik.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridMojaDeca)).BeginInit();
            this.groupBoxOcene.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridOcene)).BeginInit();
            this.groupBoxRaspored.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridRaspored)).BeginInit();
            this.groupBoxOpravdavanjeCasova.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownBrojCasova)).BeginInit();
            this.groupBoxFunkcija.SuspendLayout();
            this.groupBoxPredstavljanje.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelPrijavljeniNastavnik
            // 
            this.panelPrijavljeniNastavnik.Controls.Add(this.groupBox3);
            this.panelPrijavljeniNastavnik.Controls.Add(this.button1);
            this.panelPrijavljeniNastavnik.Controls.Add(this.txtImePrezimeRoditelja);
            this.panelPrijavljeniNastavnik.Controls.Add(this.txt1);
            this.panelPrijavljeniNastavnik.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelPrijavljeniNastavnik.Location = new System.Drawing.Point(0, 0);
            this.panelPrijavljeniNastavnik.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.panelPrijavljeniNastavnik.Name = "panelPrijavljeniNastavnik";
            this.panelPrijavljeniNastavnik.Size = new System.Drawing.Size(1017, 51);
            this.panelPrijavljeniNastavnik.TabIndex = 4;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.label3);
            this.groupBox3.Controls.Add(this.label4);
            this.groupBox3.Controls.Add(this.label5);
            this.groupBox3.Controls.Add(this.label6);
            this.groupBox3.Controls.Add(this.dataGridView1);
            this.groupBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox3.ForeColor = System.Drawing.Color.MidnightBlue;
            this.groupBox3.Location = new System.Drawing.Point(173, 50);
            this.groupBox3.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Padding = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupBox3.Size = new System.Drawing.Size(676, 219);
            this.groupBox3.TabIndex = 34;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Ocene";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.MidnightBlue;
            this.label3.Location = new System.Drawing.Point(54, 136);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(65, 17);
            this.label3.TabIndex = 32;
            this.label3.Text = "prezime";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label4.Location = new System.Drawing.Point(38, 106);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(134, 17);
            this.label4.TabIndex = 31;
            this.label4.Text = "Prezime Učenika:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label5.Location = new System.Drawing.Point(46, 37);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(90, 17);
            this.label5.TabIndex = 10;
            this.label5.Text = "Ime učenik:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.MidnightBlue;
            this.label6.Location = new System.Drawing.Point(46, 68);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(33, 17);
            this.label6.TabIndex = 10;
            this.label6.Text = "ime";
            // 
            // dataGridView1
            // 
            this.dataGridView1.BackgroundColor = System.Drawing.Color.White;
            this.dataGridView1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.MidnightBlue;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewTextBoxColumn4});
            this.dataGridView1.EnableHeadersVisualStyles = false;
            this.dataGridView1.GridColor = System.Drawing.Color.MediumBlue;
            this.dataGridView1.Location = new System.Drawing.Point(196, 17);
            this.dataGridView1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.RowHeadersVisible = false;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.ControlLight;
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.MidnightBlue;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.Aqua;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.MidnightBlue;
            this.dataGridView1.RowsDefaultCellStyle = dataGridViewCellStyle2;
            this.dataGridView1.RowTemplate.Height = 24;
            this.dataGridView1.Size = new System.Drawing.Size(458, 187);
            this.dataGridView1.TabIndex = 30;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.HeaderText = "ID Ocene";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.Width = 130;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.HeaderText = "Predmet";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.HeaderText = "Tip ocene";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            this.dataGridViewTextBoxColumn3.Width = 130;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.HeaderText = "Ocena";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            // 
            // button1
            // 
            this.button1.Dock = System.Windows.Forms.DockStyle.Right;
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.ForeColor = System.Drawing.SystemColors.Highlight;
            this.button1.Image = ((System.Drawing.Image)(resources.GetObject("button1.Image")));
            this.button1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button1.Location = new System.Drawing.Point(887, 0);
            this.button1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(130, 51);
            this.button1.TabIndex = 9;
            this.button1.Text = "     Odjava";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // txtImePrezimeRoditelja
            // 
            this.txtImePrezimeRoditelja.AutoSize = true;
            this.txtImePrezimeRoditelja.BackColor = System.Drawing.Color.Transparent;
            this.txtImePrezimeRoditelja.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtImePrezimeRoditelja.ForeColor = System.Drawing.Color.MidnightBlue;
            this.txtImePrezimeRoditelja.Location = new System.Drawing.Point(73, 10);
            this.txtImePrezimeRoditelja.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.txtImePrezimeRoditelja.Name = "txtImePrezimeRoditelja";
            this.txtImePrezimeRoditelja.Size = new System.Drawing.Size(91, 17);
            this.txtImePrezimeRoditelja.TabIndex = 3;
            this.txtImePrezimeRoditelja.Text = "ImePrezime";
            // 
            // txt1
            // 
            this.txt1.AutoSize = true;
            this.txt1.BackColor = System.Drawing.Color.Transparent;
            this.txt1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt1.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.txt1.Location = new System.Drawing.Point(2, 10);
            this.txt1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.txt1.Name = "txt1";
            this.txt1.Size = new System.Drawing.Size(68, 17);
            this.txt1.TabIndex = 2;
            this.txt1.Text = "Roditelj:";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.btnOpravdajCasove);
            this.groupBox2.Controls.Add(this.btnOcene);
            this.groupBox2.Controls.Add(this.dataGridMojaDeca);
            this.groupBox2.Controls.Add(this.btnRasporedCasova);
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.ForeColor = System.Drawing.Color.MidnightBlue;
            this.groupBox2.Location = new System.Drawing.Point(11, 67);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupBox2.Size = new System.Drawing.Size(791, 256);
            this.groupBox2.TabIndex = 11;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Moja deca";
            // 
            // btnOpravdajCasove
            // 
            this.btnOpravdajCasove.BackColor = System.Drawing.Color.PowderBlue;
            this.btnOpravdajCasove.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnOpravdajCasove.ForeColor = System.Drawing.Color.MidnightBlue;
            this.btnOpravdajCasove.Image = ((System.Drawing.Image)(resources.GetObject("btnOpravdajCasove.Image")));
            this.btnOpravdajCasove.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnOpravdajCasove.Location = new System.Drawing.Point(533, 159);
            this.btnOpravdajCasove.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnOpravdajCasove.Name = "btnOpravdajCasove";
            this.btnOpravdajCasove.Size = new System.Drawing.Size(154, 80);
            this.btnOpravdajCasove.TabIndex = 32;
            this.btnOpravdajCasove.Text = "Opravdaj časove";
            this.btnOpravdajCasove.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnOpravdajCasove.UseVisualStyleBackColor = false;
            this.btnOpravdajCasove.Click += new System.EventHandler(this.btnOpravdajCasove_Click);
            // 
            // btnOcene
            // 
            this.btnOcene.BackColor = System.Drawing.Color.PowderBlue;
            this.btnOcene.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnOcene.ForeColor = System.Drawing.Color.MidnightBlue;
            this.btnOcene.Image = ((System.Drawing.Image)(resources.GetObject("btnOcene.Image")));
            this.btnOcene.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnOcene.Location = new System.Drawing.Point(293, 159);
            this.btnOcene.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnOcene.Name = "btnOcene";
            this.btnOcene.Size = new System.Drawing.Size(154, 80);
            this.btnOcene.TabIndex = 31;
            this.btnOcene.Text = "      Ocene";
            this.btnOcene.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnOcene.UseVisualStyleBackColor = false;
            this.btnOcene.Click += new System.EventHandler(this.btnOcene_Click);
            // 
            // dataGridMojaDeca
            // 
            this.dataGridMojaDeca.BackgroundColor = System.Drawing.Color.White;
            this.dataGridMojaDeca.BorderStyle = System.Windows.Forms.BorderStyle.None;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.MidnightBlue;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridMojaDeca.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dataGridMojaDeca.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridMojaDeca.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column4,
            this.Column5,
            this.Column6,
            this.Odeljenje,
            this.Column1,
            this.Column2,
            this.Column3});
            this.dataGridMojaDeca.EnableHeadersVisualStyles = false;
            this.dataGridMojaDeca.GridColor = System.Drawing.Color.MediumBlue;
            this.dataGridMojaDeca.Location = new System.Drawing.Point(14, 24);
            this.dataGridMojaDeca.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.dataGridMojaDeca.Name = "dataGridMojaDeca";
            this.dataGridMojaDeca.ReadOnly = true;
            this.dataGridMojaDeca.RowHeadersVisible = false;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.ControlLight;
            dataGridViewCellStyle4.ForeColor = System.Drawing.Color.MidnightBlue;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.Color.LightCyan;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.Color.MidnightBlue;
            this.dataGridMojaDeca.RowsDefaultCellStyle = dataGridViewCellStyle4;
            this.dataGridMojaDeca.RowTemplate.Height = 24;
            this.dataGridMojaDeca.Size = new System.Drawing.Size(735, 121);
            this.dataGridMojaDeca.TabIndex = 30;
            this.dataGridMojaDeca.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridMojaDeca_CellClick);
            // 
            // btnRasporedCasova
            // 
            this.btnRasporedCasova.BackColor = System.Drawing.Color.PowderBlue;
            this.btnRasporedCasova.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRasporedCasova.ForeColor = System.Drawing.Color.MidnightBlue;
            this.btnRasporedCasova.Image = ((System.Drawing.Image)(resources.GetObject("btnRasporedCasova.Image")));
            this.btnRasporedCasova.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnRasporedCasova.Location = new System.Drawing.Point(36, 159);
            this.btnRasporedCasova.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnRasporedCasova.Name = "btnRasporedCasova";
            this.btnRasporedCasova.Size = new System.Drawing.Size(154, 80);
            this.btnRasporedCasova.TabIndex = 9;
            this.btnRasporedCasova.Text = "Raspored   časova";
            this.btnRasporedCasova.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnRasporedCasova.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnRasporedCasova.UseVisualStyleBackColor = false;
            this.btnRasporedCasova.Click += new System.EventHandler(this.btnRasporedCasova_Click);
            // 
            // groupBoxOcene
            // 
            this.groupBoxOcene.Controls.Add(this.labelOceneUcenikPrezime);
            this.groupBoxOcene.Controls.Add(this.label1);
            this.groupBoxOcene.Controls.Add(this.label2);
            this.groupBoxOcene.Controls.Add(this.labelOceneUcenikIme);
            this.groupBoxOcene.Controls.Add(this.dataGridOcene);
            this.groupBoxOcene.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBoxOcene.ForeColor = System.Drawing.Color.MidnightBlue;
            this.groupBoxOcene.Location = new System.Drawing.Point(38, 392);
            this.groupBoxOcene.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupBoxOcene.Name = "groupBoxOcene";
            this.groupBoxOcene.Padding = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupBoxOcene.Size = new System.Drawing.Size(722, 219);
            this.groupBoxOcene.TabIndex = 33;
            this.groupBoxOcene.TabStop = false;
            this.groupBoxOcene.Text = "Ocene";
            // 
            // labelOceneUcenikPrezime
            // 
            this.labelOceneUcenikPrezime.AutoSize = true;
            this.labelOceneUcenikPrezime.BackColor = System.Drawing.Color.Transparent;
            this.labelOceneUcenikPrezime.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelOceneUcenikPrezime.ForeColor = System.Drawing.Color.MidnightBlue;
            this.labelOceneUcenikPrezime.Location = new System.Drawing.Point(46, 134);
            this.labelOceneUcenikPrezime.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelOceneUcenikPrezime.Name = "labelOceneUcenikPrezime";
            this.labelOceneUcenikPrezime.Size = new System.Drawing.Size(65, 17);
            this.labelOceneUcenikPrezime.TabIndex = 32;
            this.labelOceneUcenikPrezime.Text = "prezime";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label1.Location = new System.Drawing.Point(31, 104);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(132, 17);
            this.label1.TabIndex = 31;
            this.label1.Text = "Prezime učenika:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label2.Location = new System.Drawing.Point(46, 37);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(90, 17);
            this.label2.TabIndex = 10;
            this.label2.Text = "Ime učenik:";
            // 
            // labelOceneUcenikIme
            // 
            this.labelOceneUcenikIme.AutoSize = true;
            this.labelOceneUcenikIme.BackColor = System.Drawing.Color.Transparent;
            this.labelOceneUcenikIme.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelOceneUcenikIme.ForeColor = System.Drawing.Color.MidnightBlue;
            this.labelOceneUcenikIme.Location = new System.Drawing.Point(62, 67);
            this.labelOceneUcenikIme.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelOceneUcenikIme.Name = "labelOceneUcenikIme";
            this.labelOceneUcenikIme.Size = new System.Drawing.Size(33, 17);
            this.labelOceneUcenikIme.TabIndex = 10;
            this.labelOceneUcenikIme.Text = "ime";
            // 
            // dataGridOcene
            // 
            this.dataGridOcene.BackgroundColor = System.Drawing.Color.White;
            this.dataGridOcene.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.MidnightBlue;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridOcene.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle5;
            this.dataGridOcene.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridOcene.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column7,
            this.Column8,
            this.Column9,
            this.Column10});
            this.dataGridOcene.EnableHeadersVisualStyles = false;
            this.dataGridOcene.GridColor = System.Drawing.Color.MediumBlue;
            this.dataGridOcene.Location = new System.Drawing.Point(196, 17);
            this.dataGridOcene.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.dataGridOcene.Name = "dataGridOcene";
            this.dataGridOcene.ReadOnly = true;
            this.dataGridOcene.RowHeadersVisible = false;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.ControlLight;
            dataGridViewCellStyle6.ForeColor = System.Drawing.Color.MidnightBlue;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.Color.LightCyan;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.Color.MidnightBlue;
            this.dataGridOcene.RowsDefaultCellStyle = dataGridViewCellStyle6;
            this.dataGridOcene.RowTemplate.Height = 24;
            this.dataGridOcene.Size = new System.Drawing.Size(501, 187);
            this.dataGridOcene.TabIndex = 30;
            // 
            // Column7
            // 
            this.Column7.HeaderText = "ID Ocene";
            this.Column7.Name = "Column7";
            this.Column7.ReadOnly = true;
            this.Column7.Width = 130;
            // 
            // Column8
            // 
            this.Column8.HeaderText = "Predmet";
            this.Column8.Name = "Column8";
            this.Column8.ReadOnly = true;
            // 
            // Column9
            // 
            this.Column9.HeaderText = "Tip ocene";
            this.Column9.Name = "Column9";
            this.Column9.ReadOnly = true;
            this.Column9.Width = 130;
            // 
            // Column10
            // 
            this.Column10.HeaderText = "Ocena";
            this.Column10.Name = "Column10";
            this.Column10.ReadOnly = true;
            // 
            // groupBoxRaspored
            // 
            this.groupBoxRaspored.Controls.Add(this.labelRasporedSmer);
            this.groupBoxRaspored.Controls.Add(this.label8);
            this.groupBoxRaspored.Controls.Add(this.label9);
            this.groupBoxRaspored.Controls.Add(this.labelRasporedOdeljenje);
            this.groupBoxRaspored.Controls.Add(this.dataGridRaspored);
            this.groupBoxRaspored.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBoxRaspored.ForeColor = System.Drawing.Color.MidnightBlue;
            this.groupBoxRaspored.Location = new System.Drawing.Point(9, 349);
            this.groupBoxRaspored.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupBoxRaspored.Name = "groupBoxRaspored";
            this.groupBoxRaspored.Padding = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupBoxRaspored.Size = new System.Drawing.Size(793, 254);
            this.groupBoxRaspored.TabIndex = 34;
            this.groupBoxRaspored.TabStop = false;
            this.groupBoxRaspored.Text = "Raspored časova";
            // 
            // labelRasporedSmer
            // 
            this.labelRasporedSmer.AutoSize = true;
            this.labelRasporedSmer.BackColor = System.Drawing.Color.Transparent;
            this.labelRasporedSmer.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelRasporedSmer.ForeColor = System.Drawing.Color.MidnightBlue;
            this.labelRasporedSmer.Location = new System.Drawing.Point(254, 29);
            this.labelRasporedSmer.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelRasporedSmer.Name = "labelRasporedSmer";
            this.labelRasporedSmer.Size = new System.Drawing.Size(65, 17);
            this.labelRasporedSmer.TabIndex = 32;
            this.labelRasporedSmer.Text = "prezime";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label8.Location = new System.Drawing.Point(206, 29);
            this.label8.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(50, 17);
            this.label8.TabIndex = 31;
            this.label8.Text = "Smer:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label9.Location = new System.Drawing.Point(47, 29);
            this.label9.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(82, 17);
            this.label9.TabIndex = 10;
            this.label9.Text = "Odeljenje:";
            // 
            // labelRasporedOdeljenje
            // 
            this.labelRasporedOdeljenje.AutoSize = true;
            this.labelRasporedOdeljenje.BackColor = System.Drawing.Color.Transparent;
            this.labelRasporedOdeljenje.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelRasporedOdeljenje.ForeColor = System.Drawing.Color.MidnightBlue;
            this.labelRasporedOdeljenje.Location = new System.Drawing.Point(141, 29);
            this.labelRasporedOdeljenje.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelRasporedOdeljenje.Name = "labelRasporedOdeljenje";
            this.labelRasporedOdeljenje.Size = new System.Drawing.Size(33, 17);
            this.labelRasporedOdeljenje.TabIndex = 10;
            this.labelRasporedOdeljenje.Text = "ime";
            // 
            // dataGridRaspored
            // 
            this.dataGridRaspored.BackgroundColor = System.Drawing.Color.White;
            this.dataGridRaspored.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle7.BackColor = System.Drawing.Color.MidnightBlue;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle7.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridRaspored.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle7;
            this.dataGridRaspored.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridRaspored.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column11,
            this.Column12,
            this.Column13,
            this.Column14,
            this.Column15});
            this.dataGridRaspored.EnableHeadersVisualStyles = false;
            this.dataGridRaspored.GridColor = System.Drawing.Color.MediumBlue;
            this.dataGridRaspored.Location = new System.Drawing.Point(0, 62);
            this.dataGridRaspored.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.dataGridRaspored.Name = "dataGridRaspored";
            this.dataGridRaspored.ReadOnly = true;
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle8.BackColor = System.Drawing.SystemColors.ControlLight;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle8.ForeColor = System.Drawing.Color.MidnightBlue;
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.Color.LightCyan;
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.Color.MidnightBlue;
            dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridRaspored.RowHeadersDefaultCellStyle = dataGridViewCellStyle8;
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle9.BackColor = System.Drawing.SystemColors.ControlLight;
            dataGridViewCellStyle9.ForeColor = System.Drawing.Color.MidnightBlue;
            dataGridViewCellStyle9.SelectionBackColor = System.Drawing.Color.LightCyan;
            dataGridViewCellStyle9.SelectionForeColor = System.Drawing.Color.MidnightBlue;
            this.dataGridRaspored.RowsDefaultCellStyle = dataGridViewCellStyle9;
            this.dataGridRaspored.RowTemplate.Height = 24;
            this.dataGridRaspored.Size = new System.Drawing.Size(784, 187);
            this.dataGridRaspored.TabIndex = 30;
            // 
            // groupBoxOpravdavanjeCasova
            // 
            this.groupBoxOpravdavanjeCasova.Controls.Add(this.label10);
            this.groupBoxOpravdavanjeCasova.Controls.Add(this.buttonOpavdajCasoveUceniku);
            this.groupBoxOpravdavanjeCasova.Controls.Add(this.numericUpDownBrojCasova);
            this.groupBoxOpravdavanjeCasova.Controls.Add(this.labelOpravdajPrezime);
            this.groupBoxOpravdavanjeCasova.Controls.Add(this.label11);
            this.groupBoxOpravdavanjeCasova.Controls.Add(this.labelOpravdajIme);
            this.groupBoxOpravdavanjeCasova.Controls.Add(this.label7);
            this.groupBoxOpravdavanjeCasova.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBoxOpravdavanjeCasova.ForeColor = System.Drawing.Color.MidnightBlue;
            this.groupBoxOpravdavanjeCasova.Location = new System.Drawing.Point(322, 336);
            this.groupBoxOpravdavanjeCasova.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupBoxOpravdavanjeCasova.Name = "groupBoxOpravdavanjeCasova";
            this.groupBoxOpravdavanjeCasova.Padding = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupBoxOpravdavanjeCasova.Size = new System.Drawing.Size(212, 248);
            this.groupBoxOpravdavanjeCasova.TabIndex = 35;
            this.groupBoxOpravdavanjeCasova.TabStop = false;
            this.groupBoxOpravdavanjeCasova.Text = "Opravdavanje časova";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label10.Location = new System.Drawing.Point(4, 141);
            this.label10.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(193, 15);
            this.label10.TabIndex = 34;
            this.label10.Text = "Broj časova za opravdavanje:";
            // 
            // buttonOpavdajCasoveUceniku
            // 
            this.buttonOpavdajCasoveUceniku.BackColor = System.Drawing.Color.PowderBlue;
            this.buttonOpavdajCasoveUceniku.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonOpavdajCasoveUceniku.ForeColor = System.Drawing.Color.MidnightBlue;
            this.buttonOpavdajCasoveUceniku.Location = new System.Drawing.Point(28, 206);
            this.buttonOpavdajCasoveUceniku.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.buttonOpavdajCasoveUceniku.Name = "buttonOpavdajCasoveUceniku";
            this.buttonOpavdajCasoveUceniku.Size = new System.Drawing.Size(157, 25);
            this.buttonOpavdajCasoveUceniku.TabIndex = 33;
            this.buttonOpavdajCasoveUceniku.Text = "Opravdaj časove";
            this.buttonOpavdajCasoveUceniku.UseVisualStyleBackColor = false;
            this.buttonOpavdajCasoveUceniku.Click += new System.EventHandler(this.buttonOpavdajCasoveUceniku_Click);
            // 
            // numericUpDownBrojCasova
            // 
            this.numericUpDownBrojCasova.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.numericUpDownBrojCasova.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.numericUpDownBrojCasova.Location = new System.Drawing.Point(76, 170);
            this.numericUpDownBrojCasova.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.numericUpDownBrojCasova.Name = "numericUpDownBrojCasova";
            this.numericUpDownBrojCasova.Size = new System.Drawing.Size(73, 19);
            this.numericUpDownBrojCasova.TabIndex = 15;
            this.numericUpDownBrojCasova.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // labelOpravdajPrezime
            // 
            this.labelOpravdajPrezime.AutoSize = true;
            this.labelOpravdajPrezime.BackColor = System.Drawing.Color.Transparent;
            this.labelOpravdajPrezime.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelOpravdajPrezime.ForeColor = System.Drawing.Color.MidnightBlue;
            this.labelOpravdajPrezime.Location = new System.Drawing.Point(73, 106);
            this.labelOpravdajPrezime.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelOpravdajPrezime.Name = "labelOpravdajPrezime";
            this.labelOpravdajPrezime.Size = new System.Drawing.Size(65, 17);
            this.labelOpravdajPrezime.TabIndex = 14;
            this.labelOpravdajPrezime.Text = "prezime";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label11.Location = new System.Drawing.Point(41, 89);
            this.label11.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(132, 17);
            this.label11.TabIndex = 13;
            this.label11.Text = "Prezime učenika:";
            // 
            // labelOpravdajIme
            // 
            this.labelOpravdajIme.AutoSize = true;
            this.labelOpravdajIme.BackColor = System.Drawing.Color.Transparent;
            this.labelOpravdajIme.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelOpravdajIme.ForeColor = System.Drawing.Color.MidnightBlue;
            this.labelOpravdajIme.Location = new System.Drawing.Point(83, 54);
            this.labelOpravdajIme.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelOpravdajIme.Name = "labelOpravdajIme";
            this.labelOpravdajIme.Size = new System.Drawing.Size(33, 17);
            this.labelOpravdajIme.TabIndex = 12;
            this.labelOpravdajIme.Text = "ime";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label7.Location = new System.Drawing.Point(56, 27);
            this.label7.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(99, 17);
            this.label7.TabIndex = 11;
            this.label7.Text = "Ime učenika:";
            // 
            // groupBoxFunkcija
            // 
            this.groupBoxFunkcija.Controls.Add(this.labelFjaKraj);
            this.groupBoxFunkcija.Controls.Add(this.label16);
            this.groupBoxFunkcija.Controls.Add(this.labelFjaPocetak);
            this.groupBoxFunkcija.Controls.Add(this.label14);
            this.groupBoxFunkcija.Controls.Add(this.labelFjaTip);
            this.groupBoxFunkcija.Controls.Add(this.label12);
            this.groupBoxFunkcija.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBoxFunkcija.ForeColor = System.Drawing.Color.MidnightBlue;
            this.groupBoxFunkcija.Location = new System.Drawing.Point(821, 353);
            this.groupBoxFunkcija.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupBoxFunkcija.Name = "groupBoxFunkcija";
            this.groupBoxFunkcija.Padding = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupBoxFunkcija.Size = new System.Drawing.Size(185, 199);
            this.groupBoxFunkcija.TabIndex = 36;
            this.groupBoxFunkcija.TabStop = false;
            this.groupBoxFunkcija.Text = "Funkcija roditelja";
            // 
            // labelFjaKraj
            // 
            this.labelFjaKraj.AutoSize = true;
            this.labelFjaKraj.BackColor = System.Drawing.Color.Transparent;
            this.labelFjaKraj.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelFjaKraj.ForeColor = System.Drawing.Color.DodgerBlue;
            this.labelFjaKraj.Location = new System.Drawing.Point(31, 163);
            this.labelFjaKraj.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelFjaKraj.Name = "labelFjaKraj";
            this.labelFjaKraj.Size = new System.Drawing.Size(28, 13);
            this.labelFjaKraj.TabIndex = 16;
            this.labelFjaKraj.Text = "kraj";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.BackColor = System.Drawing.Color.Transparent;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.MidnightBlue;
            this.label16.Location = new System.Drawing.Point(9, 134);
            this.label16.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(90, 13);
            this.label16.TabIndex = 15;
            this.label16.Text = "Na funkciji do:";
            // 
            // labelFjaPocetak
            // 
            this.labelFjaPocetak.AutoSize = true;
            this.labelFjaPocetak.BackColor = System.Drawing.Color.Transparent;
            this.labelFjaPocetak.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelFjaPocetak.ForeColor = System.Drawing.Color.DodgerBlue;
            this.labelFjaPocetak.Location = new System.Drawing.Point(31, 104);
            this.labelFjaPocetak.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelFjaPocetak.Name = "labelFjaPocetak";
            this.labelFjaPocetak.Size = new System.Drawing.Size(53, 13);
            this.labelFjaPocetak.TabIndex = 14;
            this.labelFjaPocetak.Text = "pocetak";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.BackColor = System.Drawing.Color.Transparent;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.MidnightBlue;
            this.label14.Location = new System.Drawing.Point(9, 82);
            this.label14.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(152, 13);
            this.label14.TabIndex = 13;
            this.label14.Text = "Početak vršenja funkcije:";
            // 
            // labelFjaTip
            // 
            this.labelFjaTip.AutoSize = true;
            this.labelFjaTip.BackColor = System.Drawing.Color.Transparent;
            this.labelFjaTip.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelFjaTip.ForeColor = System.Drawing.Color.DodgerBlue;
            this.labelFjaTip.Location = new System.Drawing.Point(31, 54);
            this.labelFjaTip.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelFjaTip.Name = "labelFjaTip";
            this.labelFjaTip.Size = new System.Drawing.Size(21, 13);
            this.labelFjaTip.TabIndex = 12;
            this.labelFjaTip.Text = "fja";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.Color.Transparent;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.MidnightBlue;
            this.label12.Location = new System.Drawing.Point(9, 31);
            this.label12.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(78, 13);
            this.label12.TabIndex = 11;
            this.label12.Text = "Tip funkcije:";
            // 
            // groupBoxPredstavljanje
            // 
            this.groupBoxPredstavljanje.Controls.Add(this.labelPredtsavljaOdeljenjeSmer);
            this.groupBoxPredstavljanje.Controls.Add(this.label21);
            this.groupBoxPredstavljanje.Controls.Add(this.labelPredstavljanjeKraj);
            this.groupBoxPredstavljanje.Controls.Add(this.label15);
            this.groupBoxPredstavljanje.Controls.Add(this.labelPredstavljanjePocetak);
            this.groupBoxPredstavljanje.Controls.Add(this.label18);
            this.groupBoxPredstavljanje.Controls.Add(this.labelPredstavljaOdeljenje);
            this.groupBoxPredstavljanje.Controls.Add(this.label20);
            this.groupBoxPredstavljanje.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBoxPredstavljanje.ForeColor = System.Drawing.Color.MidnightBlue;
            this.groupBoxPredstavljanje.Location = new System.Drawing.Point(821, 67);
            this.groupBoxPredstavljanje.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupBoxPredstavljanje.Name = "groupBoxPredstavljanje";
            this.groupBoxPredstavljanje.Padding = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupBoxPredstavljanje.Size = new System.Drawing.Size(185, 256);
            this.groupBoxPredstavljanje.TabIndex = 37;
            this.groupBoxPredstavljanje.TabStop = false;
            this.groupBoxPredstavljanje.Text = "Predstavnik odeljenja";
            // 
            // labelPredtsavljaOdeljenjeSmer
            // 
            this.labelPredtsavljaOdeljenjeSmer.AutoSize = true;
            this.labelPredtsavljaOdeljenjeSmer.BackColor = System.Drawing.Color.Transparent;
            this.labelPredtsavljaOdeljenjeSmer.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPredtsavljaOdeljenjeSmer.ForeColor = System.Drawing.Color.DodgerBlue;
            this.labelPredtsavljaOdeljenjeSmer.Location = new System.Drawing.Point(93, 67);
            this.labelPredtsavljaOdeljenjeSmer.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelPredtsavljaOdeljenjeSmer.Name = "labelPredtsavljaOdeljenjeSmer";
            this.labelPredtsavljaOdeljenjeSmer.Size = new System.Drawing.Size(33, 13);
            this.labelPredtsavljaOdeljenjeSmer.TabIndex = 18;
            this.labelPredtsavljaOdeljenjeSmer.Text = "smer";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.BackColor = System.Drawing.Color.Transparent;
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.ForeColor = System.Drawing.Color.MidnightBlue;
            this.label21.Location = new System.Drawing.Point(44, 67);
            this.label21.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(39, 13);
            this.label21.TabIndex = 17;
            this.label21.Text = "Smer:";
            // 
            // labelPredstavljanjeKraj
            // 
            this.labelPredstavljanjeKraj.AutoSize = true;
            this.labelPredstavljanjeKraj.BackColor = System.Drawing.Color.Transparent;
            this.labelPredstavljanjeKraj.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPredstavljanjeKraj.ForeColor = System.Drawing.Color.DodgerBlue;
            this.labelPredstavljanjeKraj.Location = new System.Drawing.Point(44, 201);
            this.labelPredstavljanjeKraj.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelPredstavljanjeKraj.Name = "labelPredstavljanjeKraj";
            this.labelPredstavljanjeKraj.Size = new System.Drawing.Size(28, 13);
            this.labelPredstavljanjeKraj.TabIndex = 16;
            this.labelPredstavljanjeKraj.Text = "kraj";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.BackColor = System.Drawing.Color.Transparent;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.MidnightBlue;
            this.label15.Location = new System.Drawing.Point(18, 174);
            this.label15.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(141, 13);
            this.label15.TabIndex = 15;
            this.label15.Text = "Predstava odeljenje do:";
            // 
            // labelPredstavljanjePocetak
            // 
            this.labelPredstavljanjePocetak.AutoSize = true;
            this.labelPredstavljanjePocetak.BackColor = System.Drawing.Color.Transparent;
            this.labelPredstavljanjePocetak.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPredstavljanjePocetak.ForeColor = System.Drawing.Color.DodgerBlue;
            this.labelPredstavljanjePocetak.Location = new System.Drawing.Point(44, 141);
            this.labelPredstavljanjePocetak.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelPredstavljanjePocetak.Name = "labelPredstavljanjePocetak";
            this.labelPredstavljanjePocetak.Size = new System.Drawing.Size(53, 13);
            this.labelPredstavljanjePocetak.TabIndex = 14;
            this.labelPredstavljanjePocetak.Text = "pocetak";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.BackColor = System.Drawing.Color.Transparent;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.Color.MidnightBlue;
            this.label18.Location = new System.Drawing.Point(18, 109);
            this.label18.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(141, 13);
            this.label18.TabIndex = 13;
            this.label18.Text = "Početak predstavljanja:";
            // 
            // labelPredstavljaOdeljenje
            // 
            this.labelPredstavljaOdeljenje.AutoSize = true;
            this.labelPredstavljaOdeljenje.BackColor = System.Drawing.Color.Transparent;
            this.labelPredstavljaOdeljenje.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPredstavljaOdeljenje.ForeColor = System.Drawing.Color.DodgerBlue;
            this.labelPredstavljaOdeljenje.Location = new System.Drawing.Point(132, 31);
            this.labelPredstavljaOdeljenje.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelPredstavljaOdeljenje.Name = "labelPredstavljaOdeljenje";
            this.labelPredstavljaOdeljenje.Size = new System.Drawing.Size(34, 13);
            this.labelPredstavljaOdeljenje.TabIndex = 12;
            this.labelPredstavljaOdeljenje.Text = "odelj";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.BackColor = System.Drawing.Color.Transparent;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.ForeColor = System.Drawing.Color.MidnightBlue;
            this.label20.Location = new System.Drawing.Point(9, 31);
            this.label20.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(129, 13);
            this.label20.TabIndex = 11;
            this.label20.Text = "Predstavlja odeljenje:";
            // 
            // Column11
            // 
            this.Column11.HeaderText = "Ponedeljak";
            this.Column11.Name = "Column11";
            this.Column11.ReadOnly = true;
            this.Column11.Width = 145;
            // 
            // Column12
            // 
            this.Column12.HeaderText = "Utorak";
            this.Column12.Name = "Column12";
            this.Column12.ReadOnly = true;
            this.Column12.Width = 145;
            // 
            // Column13
            // 
            this.Column13.HeaderText = "Sreda";
            this.Column13.Name = "Column13";
            this.Column13.ReadOnly = true;
            this.Column13.Width = 145;
            // 
            // Column14
            // 
            this.Column14.HeaderText = "Cetvrtak";
            this.Column14.Name = "Column14";
            this.Column14.ReadOnly = true;
            this.Column14.Width = 145;
            // 
            // Column15
            // 
            this.Column15.HeaderText = "Petak";
            this.Column15.Name = "Column15";
            this.Column15.ReadOnly = true;
            this.Column15.Width = 145;
            // 
            // Column4
            // 
            this.Column4.HeaderText = "ID Ucenika";
            this.Column4.Name = "Column4";
            this.Column4.ReadOnly = true;
            this.Column4.Width = 110;
            // 
            // Column5
            // 
            this.Column5.HeaderText = "Ime";
            this.Column5.Name = "Column5";
            this.Column5.ReadOnly = true;
            // 
            // Column6
            // 
            this.Column6.HeaderText = "Prezime";
            this.Column6.Name = "Column6";
            this.Column6.ReadOnly = true;
            // 
            // Odeljenje
            // 
            this.Odeljenje.HeaderText = "Odeljenje";
            this.Odeljenje.Name = "Odeljenje";
            this.Odeljenje.ReadOnly = true;
            this.Odeljenje.Width = 90;
            // 
            // Column1
            // 
            this.Column1.HeaderText = "Smer";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            // 
            // Column2
            // 
            this.Column2.HeaderText = "Neopravdani";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            // 
            // Column3
            // 
            this.Column3.HeaderText = "Opravdani";
            this.Column3.Name = "Column3";
            this.Column3.ReadOnly = true;
            // 
            // FormaRoditeljPocetna
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1017, 609);
            this.Controls.Add(this.groupBoxPredstavljanje);
            this.Controls.Add(this.groupBoxFunkcija);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.panelPrijavljeniNastavnik);
            this.Controls.Add(this.groupBoxRaspored);
            this.Controls.Add(this.groupBoxOpravdavanjeCasova);
            this.Controls.Add(this.groupBoxOcene);
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Name = "FormaRoditeljPocetna";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "e-Dnevnik";
            this.Load += new System.EventHandler(this.FormaRoditeljPocetna_Load);
            this.panelPrijavljeniNastavnik.ResumeLayout(false);
            this.panelPrijavljeniNastavnik.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridMojaDeca)).EndInit();
            this.groupBoxOcene.ResumeLayout(false);
            this.groupBoxOcene.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridOcene)).EndInit();
            this.groupBoxRaspored.ResumeLayout(false);
            this.groupBoxRaspored.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridRaspored)).EndInit();
            this.groupBoxOpravdavanjeCasova.ResumeLayout(false);
            this.groupBoxOpravdavanjeCasova.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownBrojCasova)).EndInit();
            this.groupBoxFunkcija.ResumeLayout(false);
            this.groupBoxFunkcija.PerformLayout();
            this.groupBoxPredstavljanje.ResumeLayout(false);
            this.groupBoxPredstavljanje.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelPrijavljeniNastavnik;
        private System.Windows.Forms.Label txtImePrezimeRoditelja;
        private System.Windows.Forms.Label txt1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.DataGridView dataGridMojaDeca;
        private System.Windows.Forms.Button btnRasporedCasova;
        private System.Windows.Forms.Button btnOcene;
        private System.Windows.Forms.GroupBox groupBoxOcene;
        private System.Windows.Forms.Label labelOceneUcenikPrezime;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label labelOceneUcenikIme;
        private System.Windows.Forms.DataGridView dataGridOcene;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column7;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column8;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column9;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column10;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.GroupBox groupBoxRaspored;
        private System.Windows.Forms.Label labelRasporedSmer;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label labelRasporedOdeljenje;
        private System.Windows.Forms.DataGridView dataGridRaspored;
        private System.Windows.Forms.GroupBox groupBoxOpravdavanjeCasova;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Button buttonOpavdajCasoveUceniku;
        private System.Windows.Forms.NumericUpDown numericUpDownBrojCasova;
        private System.Windows.Forms.Label labelOpravdajPrezime;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label labelOpravdajIme;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button btnOpravdajCasove;
        private System.Windows.Forms.GroupBox groupBoxFunkcija;
        private System.Windows.Forms.Label labelFjaKraj;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label labelFjaPocetak;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label labelFjaTip;
        private System.Windows.Forms.Label label12;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.GroupBox groupBoxPredstavljanje;
        private System.Windows.Forms.Label labelPredtsavljaOdeljenjeSmer;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label labelPredstavljanjeKraj;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label labelPredstavljanjePocetak;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label labelPredstavljaOdeljenje;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column6;
        private System.Windows.Forms.DataGridViewTextBoxColumn Odeljenje;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column11;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column12;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column13;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column14;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column15;
    }
}