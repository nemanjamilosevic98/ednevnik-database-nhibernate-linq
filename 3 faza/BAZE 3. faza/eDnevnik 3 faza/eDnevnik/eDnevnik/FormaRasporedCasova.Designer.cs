﻿namespace eDnevnik
{
    partial class FormaRasporedCasova
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.groupBoxRaspored = new System.Windows.Forms.GroupBox();
            this.btnUkloniCasOpcija = new System.Windows.Forms.Button();
            this.labelRasporedSmer = new System.Windows.Forms.Label();
            this.btnIzmeniCasOpcija = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.btnDodajCasOpcija = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.labelRasporedOdeljenje = new System.Windows.Forms.Label();
            this.dataGridRaspored = new System.Windows.Forms.DataGridView();
            this.groupBoxDodavanjeCasa = new System.Windows.Forms.GroupBox();
            this.btnDodajCas = new System.Windows.Forms.Button();
            this.cbxBrojCasa = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.cbxDani = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.cbxPredmeti = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.Column11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column12 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column13 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column14 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column15 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBoxRaspored.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridRaspored)).BeginInit();
            this.groupBoxDodavanjeCasa.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBoxRaspored
            // 
            this.groupBoxRaspored.Controls.Add(this.btnUkloniCasOpcija);
            this.groupBoxRaspored.Controls.Add(this.labelRasporedSmer);
            this.groupBoxRaspored.Controls.Add(this.btnIzmeniCasOpcija);
            this.groupBoxRaspored.Controls.Add(this.label8);
            this.groupBoxRaspored.Controls.Add(this.btnDodajCasOpcija);
            this.groupBoxRaspored.Controls.Add(this.label9);
            this.groupBoxRaspored.Controls.Add(this.labelRasporedOdeljenje);
            this.groupBoxRaspored.Controls.Add(this.dataGridRaspored);
            this.groupBoxRaspored.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBoxRaspored.ForeColor = System.Drawing.Color.MidnightBlue;
            this.groupBoxRaspored.Location = new System.Drawing.Point(15, 10);
            this.groupBoxRaspored.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupBoxRaspored.Name = "groupBoxRaspored";
            this.groupBoxRaspored.Padding = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupBoxRaspored.Size = new System.Drawing.Size(777, 366);
            this.groupBoxRaspored.TabIndex = 35;
            this.groupBoxRaspored.TabStop = false;
            this.groupBoxRaspored.Text = "Raspored časova";
            // 
            // btnUkloniCasOpcija
            // 
            this.btnUkloniCasOpcija.BackColor = System.Drawing.Color.PowderBlue;
            this.btnUkloniCasOpcija.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUkloniCasOpcija.ForeColor = System.Drawing.Color.MidnightBlue;
            this.btnUkloniCasOpcija.Location = new System.Drawing.Point(641, 328);
            this.btnUkloniCasOpcija.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnUkloniCasOpcija.Name = "btnUkloniCasOpcija";
            this.btnUkloniCasOpcija.Size = new System.Drawing.Size(131, 33);
            this.btnUkloniCasOpcija.TabIndex = 38;
            this.btnUkloniCasOpcija.Text = "Ukloni čas";
            this.btnUkloniCasOpcija.UseVisualStyleBackColor = false;
            this.btnUkloniCasOpcija.Click += new System.EventHandler(this.btnUkloniCasOpcija_Click);
            // 
            // labelRasporedSmer
            // 
            this.labelRasporedSmer.AutoSize = true;
            this.labelRasporedSmer.BackColor = System.Drawing.Color.Transparent;
            this.labelRasporedSmer.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelRasporedSmer.ForeColor = System.Drawing.Color.MidnightBlue;
            this.labelRasporedSmer.Location = new System.Drawing.Point(268, 29);
            this.labelRasporedSmer.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelRasporedSmer.Name = "labelRasporedSmer";
            this.labelRasporedSmer.Size = new System.Drawing.Size(43, 17);
            this.labelRasporedSmer.TabIndex = 32;
            this.labelRasporedSmer.Text = "smer";
            // 
            // btnIzmeniCasOpcija
            // 
            this.btnIzmeniCasOpcija.BackColor = System.Drawing.Color.PowderBlue;
            this.btnIzmeniCasOpcija.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnIzmeniCasOpcija.ForeColor = System.Drawing.Color.MidnightBlue;
            this.btnIzmeniCasOpcija.Location = new System.Drawing.Point(325, 328);
            this.btnIzmeniCasOpcija.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnIzmeniCasOpcija.Name = "btnIzmeniCasOpcija";
            this.btnIzmeniCasOpcija.Size = new System.Drawing.Size(131, 33);
            this.btnIzmeniCasOpcija.TabIndex = 37;
            this.btnIzmeniCasOpcija.Text = "Izmeni čas";
            this.btnIzmeniCasOpcija.UseVisualStyleBackColor = false;
            this.btnIzmeniCasOpcija.Click += new System.EventHandler(this.btnIzmeniCasOpcija_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label8.Location = new System.Drawing.Point(206, 29);
            this.label8.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(50, 17);
            this.label8.TabIndex = 31;
            this.label8.Text = "Smer:";
            // 
            // btnDodajCasOpcija
            // 
            this.btnDodajCasOpcija.BackColor = System.Drawing.Color.PowderBlue;
            this.btnDodajCasOpcija.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDodajCasOpcija.ForeColor = System.Drawing.Color.MidnightBlue;
            this.btnDodajCasOpcija.Location = new System.Drawing.Point(4, 328);
            this.btnDodajCasOpcija.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnDodajCasOpcija.Name = "btnDodajCasOpcija";
            this.btnDodajCasOpcija.Size = new System.Drawing.Size(131, 33);
            this.btnDodajCasOpcija.TabIndex = 36;
            this.btnDodajCasOpcija.Text = "Dodaj čas";
            this.btnDodajCasOpcija.UseVisualStyleBackColor = false;
            this.btnDodajCasOpcija.Click += new System.EventHandler(this.btnDodajCasOpcija_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label9.Location = new System.Drawing.Point(47, 29);
            this.label9.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(82, 17);
            this.label9.TabIndex = 10;
            this.label9.Text = "Odeljenje:";
            // 
            // labelRasporedOdeljenje
            // 
            this.labelRasporedOdeljenje.AutoSize = true;
            this.labelRasporedOdeljenje.BackColor = System.Drawing.Color.Transparent;
            this.labelRasporedOdeljenje.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelRasporedOdeljenje.ForeColor = System.Drawing.Color.MidnightBlue;
            this.labelRasporedOdeljenje.Location = new System.Drawing.Point(141, 29);
            this.labelRasporedOdeljenje.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelRasporedOdeljenje.Name = "labelRasporedOdeljenje";
            this.labelRasporedOdeljenje.Size = new System.Drawing.Size(46, 17);
            this.labelRasporedOdeljenje.TabIndex = 10;
            this.labelRasporedOdeljenje.Text = "naziv";
            // 
            // dataGridRaspored
            // 
            this.dataGridRaspored.BackgroundColor = System.Drawing.Color.White;
            this.dataGridRaspored.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.MidnightBlue;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridRaspored.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridRaspored.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridRaspored.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column11,
            this.Column12,
            this.Column13,
            this.Column14,
            this.Column15});
            this.dataGridRaspored.EnableHeadersVisualStyles = false;
            this.dataGridRaspored.GridColor = System.Drawing.Color.MediumBlue;
            this.dataGridRaspored.Location = new System.Drawing.Point(4, 62);
            this.dataGridRaspored.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.dataGridRaspored.Name = "dataGridRaspored";
            this.dataGridRaspored.ReadOnly = true;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.ControlLight;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.MidnightBlue;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.LightCyan;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.MidnightBlue;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridRaspored.RowHeadersDefaultCellStyle = dataGridViewCellStyle2;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.ControlLight;
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.MidnightBlue;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.LightCyan;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.MidnightBlue;
            this.dataGridRaspored.RowsDefaultCellStyle = dataGridViewCellStyle3;
            this.dataGridRaspored.RowTemplate.Height = 24;
            this.dataGridRaspored.Size = new System.Drawing.Size(768, 249);
            this.dataGridRaspored.TabIndex = 30;
            // 
            // groupBoxDodavanjeCasa
            // 
            this.groupBoxDodavanjeCasa.Controls.Add(this.btnDodajCas);
            this.groupBoxDodavanjeCasa.Controls.Add(this.cbxBrojCasa);
            this.groupBoxDodavanjeCasa.Controls.Add(this.label3);
            this.groupBoxDodavanjeCasa.Controls.Add(this.cbxDani);
            this.groupBoxDodavanjeCasa.Controls.Add(this.label2);
            this.groupBoxDodavanjeCasa.Controls.Add(this.cbxPredmeti);
            this.groupBoxDodavanjeCasa.Controls.Add(this.label1);
            this.groupBoxDodavanjeCasa.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBoxDodavanjeCasa.ForeColor = System.Drawing.Color.MidnightBlue;
            this.groupBoxDodavanjeCasa.Location = new System.Drawing.Point(802, 10);
            this.groupBoxDodavanjeCasa.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupBoxDodavanjeCasa.Name = "groupBoxDodavanjeCasa";
            this.groupBoxDodavanjeCasa.Padding = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupBoxDodavanjeCasa.Size = new System.Drawing.Size(201, 366);
            this.groupBoxDodavanjeCasa.TabIndex = 36;
            this.groupBoxDodavanjeCasa.TabStop = false;
            this.groupBoxDodavanjeCasa.Text = "Dodavanje časa";
            // 
            // btnDodajCas
            // 
            this.btnDodajCas.BackColor = System.Drawing.Color.PowderBlue;
            this.btnDodajCas.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDodajCas.ForeColor = System.Drawing.Color.MidnightBlue;
            this.btnDodajCas.Location = new System.Drawing.Point(47, 307);
            this.btnDodajCas.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnDodajCas.Name = "btnDodajCas";
            this.btnDodajCas.Size = new System.Drawing.Size(113, 33);
            this.btnDodajCas.TabIndex = 39;
            this.btnDodajCas.Text = "Dodaj čas";
            this.btnDodajCas.UseVisualStyleBackColor = false;
            this.btnDodajCas.Click += new System.EventHandler(this.btnDodajCas_Click);
            // 
            // cbxBrojCasa
            // 
            this.cbxBrojCasa.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.cbxBrojCasa.ForeColor = System.Drawing.Color.MidnightBlue;
            this.cbxBrojCasa.FormattingEnabled = true;
            this.cbxBrojCasa.Location = new System.Drawing.Point(23, 242);
            this.cbxBrojCasa.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.cbxBrojCasa.Name = "cbxBrojCasa";
            this.cbxBrojCasa.Size = new System.Drawing.Size(164, 21);
            this.cbxBrojCasa.TabIndex = 16;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.MidnightBlue;
            this.label3.Location = new System.Drawing.Point(29, 202);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(148, 17);
            this.label3.TabIndex = 15;
            this.label3.Text = "Izaberite broj časa:";
            // 
            // cbxDani
            // 
            this.cbxDani.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.cbxDani.ForeColor = System.Drawing.Color.MidnightBlue;
            this.cbxDani.FormattingEnabled = true;
            this.cbxDani.Items.AddRange(new object[] {
            "ponedeljak",
            "utorak",
            "sreda",
            "cetvrtak",
            "petak"});
            this.cbxDani.Location = new System.Drawing.Point(22, 153);
            this.cbxDani.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.cbxDani.Name = "cbxDani";
            this.cbxDani.Size = new System.Drawing.Size(164, 21);
            this.cbxDani.TabIndex = 14;
            this.cbxDani.SelectedIndexChanged += new System.EventHandler(this.cbxDani_SelectedIndexChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.MidnightBlue;
            this.label2.Location = new System.Drawing.Point(44, 119);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(108, 17);
            this.label2.TabIndex = 13;
            this.label2.Text = "Izaberite dan:";
            // 
            // cbxPredmeti
            // 
            this.cbxPredmeti.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.cbxPredmeti.ForeColor = System.Drawing.Color.MidnightBlue;
            this.cbxPredmeti.FormattingEnabled = true;
            this.cbxPredmeti.Location = new System.Drawing.Point(20, 74);
            this.cbxPredmeti.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.cbxPredmeti.Name = "cbxPredmeti";
            this.cbxPredmeti.Size = new System.Drawing.Size(164, 21);
            this.cbxPredmeti.TabIndex = 12;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.MidnightBlue;
            this.label1.Location = new System.Drawing.Point(29, 39);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(148, 17);
            this.label1.TabIndex = 11;
            this.label1.Text = "Predmeti odeljenja:";
            // 
            // Column11
            // 
            this.Column11.HeaderText = "Ponedeljak";
            this.Column11.Name = "Column11";
            this.Column11.ReadOnly = true;
            this.Column11.Width = 140;
            // 
            // Column12
            // 
            this.Column12.HeaderText = "Utorak";
            this.Column12.Name = "Column12";
            this.Column12.ReadOnly = true;
            this.Column12.Width = 140;
            // 
            // Column13
            // 
            this.Column13.HeaderText = "Sreda";
            this.Column13.Name = "Column13";
            this.Column13.ReadOnly = true;
            this.Column13.Width = 140;
            // 
            // Column14
            // 
            this.Column14.HeaderText = "Četvrtak";
            this.Column14.Name = "Column14";
            this.Column14.ReadOnly = true;
            this.Column14.Width = 140;
            // 
            // Column15
            // 
            this.Column15.HeaderText = "Petak";
            this.Column15.Name = "Column15";
            this.Column15.ReadOnly = true;
            this.Column15.Width = 140;
            // 
            // FormaRasporedCasova
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1028, 392);
            this.Controls.Add(this.groupBoxDodavanjeCasa);
            this.Controls.Add(this.groupBoxRaspored);
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Name = "FormaRasporedCasova";
            this.Text = "e-Dnevnik";
            this.Load += new System.EventHandler(this.FormaRasporedCasova_Load);
            this.groupBoxRaspored.ResumeLayout(false);
            this.groupBoxRaspored.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridRaspored)).EndInit();
            this.groupBoxDodavanjeCasa.ResumeLayout(false);
            this.groupBoxDodavanjeCasa.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBoxRaspored;
        private System.Windows.Forms.Label labelRasporedSmer;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label labelRasporedOdeljenje;
        private System.Windows.Forms.DataGridView dataGridRaspored;
        private System.Windows.Forms.Button btnUkloniCasOpcija;
        private System.Windows.Forms.Button btnIzmeniCasOpcija;
        private System.Windows.Forms.Button btnDodajCasOpcija;
        private System.Windows.Forms.GroupBox groupBoxDodavanjeCasa;
        private System.Windows.Forms.Button btnDodajCas;
        private System.Windows.Forms.ComboBox cbxBrojCasa;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cbxDani;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cbxPredmeti;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column11;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column12;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column13;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column14;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column15;
    }
}