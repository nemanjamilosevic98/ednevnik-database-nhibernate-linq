﻿namespace eDnevnik
{
    partial class FormaNastavnikovoOdeljenje
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            this.labelOdeljenjeNaziv = new System.Windows.Forms.Label();
            this.labelOdeljenjeSmer = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.cbxPredajemPredmete = new System.Windows.Forms.ComboBox();
            this.cbxUceniciOdeljenja = new System.Windows.Forms.ComboBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnPodaciUcenika = new System.Windows.Forms.Button();
            this.groupBoxIzabraniUcenik = new System.Windows.Forms.GroupBox();
            this.groupBoxIzmeniOcenu = new System.Windows.Forms.GroupBox();
            this.labelOcenaIdKojaSeMenja = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.btnIzmeniOcenu = new System.Windows.Forms.Button();
            this.txtIzmenjenaOcena = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.groupBoxUkloniOcenu = new System.Windows.Forms.GroupBox();
            this.btnUkloniOcenu = new System.Windows.Forms.Button();
            this.txtUkloniOcenuId = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.groupBoxDodajOcenu = new System.Windows.Forms.GroupBox();
            this.btnDodajOcenu = new System.Windows.Forms.Button();
            this.txtOcena = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.cbxDodajTipOcene = new System.Windows.Forms.ComboBox();
            this.btnOpcijaIzmeniOcenu = new System.Windows.Forms.Button();
            this.btnOpcijaUkloniOcenu = new System.Windows.Forms.Button();
            this.btnOpcijaDodajOcenu = new System.Windows.Forms.Button();
            this.labelIzabraniUcenikPredmet = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.dataGridOceneUcenika = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.labelIzabraniUcenikNeopravdani = new System.Windows.Forms.Label();
            this.labelIzabraniUcenikOpravdani = new System.Windows.Forms.Label();
            this.labelIzabraniUcnikOdeljenje = new System.Windows.Forms.Label();
            this.labelIzabraniUcenikPrezime = new System.Windows.Forms.Label();
            this.labelIzabraniUcenikIme = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.labelObavestenje = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.dataGridOdsutniUcenici = new System.Windows.Forms.DataGridView();
            this.btnUpisOdsutnih = new System.Windows.Forms.Button();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column7 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.groupBox1.SuspendLayout();
            this.groupBoxIzabraniUcenik.SuspendLayout();
            this.groupBoxIzmeniOcenu.SuspendLayout();
            this.groupBoxUkloniOcenu.SuspendLayout();
            this.groupBoxDodajOcenu.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridOceneUcenika)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridOdsutniUcenici)).BeginInit();
            this.SuspendLayout();
            // 
            // labelOdeljenjeNaziv
            // 
            this.labelOdeljenjeNaziv.AutoSize = true;
            this.labelOdeljenjeNaziv.BackColor = System.Drawing.Color.Transparent;
            this.labelOdeljenjeNaziv.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelOdeljenjeNaziv.ForeColor = System.Drawing.Color.MidnightBlue;
            this.labelOdeljenjeNaziv.Location = new System.Drawing.Point(26, 28);
            this.labelOdeljenjeNaziv.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelOdeljenjeNaziv.Name = "labelOdeljenjeNaziv";
            this.labelOdeljenjeNaziv.Size = new System.Drawing.Size(82, 17);
            this.labelOdeljenjeNaziv.TabIndex = 3;
            this.labelOdeljenjeNaziv.Text = "Odeljenje:";
            // 
            // labelOdeljenjeSmer
            // 
            this.labelOdeljenjeSmer.AutoSize = true;
            this.labelOdeljenjeSmer.BackColor = System.Drawing.Color.Transparent;
            this.labelOdeljenjeSmer.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelOdeljenjeSmer.ForeColor = System.Drawing.Color.MidnightBlue;
            this.labelOdeljenjeSmer.Location = new System.Drawing.Point(225, 28);
            this.labelOdeljenjeSmer.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelOdeljenjeSmer.Name = "labelOdeljenjeSmer";
            this.labelOdeljenjeSmer.Size = new System.Drawing.Size(50, 17);
            this.labelOdeljenjeSmer.TabIndex = 4;
            this.labelOdeljenjeSmer.Text = "Smer:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label2.Location = new System.Drawing.Point(20, 107);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(241, 17);
            this.label2.TabIndex = 5;
            this.label2.Text = "Spisak svih učenika u odeljenju:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label3.Location = new System.Drawing.Point(20, 72);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(255, 17);
            this.label3.TabIndex = 6;
            this.label3.Text = "Predmeti koje predajem odeljenju:";
            // 
            // cbxPredajemPredmete
            // 
            this.cbxPredajemPredmete.FormattingEnabled = true;
            this.cbxPredajemPredmete.Location = new System.Drawing.Point(280, 68);
            this.cbxPredajemPredmete.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.cbxPredajemPredmete.Name = "cbxPredajemPredmete";
            this.cbxPredajemPredmete.Size = new System.Drawing.Size(159, 21);
            this.cbxPredajemPredmete.TabIndex = 7;
            // 
            // cbxUceniciOdeljenja
            // 
            this.cbxUceniciOdeljenja.FormattingEnabled = true;
            this.cbxUceniciOdeljenja.Location = new System.Drawing.Point(280, 107);
            this.cbxUceniciOdeljenja.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.cbxUceniciOdeljenja.Name = "cbxUceniciOdeljenja";
            this.cbxUceniciOdeljenja.Size = new System.Drawing.Size(159, 21);
            this.cbxUceniciOdeljenja.TabIndex = 8;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnPodaciUcenika);
            this.groupBox1.Controls.Add(this.labelOdeljenjeNaziv);
            this.groupBox1.Controls.Add(this.cbxUceniciOdeljenja);
            this.groupBox1.Controls.Add(this.labelOdeljenjeSmer);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.cbxPredajemPredmete);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.ForeColor = System.Drawing.Color.MidnightBlue;
            this.groupBox1.Location = new System.Drawing.Point(9, 22);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupBox1.Size = new System.Drawing.Size(455, 193);
            this.groupBox1.TabIndex = 9;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Odeljenje kojem predajem";
            // 
            // btnPodaciUcenika
            // 
            this.btnPodaciUcenika.BackColor = System.Drawing.Color.PowderBlue;
            this.btnPodaciUcenika.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPodaciUcenika.ForeColor = System.Drawing.Color.MidnightBlue;
            this.btnPodaciUcenika.Location = new System.Drawing.Point(291, 146);
            this.btnPodaciUcenika.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnPodaciUcenika.Name = "btnPodaciUcenika";
            this.btnPodaciUcenika.Size = new System.Drawing.Size(131, 33);
            this.btnPodaciUcenika.TabIndex = 9;
            this.btnPodaciUcenika.Text = "Podaci o učeniku";
            this.btnPodaciUcenika.UseVisualStyleBackColor = false;
            this.btnPodaciUcenika.Click += new System.EventHandler(this.btnPodaciUcenika_Click);
            // 
            // groupBoxIzabraniUcenik
            // 
            this.groupBoxIzabraniUcenik.Controls.Add(this.groupBoxUkloniOcenu);
            this.groupBoxIzabraniUcenik.Controls.Add(this.groupBoxDodajOcenu);
            this.groupBoxIzabraniUcenik.Controls.Add(this.dataGridOceneUcenika);
            this.groupBoxIzabraniUcenik.Controls.Add(this.groupBoxIzmeniOcenu);
            this.groupBoxIzabraniUcenik.Controls.Add(this.btnOpcijaIzmeniOcenu);
            this.groupBoxIzabraniUcenik.Controls.Add(this.btnOpcijaUkloniOcenu);
            this.groupBoxIzabraniUcenik.Controls.Add(this.btnOpcijaDodajOcenu);
            this.groupBoxIzabraniUcenik.Controls.Add(this.labelIzabraniUcenikPredmet);
            this.groupBoxIzabraniUcenik.Controls.Add(this.label8);
            this.groupBoxIzabraniUcenik.Controls.Add(this.labelIzabraniUcenikNeopravdani);
            this.groupBoxIzabraniUcenik.Controls.Add(this.labelIzabraniUcenikOpravdani);
            this.groupBoxIzabraniUcenik.Controls.Add(this.labelIzabraniUcnikOdeljenje);
            this.groupBoxIzabraniUcenik.Controls.Add(this.labelIzabraniUcenikPrezime);
            this.groupBoxIzabraniUcenik.Controls.Add(this.labelIzabraniUcenikIme);
            this.groupBoxIzabraniUcenik.Controls.Add(this.label7);
            this.groupBoxIzabraniUcenik.Controls.Add(this.label6);
            this.groupBoxIzabraniUcenik.Controls.Add(this.label5);
            this.groupBoxIzabraniUcenik.Controls.Add(this.label1);
            this.groupBoxIzabraniUcenik.Controls.Add(this.label4);
            this.groupBoxIzabraniUcenik.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBoxIzabraniUcenik.ForeColor = System.Drawing.Color.MidnightBlue;
            this.groupBoxIzabraniUcenik.Location = new System.Drawing.Point(9, 220);
            this.groupBoxIzabraniUcenik.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupBoxIzabraniUcenik.Name = "groupBoxIzabraniUcenik";
            this.groupBoxIzabraniUcenik.Padding = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupBoxIzabraniUcenik.Size = new System.Drawing.Size(883, 315);
            this.groupBoxIzabraniUcenik.TabIndex = 10;
            this.groupBoxIzabraniUcenik.TabStop = false;
            this.groupBoxIzabraniUcenik.Text = "Izabrani učenik";
            // 
            // groupBoxIzmeniOcenu
            // 
            this.groupBoxIzmeniOcenu.Controls.Add(this.labelOcenaIdKojaSeMenja);
            this.groupBoxIzmeniOcenu.Controls.Add(this.label13);
            this.groupBoxIzmeniOcenu.Controls.Add(this.btnIzmeniOcenu);
            this.groupBoxIzmeniOcenu.Controls.Add(this.txtIzmenjenaOcena);
            this.groupBoxIzmeniOcenu.Controls.Add(this.label12);
            this.groupBoxIzmeniOcenu.ForeColor = System.Drawing.Color.MidnightBlue;
            this.groupBoxIzmeniOcenu.Location = new System.Drawing.Point(660, 40);
            this.groupBoxIzmeniOcenu.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupBoxIzmeniOcenu.Name = "groupBoxIzmeniOcenu";
            this.groupBoxIzmeniOcenu.Padding = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupBoxIzmeniOcenu.Size = new System.Drawing.Size(206, 192);
            this.groupBoxIzmeniOcenu.TabIndex = 29;
            this.groupBoxIzmeniOcenu.TabStop = false;
            this.groupBoxIzmeniOcenu.Text = "Izmeni ocenu";
            // 
            // labelOcenaIdKojaSeMenja
            // 
            this.labelOcenaIdKojaSeMenja.AutoSize = true;
            this.labelOcenaIdKojaSeMenja.BackColor = System.Drawing.Color.Transparent;
            this.labelOcenaIdKojaSeMenja.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelOcenaIdKojaSeMenja.ForeColor = System.Drawing.Color.MidnightBlue;
            this.labelOcenaIdKojaSeMenja.Location = new System.Drawing.Point(92, 54);
            this.labelOcenaIdKojaSeMenja.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelOcenaIdKojaSeMenja.Name = "labelOcenaIdKojaSeMenja";
            this.labelOcenaIdKojaSeMenja.Size = new System.Drawing.Size(21, 17);
            this.labelOcenaIdKojaSeMenja.TabIndex = 29;
            this.labelOcenaIdKojaSeMenja.Text = "id";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.BackColor = System.Drawing.Color.Transparent;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.MidnightBlue;
            this.label13.Location = new System.Drawing.Point(62, 31);
            this.label13.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(80, 17);
            this.label13.TabIndex = 28;
            this.label13.Text = "ID Ocene:";
            // 
            // btnIzmeniOcenu
            // 
            this.btnIzmeniOcenu.Location = new System.Drawing.Point(61, 162);
            this.btnIzmeniOcenu.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnIzmeniOcenu.Name = "btnIzmeniOcenu";
            this.btnIzmeniOcenu.Size = new System.Drawing.Size(91, 20);
            this.btnIzmeniOcenu.TabIndex = 25;
            this.btnIzmeniOcenu.Text = "Izmeni ocenu";
            this.btnIzmeniOcenu.UseVisualStyleBackColor = true;
            this.btnIzmeniOcenu.Click += new System.EventHandler(this.btnIzmeniOcenu_Click);
            // 
            // txtIzmenjenaOcena
            // 
            this.txtIzmenjenaOcena.Location = new System.Drawing.Point(60, 123);
            this.txtIzmenjenaOcena.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.txtIzmenjenaOcena.MaxLength = 1;
            this.txtIzmenjenaOcena.Name = "txtIzmenjenaOcena";
            this.txtIzmenjenaOcena.Size = new System.Drawing.Size(92, 19);
            this.txtIzmenjenaOcena.TabIndex = 27;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.Color.Transparent;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.MidnightBlue;
            this.label12.Location = new System.Drawing.Point(57, 87);
            this.label12.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(99, 17);
            this.label12.TabIndex = 26;
            this.label12.Text = "Nova ocena:";
            // 
            // groupBoxUkloniOcenu
            // 
            this.groupBoxUkloniOcenu.Controls.Add(this.btnUkloniOcenu);
            this.groupBoxUkloniOcenu.Controls.Add(this.txtUkloniOcenuId);
            this.groupBoxUkloniOcenu.Controls.Add(this.label11);
            this.groupBoxUkloniOcenu.ForeColor = System.Drawing.Color.MidnightBlue;
            this.groupBoxUkloniOcenu.Location = new System.Drawing.Point(660, 40);
            this.groupBoxUkloniOcenu.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupBoxUkloniOcenu.Name = "groupBoxUkloniOcenu";
            this.groupBoxUkloniOcenu.Padding = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupBoxUkloniOcenu.Size = new System.Drawing.Size(206, 115);
            this.groupBoxUkloniOcenu.TabIndex = 28;
            this.groupBoxUkloniOcenu.TabStop = false;
            this.groupBoxUkloniOcenu.Text = "Ukloni ocenu";
            // 
            // btnUkloniOcenu
            // 
            this.btnUkloniOcenu.Location = new System.Drawing.Point(61, 80);
            this.btnUkloniOcenu.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnUkloniOcenu.Name = "btnUkloniOcenu";
            this.btnUkloniOcenu.Size = new System.Drawing.Size(91, 20);
            this.btnUkloniOcenu.TabIndex = 25;
            this.btnUkloniOcenu.Text = "Ukloni ocenu";
            this.btnUkloniOcenu.UseVisualStyleBackColor = true;
            this.btnUkloniOcenu.Click += new System.EventHandler(this.btnUkloniOcenu_Click);
            // 
            // txtUkloniOcenuId
            // 
            this.txtUkloniOcenuId.Location = new System.Drawing.Point(61, 50);
            this.txtUkloniOcenuId.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.txtUkloniOcenuId.MaxLength = 1000;
            this.txtUkloniOcenuId.Name = "txtUkloniOcenuId";
            this.txtUkloniOcenuId.Size = new System.Drawing.Size(91, 19);
            this.txtUkloniOcenuId.TabIndex = 27;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.MidnightBlue;
            this.label11.Location = new System.Drawing.Point(65, 23);
            this.label11.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(80, 17);
            this.label11.TabIndex = 26;
            this.label11.Text = "ID Ocene:";
            // 
            // groupBoxDodajOcenu
            // 
            this.groupBoxDodajOcenu.Controls.Add(this.btnDodajOcenu);
            this.groupBoxDodajOcenu.Controls.Add(this.txtOcena);
            this.groupBoxDodajOcenu.Controls.Add(this.label10);
            this.groupBoxDodajOcenu.Controls.Add(this.label9);
            this.groupBoxDodajOcenu.Controls.Add(this.cbxDodajTipOcene);
            this.groupBoxDodajOcenu.ForeColor = System.Drawing.Color.MidnightBlue;
            this.groupBoxDodajOcenu.Location = new System.Drawing.Point(660, 40);
            this.groupBoxDodajOcenu.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupBoxDodajOcenu.Name = "groupBoxDodajOcenu";
            this.groupBoxDodajOcenu.Padding = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupBoxDodajOcenu.Size = new System.Drawing.Size(206, 197);
            this.groupBoxDodajOcenu.TabIndex = 24;
            this.groupBoxDodajOcenu.TabStop = false;
            this.groupBoxDodajOcenu.Text = "Dodaj ocenu";
            // 
            // btnDodajOcenu
            // 
            this.btnDodajOcenu.Location = new System.Drawing.Point(56, 162);
            this.btnDodajOcenu.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnDodajOcenu.Name = "btnDodajOcenu";
            this.btnDodajOcenu.Size = new System.Drawing.Size(98, 20);
            this.btnDodajOcenu.TabIndex = 25;
            this.btnDodajOcenu.Text = "Dodaj ocenu";
            this.btnDodajOcenu.UseVisualStyleBackColor = true;
            this.btnDodajOcenu.Click += new System.EventHandler(this.btnDodajOcenu_Click);
            // 
            // txtOcena
            // 
            this.txtOcena.Location = new System.Drawing.Point(56, 124);
            this.txtOcena.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.txtOcena.MaxLength = 1;
            this.txtOcena.Name = "txtOcena";
            this.txtOcena.Size = new System.Drawing.Size(97, 19);
            this.txtOcena.TabIndex = 27;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.MidnightBlue;
            this.label10.Location = new System.Drawing.Point(56, 92);
            this.label10.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(99, 17);
            this.label10.TabIndex = 26;
            this.label10.Text = "Nova ocena:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.MidnightBlue;
            this.label9.Location = new System.Drawing.Point(65, 28);
            this.label9.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(85, 17);
            this.label9.TabIndex = 25;
            this.label9.Text = "Tip ocene:";
            // 
            // cbxDodajTipOcene
            // 
            this.cbxDodajTipOcene.FormattingEnabled = true;
            this.cbxDodajTipOcene.Items.AddRange(new object[] {
            "Brojčana",
            "Opisna"});
            this.cbxDodajTipOcene.Location = new System.Drawing.Point(56, 57);
            this.cbxDodajTipOcene.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.cbxDodajTipOcene.Name = "cbxDodajTipOcene";
            this.cbxDodajTipOcene.Size = new System.Drawing.Size(97, 21);
            this.cbxDodajTipOcene.TabIndex = 0;
            // 
            // btnOpcijaIzmeniOcenu
            // 
            this.btnOpcijaIzmeniOcenu.Location = new System.Drawing.Point(772, 263);
            this.btnOpcijaIzmeniOcenu.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnOpcijaIzmeniOcenu.Name = "btnOpcijaIzmeniOcenu";
            this.btnOpcijaIzmeniOcenu.Size = new System.Drawing.Size(94, 28);
            this.btnOpcijaIzmeniOcenu.TabIndex = 23;
            this.btnOpcijaIzmeniOcenu.Text = "Izmeni ocenu";
            this.btnOpcijaIzmeniOcenu.UseVisualStyleBackColor = true;
            this.btnOpcijaIzmeniOcenu.Click += new System.EventHandler(this.btnOpcijaIzmeniOcenu_Click);
            // 
            // btnOpcijaUkloniOcenu
            // 
            this.btnOpcijaUkloniOcenu.Location = new System.Drawing.Point(538, 263);
            this.btnOpcijaUkloniOcenu.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnOpcijaUkloniOcenu.Name = "btnOpcijaUkloniOcenu";
            this.btnOpcijaUkloniOcenu.Size = new System.Drawing.Size(98, 28);
            this.btnOpcijaUkloniOcenu.TabIndex = 22;
            this.btnOpcijaUkloniOcenu.Text = "Ukloni ocenu";
            this.btnOpcijaUkloniOcenu.UseVisualStyleBackColor = true;
            this.btnOpcijaUkloniOcenu.Click += new System.EventHandler(this.btnOpcijaUkloniOcenu_Click);
            // 
            // btnOpcijaDodajOcenu
            // 
            this.btnOpcijaDodajOcenu.Location = new System.Drawing.Point(291, 263);
            this.btnOpcijaDodajOcenu.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnOpcijaDodajOcenu.Name = "btnOpcijaDodajOcenu";
            this.btnOpcijaDodajOcenu.Size = new System.Drawing.Size(100, 28);
            this.btnOpcijaDodajOcenu.TabIndex = 21;
            this.btnOpcijaDodajOcenu.Text = "Dodaj ocenu";
            this.btnOpcijaDodajOcenu.UseVisualStyleBackColor = true;
            this.btnOpcijaDodajOcenu.Click += new System.EventHandler(this.btnOpcijaDodajOcenu_Click);
            // 
            // labelIzabraniUcenikPredmet
            // 
            this.labelIzabraniUcenikPredmet.AutoSize = true;
            this.labelIzabraniUcenikPredmet.BackColor = System.Drawing.Color.Transparent;
            this.labelIzabraniUcenikPredmet.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelIzabraniUcenikPredmet.ForeColor = System.Drawing.Color.MidnightBlue;
            this.labelIzabraniUcenikPredmet.Location = new System.Drawing.Point(157, 171);
            this.labelIzabraniUcenikPredmet.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelIzabraniUcenikPredmet.Name = "labelIzabraniUcenikPredmet";
            this.labelIzabraniUcenikPredmet.Size = new System.Drawing.Size(53, 17);
            this.labelIzabraniUcenikPredmet.TabIndex = 20;
            this.labelIzabraniUcenikPredmet.Text = "Prikaz";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label8.Location = new System.Drawing.Point(14, 171);
            this.label8.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(135, 17);
            this.label8.TabIndex = 19;
            this.label8.Text = "Izabrani predmet:";
            // 
            // dataGridOceneUcenika
            // 
            this.dataGridOceneUcenika.BackgroundColor = System.Drawing.Color.White;
            this.dataGridOceneUcenika.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.MidnightBlue;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridOceneUcenika.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle5;
            this.dataGridOceneUcenika.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridOceneUcenika.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column2,
            this.Column3});
            this.dataGridOceneUcenika.EnableHeadersVisualStyles = false;
            this.dataGridOceneUcenika.GridColor = System.Drawing.Color.MediumBlue;
            this.dataGridOceneUcenika.Location = new System.Drawing.Point(291, 32);
            this.dataGridOceneUcenika.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.dataGridOceneUcenika.Name = "dataGridOceneUcenika";
            this.dataGridOceneUcenika.ReadOnly = true;
            this.dataGridOceneUcenika.RowHeadersVisible = false;
            dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.ControlLight;
            dataGridViewCellStyle6.ForeColor = System.Drawing.Color.MidnightBlue;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.Color.Aqua;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.Color.MidnightBlue;
            this.dataGridOceneUcenika.RowsDefaultCellStyle = dataGridViewCellStyle6;
            this.dataGridOceneUcenika.RowTemplate.Height = 24;
            this.dataGridOceneUcenika.Size = new System.Drawing.Size(345, 214);
            this.dataGridOceneUcenika.TabIndex = 18;
            this.dataGridOceneUcenika.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridOceneUcenika_CellClick);
            // 
            // Column1
            // 
            this.Column1.HeaderText = "ID Ocene";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            this.Column1.Width = 110;
            // 
            // Column2
            // 
            this.Column2.HeaderText = "Tip Ocene";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            this.Column2.Width = 120;
            // 
            // Column3
            // 
            this.Column3.HeaderText = "Ocena";
            this.Column3.Name = "Column3";
            this.Column3.ReadOnly = true;
            // 
            // labelIzabraniUcenikNeopravdani
            // 
            this.labelIzabraniUcenikNeopravdani.AutoSize = true;
            this.labelIzabraniUcenikNeopravdani.BackColor = System.Drawing.Color.Transparent;
            this.labelIzabraniUcenikNeopravdani.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelIzabraniUcenikNeopravdani.ForeColor = System.Drawing.Color.MidnightBlue;
            this.labelIzabraniUcenikNeopravdani.Location = new System.Drawing.Point(157, 263);
            this.labelIzabraniUcenikNeopravdani.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelIzabraniUcenikNeopravdani.Name = "labelIzabraniUcenikNeopravdani";
            this.labelIzabraniUcenikNeopravdani.Size = new System.Drawing.Size(53, 17);
            this.labelIzabraniUcenikNeopravdani.TabIndex = 17;
            this.labelIzabraniUcenikNeopravdani.Text = "Prikaz";
            // 
            // labelIzabraniUcenikOpravdani
            // 
            this.labelIzabraniUcenikOpravdani.AutoSize = true;
            this.labelIzabraniUcenikOpravdani.BackColor = System.Drawing.Color.Transparent;
            this.labelIzabraniUcenikOpravdani.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelIzabraniUcenikOpravdani.ForeColor = System.Drawing.Color.MidnightBlue;
            this.labelIzabraniUcenikOpravdani.Location = new System.Drawing.Point(157, 216);
            this.labelIzabraniUcenikOpravdani.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelIzabraniUcenikOpravdani.Name = "labelIzabraniUcenikOpravdani";
            this.labelIzabraniUcenikOpravdani.Size = new System.Drawing.Size(53, 17);
            this.labelIzabraniUcenikOpravdani.TabIndex = 16;
            this.labelIzabraniUcenikOpravdani.Text = "Prikaz";
            // 
            // labelIzabraniUcnikOdeljenje
            // 
            this.labelIzabraniUcnikOdeljenje.AutoSize = true;
            this.labelIzabraniUcnikOdeljenje.BackColor = System.Drawing.Color.Transparent;
            this.labelIzabraniUcnikOdeljenje.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelIzabraniUcnikOdeljenje.ForeColor = System.Drawing.Color.MidnightBlue;
            this.labelIzabraniUcnikOdeljenje.Location = new System.Drawing.Point(157, 127);
            this.labelIzabraniUcnikOdeljenje.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelIzabraniUcnikOdeljenje.Name = "labelIzabraniUcnikOdeljenje";
            this.labelIzabraniUcnikOdeljenje.Size = new System.Drawing.Size(53, 17);
            this.labelIzabraniUcnikOdeljenje.TabIndex = 15;
            this.labelIzabraniUcnikOdeljenje.Text = "Prikaz";
            // 
            // labelIzabraniUcenikPrezime
            // 
            this.labelIzabraniUcenikPrezime.AutoSize = true;
            this.labelIzabraniUcenikPrezime.BackColor = System.Drawing.Color.Transparent;
            this.labelIzabraniUcenikPrezime.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelIzabraniUcenikPrezime.ForeColor = System.Drawing.Color.MidnightBlue;
            this.labelIzabraniUcenikPrezime.Location = new System.Drawing.Point(157, 81);
            this.labelIzabraniUcenikPrezime.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelIzabraniUcenikPrezime.Name = "labelIzabraniUcenikPrezime";
            this.labelIzabraniUcenikPrezime.Size = new System.Drawing.Size(53, 17);
            this.labelIzabraniUcenikPrezime.TabIndex = 14;
            this.labelIzabraniUcenikPrezime.Text = "Prikaz";
            // 
            // labelIzabraniUcenikIme
            // 
            this.labelIzabraniUcenikIme.AutoSize = true;
            this.labelIzabraniUcenikIme.BackColor = System.Drawing.Color.Transparent;
            this.labelIzabraniUcenikIme.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelIzabraniUcenikIme.ForeColor = System.Drawing.Color.MidnightBlue;
            this.labelIzabraniUcenikIme.Location = new System.Drawing.Point(157, 32);
            this.labelIzabraniUcenikIme.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelIzabraniUcenikIme.Name = "labelIzabraniUcenikIme";
            this.labelIzabraniUcenikIme.Size = new System.Drawing.Size(53, 17);
            this.labelIzabraniUcenikIme.TabIndex = 13;
            this.labelIzabraniUcenikIme.Text = "Prikaz";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label7.Location = new System.Drawing.Point(4, 263);
            this.label7.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(156, 17);
            this.label7.TabIndex = 12;
            this.label7.Text = "Neopravdani časovi:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label6.Location = new System.Drawing.Point(14, 216);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(139, 17);
            this.label6.TabIndex = 11;
            this.label6.Text = "Opravdani časovi:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label5.Location = new System.Drawing.Point(11, 127);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(143, 17);
            this.label5.TabIndex = 10;
            this.label5.Text = "Odeljenje učenika:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label1.Location = new System.Drawing.Point(45, 32);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(99, 17);
            this.label1.TabIndex = 3;
            this.label1.Text = "Ime učenika:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label4.Location = new System.Drawing.Point(19, 81);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(132, 17);
            this.label4.TabIndex = 4;
            this.label4.Text = "Prezime učenika:";
            // 
            // labelObavestenje
            // 
            this.labelObavestenje.AutoSize = true;
            this.labelObavestenje.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelObavestenje.ForeColor = System.Drawing.Color.Red;
            this.labelObavestenje.Location = new System.Drawing.Point(1, 0);
            this.labelObavestenje.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelObavestenje.Name = "labelObavestenje";
            this.labelObavestenje.Size = new System.Drawing.Size(274, 16);
            this.labelObavestenje.TabIndex = 11;
            this.labelObavestenje.Text = "Potrebno je izabrati predmet i učenika.\r\n";
            // 
            // timer1
            // 
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.dataGridOdsutniUcenici);
            this.groupBox2.Controls.Add(this.btnUpisOdsutnih);
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.ForeColor = System.Drawing.Color.MidnightBlue;
            this.groupBox2.Location = new System.Drawing.Point(473, 22);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupBox2.Size = new System.Drawing.Size(408, 193);
            this.groupBox2.TabIndex = 10;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Upis odsutnih učenika";
            // 
            // dataGridOdsutniUcenici
            // 
            this.dataGridOdsutniUcenici.BackgroundColor = System.Drawing.Color.White;
            this.dataGridOdsutniUcenici.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle7.BackColor = System.Drawing.Color.MidnightBlue;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle7.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridOdsutniUcenici.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle7;
            this.dataGridOdsutniUcenici.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridOdsutniUcenici.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column4,
            this.Column5,
            this.Column6,
            this.Column8,
            this.Column7});
            this.dataGridOdsutniUcenici.EnableHeadersVisualStyles = false;
            this.dataGridOdsutniUcenici.GridColor = System.Drawing.Color.MediumBlue;
            this.dataGridOdsutniUcenici.Location = new System.Drawing.Point(13, 17);
            this.dataGridOdsutniUcenici.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.dataGridOdsutniUcenici.Name = "dataGridOdsutniUcenici";
            this.dataGridOdsutniUcenici.RowHeadersVisible = false;
            dataGridViewCellStyle8.BackColor = System.Drawing.SystemColors.ControlLight;
            dataGridViewCellStyle8.ForeColor = System.Drawing.Color.MidnightBlue;
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.Color.Aqua;
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.Color.MidnightBlue;
            this.dataGridOdsutniUcenici.RowsDefaultCellStyle = dataGridViewCellStyle8;
            this.dataGridOdsutniUcenici.RowTemplate.Height = 24;
            this.dataGridOdsutniUcenici.Size = new System.Drawing.Size(381, 129);
            this.dataGridOdsutniUcenici.TabIndex = 30;
            // 
            // btnUpisOdsutnih
            // 
            this.btnUpisOdsutnih.BackColor = System.Drawing.Color.PowderBlue;
            this.btnUpisOdsutnih.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUpisOdsutnih.ForeColor = System.Drawing.Color.MidnightBlue;
            this.btnUpisOdsutnih.Location = new System.Drawing.Point(130, 150);
            this.btnUpisOdsutnih.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnUpisOdsutnih.Name = "btnUpisOdsutnih";
            this.btnUpisOdsutnih.Size = new System.Drawing.Size(154, 29);
            this.btnUpisOdsutnih.TabIndex = 9;
            this.btnUpisOdsutnih.Text = "Upiši odsutne";
            this.btnUpisOdsutnih.UseVisualStyleBackColor = false;
            this.btnUpisOdsutnih.Click += new System.EventHandler(this.btnUpisOdsutnih_Click);
            // 
            // Column4
            // 
            this.Column4.HeaderText = "ID Ucenika";
            this.Column4.Name = "Column4";
            // 
            // Column5
            // 
            this.Column5.HeaderText = "Ime";
            this.Column5.Name = "Column5";
            // 
            // Column6
            // 
            this.Column6.HeaderText = "Prezime";
            this.Column6.Name = "Column6";
            // 
            // Column8
            // 
            this.Column8.HeaderText = "Neopravdani";
            this.Column8.Name = "Column8";
            // 
            // Column7
            // 
            this.Column7.HeaderText = "Odsutan";
            this.Column7.Name = "Column7";
            // 
            // FormaNastavnikovoOdeljenje
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(901, 545);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.labelObavestenje);
            this.Controls.Add(this.groupBoxIzabraniUcenik);
            this.Controls.Add(this.groupBox1);
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Name = "FormaNastavnikovoOdeljenje";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "e-Dnevnik";
            this.Load += new System.EventHandler(this.NastavnikovoOdeljenje_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBoxIzabraniUcenik.ResumeLayout(false);
            this.groupBoxIzabraniUcenik.PerformLayout();
            this.groupBoxIzmeniOcenu.ResumeLayout(false);
            this.groupBoxIzmeniOcenu.PerformLayout();
            this.groupBoxUkloniOcenu.ResumeLayout(false);
            this.groupBoxUkloniOcenu.PerformLayout();
            this.groupBoxDodajOcenu.ResumeLayout(false);
            this.groupBoxDodajOcenu.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridOceneUcenika)).EndInit();
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridOdsutniUcenici)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelOdeljenjeNaziv;
        private System.Windows.Forms.Label labelOdeljenjeSmer;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cbxPredajemPredmete;
        private System.Windows.Forms.ComboBox cbxUceniciOdeljenja;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBoxIzabraniUcenik;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btnPodaciUcenika;
        private System.Windows.Forms.Label labelIzabraniUcenikNeopravdani;
        private System.Windows.Forms.Label labelIzabraniUcenikOpravdani;
        private System.Windows.Forms.Label labelIzabraniUcnikOdeljenje;
        private System.Windows.Forms.Label labelIzabraniUcenikPrezime;
        private System.Windows.Forms.Label labelIzabraniUcenikIme;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label labelObavestenje;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Label labelIzabraniUcenikPredmet;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.DataGridView dataGridOceneUcenika;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.Button btnOpcijaIzmeniOcenu;
        private System.Windows.Forms.Button btnOpcijaUkloniOcenu;
        private System.Windows.Forms.Button btnOpcijaDodajOcenu;
        private System.Windows.Forms.GroupBox groupBoxDodajOcenu;
        private System.Windows.Forms.Button btnDodajOcenu;
        private System.Windows.Forms.TextBox txtOcena;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox cbxDodajTipOcene;
        private System.Windows.Forms.GroupBox groupBoxUkloniOcenu;
        private System.Windows.Forms.GroupBox groupBoxIzmeniOcenu;
        private System.Windows.Forms.Button btnIzmeniOcenu;
        private System.Windows.Forms.TextBox txtIzmenjenaOcena;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Button btnUkloniOcenu;
        private System.Windows.Forms.TextBox txtUkloniOcenuId;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label labelOcenaIdKojaSeMenja;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.DataGridView dataGridOdsutniUcenici;
        private System.Windows.Forms.Button btnUpisOdsutnih;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column6;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column8;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Column7;
    }
}