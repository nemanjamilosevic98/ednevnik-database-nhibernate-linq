﻿namespace eDnevnik
{
    partial class FormaUceniciOdeljenja
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.cbxOdeljenje = new System.Windows.Forms.ComboBox();
            this.txtImePrezimeRoditelja = new System.Windows.Forms.Label();
            this.dataGridUcenici = new System.Windows.Forms.DataGridView();
            this.btnPremestiUcenika = new System.Windows.Forms.Button();
            this.panelPremestanje = new System.Windows.Forms.Panel();
            this.btnPremesti = new System.Windows.Forms.Button();
            this.labelPrezime = new System.Windows.Forms.Label();
            this.labelIme = new System.Windows.Forms.Label();
            this.cbxNovoOdeljenje = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridUcenici)).BeginInit();
            this.panelPremestanje.SuspendLayout();
            this.SuspendLayout();
            // 
            // cbxOdeljenje
            // 
            this.cbxOdeljenje.FormattingEnabled = true;
            this.cbxOdeljenje.Location = new System.Drawing.Point(127, 40);
            this.cbxOdeljenje.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.cbxOdeljenje.Name = "cbxOdeljenje";
            this.cbxOdeljenje.Size = new System.Drawing.Size(100, 21);
            this.cbxOdeljenje.TabIndex = 0;
            this.cbxOdeljenje.SelectedIndexChanged += new System.EventHandler(this.cbxOdeljenje_SelectedIndexChanged);
            // 
            // txtImePrezimeRoditelja
            // 
            this.txtImePrezimeRoditelja.AutoSize = true;
            this.txtImePrezimeRoditelja.BackColor = System.Drawing.Color.Transparent;
            this.txtImePrezimeRoditelja.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtImePrezimeRoditelja.ForeColor = System.Drawing.Color.MidnightBlue;
            this.txtImePrezimeRoditelja.Location = new System.Drawing.Point(137, 21);
            this.txtImePrezimeRoditelja.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.txtImePrezimeRoditelja.Name = "txtImePrezimeRoditelja";
            this.txtImePrezimeRoditelja.Size = new System.Drawing.Size(82, 17);
            this.txtImePrezimeRoditelja.TabIndex = 4;
            this.txtImePrezimeRoditelja.Text = "Odeljenje:";
            // 
            // dataGridUcenici
            // 
            this.dataGridUcenici.BackgroundColor = System.Drawing.Color.White;
            this.dataGridUcenici.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.MidnightBlue;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridUcenici.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridUcenici.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridUcenici.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column2,
            this.Column3});
            this.dataGridUcenici.EnableHeadersVisualStyles = false;
            this.dataGridUcenici.GridColor = System.Drawing.Color.MediumBlue;
            this.dataGridUcenici.Location = new System.Drawing.Point(11, 77);
            this.dataGridUcenici.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.dataGridUcenici.Name = "dataGridUcenici";
            this.dataGridUcenici.ReadOnly = true;
            this.dataGridUcenici.RowHeadersVisible = false;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.ControlLight;
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.MidnightBlue;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.LightCyan;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.MidnightBlue;
            this.dataGridUcenici.RowsDefaultCellStyle = dataGridViewCellStyle2;
            this.dataGridUcenici.RowTemplate.Height = 24;
            this.dataGridUcenici.Size = new System.Drawing.Size(330, 158);
            this.dataGridUcenici.TabIndex = 32;
            this.dataGridUcenici.Visible = false;
            this.dataGridUcenici.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridUcenici_CellClick);
            // 
            // btnPremestiUcenika
            // 
            this.btnPremestiUcenika.BackColor = System.Drawing.Color.PowderBlue;
            this.btnPremestiUcenika.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPremestiUcenika.ForeColor = System.Drawing.Color.MidnightBlue;
            this.btnPremestiUcenika.Location = new System.Drawing.Point(112, 280);
            this.btnPremestiUcenika.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnPremestiUcenika.Name = "btnPremestiUcenika";
            this.btnPremestiUcenika.Size = new System.Drawing.Size(115, 26);
            this.btnPremestiUcenika.TabIndex = 34;
            this.btnPremestiUcenika.Text = "Premesti učenika";
            this.btnPremestiUcenika.UseVisualStyleBackColor = false;
            this.btnPremestiUcenika.Visible = false;
            this.btnPremestiUcenika.Click += new System.EventHandler(this.btnPremestiUcenika_Click);
            // 
            // panelPremestanje
            // 
            this.panelPremestanje.Controls.Add(this.btnPremesti);
            this.panelPremestanje.Controls.Add(this.labelPrezime);
            this.panelPremestanje.Controls.Add(this.labelIme);
            this.panelPremestanje.Controls.Add(this.cbxNovoOdeljenje);
            this.panelPremestanje.Controls.Add(this.label3);
            this.panelPremestanje.Controls.Add(this.label2);
            this.panelPremestanje.Controls.Add(this.label1);
            this.panelPremestanje.Location = new System.Drawing.Point(9, 256);
            this.panelPremestanje.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.panelPremestanje.Name = "panelPremestanje";
            this.panelPremestanje.Size = new System.Drawing.Size(334, 109);
            this.panelPremestanje.TabIndex = 35;
            this.panelPremestanje.Visible = false;
            // 
            // btnPremesti
            // 
            this.btnPremesti.BackColor = System.Drawing.Color.PowderBlue;
            this.btnPremesti.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPremesti.ForeColor = System.Drawing.Color.MidnightBlue;
            this.btnPremesti.Location = new System.Drawing.Point(118, 81);
            this.btnPremesti.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnPremesti.Name = "btnPremesti";
            this.btnPremesti.Size = new System.Drawing.Size(100, 26);
            this.btnPremesti.TabIndex = 39;
            this.btnPremesti.Text = "Premesti";
            this.btnPremesti.UseVisualStyleBackColor = false;
            this.btnPremesti.Click += new System.EventHandler(this.btnPremesti_Click);
            // 
            // labelPrezime
            // 
            this.labelPrezime.AutoSize = true;
            this.labelPrezime.BackColor = System.Drawing.Color.Transparent;
            this.labelPrezime.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPrezime.ForeColor = System.Drawing.Color.RoyalBlue;
            this.labelPrezime.Location = new System.Drawing.Point(79, 51);
            this.labelPrezime.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelPrezime.Name = "labelPrezime";
            this.labelPrezime.Size = new System.Drawing.Size(65, 17);
            this.labelPrezime.TabIndex = 38;
            this.labelPrezime.Text = "prezime";
            // 
            // labelIme
            // 
            this.labelIme.AutoSize = true;
            this.labelIme.BackColor = System.Drawing.Color.Transparent;
            this.labelIme.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelIme.ForeColor = System.Drawing.Color.RoyalBlue;
            this.labelIme.Location = new System.Drawing.Point(79, 20);
            this.labelIme.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelIme.Name = "labelIme";
            this.labelIme.Size = new System.Drawing.Size(33, 17);
            this.labelIme.TabIndex = 36;
            this.labelIme.Text = "ime";
            // 
            // cbxNovoOdeljenje
            // 
            this.cbxNovoOdeljenje.FormattingEnabled = true;
            this.cbxNovoOdeljenje.Location = new System.Drawing.Point(234, 47);
            this.cbxNovoOdeljenje.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.cbxNovoOdeljenje.Name = "cbxNovoOdeljenje";
            this.cbxNovoOdeljenje.Size = new System.Drawing.Size(63, 21);
            this.cbxNovoOdeljenje.TabIndex = 36;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.MidnightBlue;
            this.label3.Location = new System.Drawing.Point(202, 20);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(121, 17);
            this.label3.TabIndex = 36;
            this.label3.Text = "Novo odeljenje:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.MidnightBlue;
            this.label2.Location = new System.Drawing.Point(11, 51);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(71, 17);
            this.label2.TabIndex = 37;
            this.label2.Text = "Prezime:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.MidnightBlue;
            this.label1.Location = new System.Drawing.Point(21, 20);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(38, 17);
            this.label1.TabIndex = 36;
            this.label1.Text = "Ime:";
            // 
            // Column1
            // 
            this.Column1.HeaderText = "ID Ucenika";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            this.Column1.Width = 110;
            // 
            // Column2
            // 
            this.Column2.HeaderText = "Ime";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            // 
            // Column3
            // 
            this.Column3.HeaderText = "Prezime";
            this.Column3.Name = "Column3";
            this.Column3.ReadOnly = true;
            // 
            // FormaUceniciOdeljenja
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(352, 376);
            this.Controls.Add(this.panelPremestanje);
            this.Controls.Add(this.btnPremestiUcenika);
            this.Controls.Add(this.dataGridUcenici);
            this.Controls.Add(this.txtImePrezimeRoditelja);
            this.Controls.Add(this.cbxOdeljenje);
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Name = "FormaUceniciOdeljenja";
            this.Text = "e-Dnevnik";
            this.Load += new System.EventHandler(this.FormaUceniciOdeljenja_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridUcenici)).EndInit();
            this.panelPremestanje.ResumeLayout(false);
            this.panelPremestanje.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox cbxOdeljenje;
        private System.Windows.Forms.Label txtImePrezimeRoditelja;
        private System.Windows.Forms.DataGridView dataGridUcenici;
        private System.Windows.Forms.Button btnPremestiUcenika;
        private System.Windows.Forms.Panel panelPremestanje;
        private System.Windows.Forms.Label labelPrezime;
        private System.Windows.Forms.Label labelIme;
        private System.Windows.Forms.ComboBox cbxNovoOdeljenje;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnPremesti;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
    }
}