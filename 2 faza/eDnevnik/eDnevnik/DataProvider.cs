﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using eDnevnik.DTOs;
using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using eDnevnik.Entiteti;
using System.Threading.Tasks;
using NHibernate;
using NHibernate.Linq;

namespace eDnevnik
{
    public class DataProvider
    {
        #region Predmet
        public IEnumerable<Predmet> vratiSvePredmete()
        {
            ISession s = DataLayer.GetSession();
            IEnumerable<Predmet> predmeti = s.Query<Predmet>().Select(p => p);

            return predmeti;
        }

        public Predmet vratiPredmet(int id)
        {
            ISession s = DataLayer.GetSession();

            return s.Query<Predmet>()
                .Where(v => v.Id == id).Select(p => p).FirstOrDefault();
        }

        public PredmetView vratiPredmetView(int id)
        {

            ISession s = DataLayer.GetSession();

            Predmet u = s.Query<Predmet>()
                .Where(v => v.Id == id).Select(p => p).FirstOrDefault();

            if (u == null) return new PredmetView();

            return new PredmetView(u);
        }

        public int dodajPredmet(Predmet u)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                s.Save(u);
                s.Flush();
                s.Close();

                return 1;
            }
            catch (Exception e)
            {
                return -1;
            }
        }

        public int ukloniPredmet(int id)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                Predmet u = s.Load<Predmet>(id);

                s.Delete(u);
                s.Flush();
                s.Close();

                return 1;
            }
            catch (Exception e)
            {
                return -1;
            }
        }

        public int azurirajPredmet(int id)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                Predmet u = s.Load<Predmet>(id);

                u.BlokNastava = "DA";
                u.SpecijalanKabinet = "NE";
                s.Update(u);
                s.Flush();
                s.Close();

                return 1;
            }
            catch (Exception e)
            {
                return -1;
            }
        }
        #endregion
    }
}
