﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using eDnevnik.DTOs;
using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using eDnevnik.Entiteti;
using System.Threading.Tasks;
using NHibernate;
using NHibernate.Linq;

namespace eDnevnik
{
    public class DataProvider
    {
        #region Predmet
        public IEnumerable<PredmetPogled> vratiSvePredmete()
        {
            ISession s = DataLayer.GetSession();
            IEnumerable<Predmet> predmeti = s.Query<Predmet>().Select(p => p);
            List<PredmetPogled> predm = new List<PredmetPogled>();

            foreach (Predmet p in predmeti)
                predm.Add(new PredmetPogled(p.Id,p.Naziv,p.Opis,p.BrojCasova,p.TipPredmeta,p.MinBrojUcenika,p.BlokNastava,p.SpecijalanKabinet));

            return predm;      
        }

        public Predmet vratiPredmet(int id)
        {
            ISession s = DataLayer.GetSession();

            return s.Query<Predmet>()
                .Where(v => v.Id == id).Select(p => p).FirstOrDefault();
        }

        public PredmetView vratiPredmetView(int id)
        {

            ISession s = DataLayer.GetSession();

            Predmet u = s.Query<Predmet>()
                .Where(v => v.Id == id).Select(p => p).FirstOrDefault();

            if (u == null) return new PredmetView();

            return new PredmetView(u);
        }

        public int dodajPredmet(Predmet u)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                s.Save(u);
                s.Flush();
                s.Close();

                return 1;
            }
            catch (Exception e)
            {
                return -1;
            }
        }

        public int ukloniPredmet(int id)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                Predmet u = s.Load<Predmet>(id);

                s.Delete(u);
                s.Flush();
                s.Close();

                return 1;
            }
            catch (Exception e)
            {
                return -1;
            }
        }

        public int azurirajPredmet(int id)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                Predmet u = s.Load<Predmet>(id);

                u.BlokNastava = "DA";
                u.SpecijalanKabinet = "NE";
                s.Update(u);
                s.Flush();
                s.Close();

                return 1;
            }
            catch (Exception e)
            {
                return -1;
            }
        }
        #endregion

        #region Odeljenje
        public IEnumerable<OdeljenjePogled> vratiSvaOdeljenja()
        {
            ISession s = DataLayer.GetSession();
            IEnumerable<Odeljenje> odeljenja = s.Query<Odeljenje>().Select(p => p);
            List<OdeljenjePogled> odelj = new List<OdeljenjePogled>();

            foreach (Odeljenje p in odeljenja)
                odelj.Add(new OdeljenjePogled(p.Id,p.Naziv,p.Smer));

            return odelj;
        }

        public Odeljenje vratiOdeljenje(int id)
        {
            ISession s = DataLayer.GetSession();

            return s.Query<Odeljenje>()
                .Where(v => v.Id == id).Select(p => p).FirstOrDefault();
        }

        public OdeljenjeView vratiOdeljenjeView(int id)
        {

            ISession s = DataLayer.GetSession();

            Odeljenje u = s.Query<Odeljenje>()
                .Where(v => v.Id == id).Select(p => p).FirstOrDefault();

            if (u == null) return new OdeljenjeView();

            return new OdeljenjeView(u);
        }

        public int dodajOdeljenje(Odeljenje u)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                s.Save(u);
                s.Flush();
                s.Close();

                return 1;
            }
            catch (Exception e)
            {
                return -1;
            }
        }

        public int ukloniOdeljenje(int id)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                Odeljenje u = s.Load<Odeljenje>(id);

                s.Delete(u);
                s.Flush();
                s.Close();

                return 1;
            }
            catch (Exception e)
            {
                return -1;
            }
        }

        public int azurirajOdeljenje(int id)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                Odeljenje u = s.Load<Odeljenje>(id);

                u.Smer = "JEZICKI";
                s.Update(u);
                s.Flush();
                s.Close();

                return 1;
            }
            catch (Exception e)
            {
                return -1;
            }
        }
        #endregion

        #region Godina
        public IEnumerable<GodinaPogled> vratiSveGodine()
        {
            ISession s = DataLayer.GetSession();
            IEnumerable<Godina> godine = s.Query<Godina>().Select(p => p);
            List<GodinaPogled> god = new List<GodinaPogled>();

            foreach (Godina g in godine)
                god.Add(new GodinaPogled(g.Id, g.PredmeT.Id, g.NaGodini));

            return god;
        }

        public Godina vratiGodinu(int id)
        {
            ISession s = DataLayer.GetSession();

            return s.Query<Godina>()
                .Where(v => v.Id == id).Select(p => p).FirstOrDefault();
        }

        public GodinaView vratiGodinaView(int id)
        {

            ISession s = DataLayer.GetSession();

            Godina u = s.Query<Godina>()
                .Where(v => v.Id == id).Select(p => p).FirstOrDefault();

            if (u == null) return new GodinaView();

            return new GodinaView(u);
        }

        public int dodajGodinu(Godina u)
        {
            try
            {
                ISession s = DataLayer.GetSession();
                Predmet p = new Predmet();
                p = s.Load<Predmet>(1);
                u.PredmeT = p;

                s.Save(u);
                s.Flush();
                s.Close();

                return 1;
            }
            catch (Exception e)
            {
                return -1;
            }
        }

        public int ukloniGodinu(int id)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                Godina u = s.Load<Godina>(id);

                s.Delete(u);
                s.Flush();
                s.Close();

                return 1;
            }
            catch (Exception e)
            {
                return -1;
            }
        }

        public int azurirajGodinu(int id)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                Godina u = s.Load<Godina>(id);

                u.NaGodini = 4;
                s.Update(u);
                s.Flush();
                s.Close();

                return 1;
            }
            catch (Exception e)
            {
                return -1;
            }
        }
        #endregion

        #region Raspored
        public IEnumerable<RasporedPogled> vratiSveRasporede()
        {
            ISession s = DataLayer.GetSession();
            IEnumerable<Raspored> rasporedi = s.Query<Raspored>().Select(p => p);
            List<RasporedPogled> ras = new List<RasporedPogled>();

            foreach (Raspored r in rasporedi)
                ras.Add(new RasporedPogled(r.Id,r.Id_Odeljenja.Id,r.Id_Predmeta.Id,r.Dan,r.Cas));
            return ras;
        }

        public Raspored vratiRaspored(int id)
        {
            ISession s = DataLayer.GetSession();

            return s.Query<Raspored>()
                .Where(v => v.Id == id).Select(p => p).FirstOrDefault();
        }

        public RasporedView vratiRasporedView(int id)
        {

            ISession s = DataLayer.GetSession();

            Raspored u = s.Query<Raspored>()
                .Where(v => v.Id == id).Select(p => p).FirstOrDefault();

            if (u == null) return new RasporedView();

            return new RasporedView(u);
        }

        public int dodajRaspored(Raspored u)
        {
            try
            {
                ISession s = DataLayer.GetSession();
                Odeljenje o = new Odeljenje();
                Predmet p = new Predmet();

                 o = s.Load<Odeljenje>(1);
                 p = s.Load<Predmet>(1);
                 u.Id_Odeljenja = o;
                 u.Id_Predmeta = p;

                s.Save(u);
                s.Flush();
                s.Close();

                return 1;
            }
            catch (Exception e)
            {
                return -1;
            }
        }

        public int ukloniRaspored(int id)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                Raspored u = s.Load<Raspored>(id);

                s.Delete(u);
                s.Flush();
                s.Close();

                return 1;
            }
            catch (Exception e)
            {
                return -1;
            }
        }

        public int azurirajRaspored(int id)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                Raspored u = s.Load<Raspored>(id);

                u.Dan = "PETAK";
                u.Cas = 7;
                s.Update(u);
                s.Flush();
                s.Close();

                return 1;
            }
            catch (Exception e)
            {
                return -1;
            }
        }
        #endregion

        #region BrojTelefona
        public IEnumerable<BrojTelefonaPogled> vratiSveBrojeveTelefona()
        {
            ISession s = DataLayer.GetSession();
            IEnumerable<BrojTelefona> brojeviTelefona = s.Query<BrojTelefona>().Select(p => p);
            List<BrojTelefonaPogled> brojevi = new List<BrojTelefonaPogled>();

            foreach (BrojTelefona g in brojeviTelefona)
                brojevi.Add(new BrojTelefonaPogled(g.Id,g.PripadaKorisniku.Id,g.BrojTelefonaKorisnika));
            return brojevi;
        }

        public BrojTelefona vratiBrojTelefona(int id)
        {
            ISession s = DataLayer.GetSession();

            return s.Query<BrojTelefona>()
                .Where(v => v.Id == id).Select(p => p).FirstOrDefault();
        }

        public BrojTelefonaView vratiBrojTelefonaView(int id)
        {

            ISession s = DataLayer.GetSession();

            BrojTelefona u = s.Query<BrojTelefona>()
                .Where(v => v.Id == id).Select(p => p).FirstOrDefault();

            if (u == null) return new BrojTelefonaView();

            return new BrojTelefonaView(u);
        }

        public int dodajBrojTelefona(BrojTelefona u)
        {
            try
            {
                ISession s = DataLayer.GetSession();
                Korisnik k = s.Load<Korisnik>(10);               
                u.PripadaKorisniku = k;
                s.Save(u);
                s.Flush();
                s.Close();

                return 1;
            }
            catch (Exception e)
            {
                return -1;
            }
        }

        public int ukloniBrojTelefona(int id)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                BrojTelefona u = s.Load<BrojTelefona>(id);

                s.Delete(u);
                s.Flush();
                s.Close();

                return 1;
            }
            catch (Exception e)
            {
                return -1;
            }
        }

        public int azurirajBrojTelefona(int id)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                BrojTelefona u = s.Load<BrojTelefona>(id);

                u.BrojTelefonaKorisnika = "018191919";
                s.Update(u);
                s.Flush();
                s.Close();

                return 1;
            }
            catch (Exception e)
            {
                return -1;
            }
        }
        #endregion

        #region Predstavlja
        public IEnumerable<PredstavljaPogled> vratiSvaPredstavljanja()
        {
            ISession s = DataLayer.GetSession();
            IEnumerable<Predstavlja> predstavljanja = s.Query<Predstavlja>().Select(p => p);
            List<PredstavljaPogled> pred = new List<PredstavljaPogled>();

            foreach (Predstavlja r in predstavljanja)
                pred.Add(new PredstavljaPogled(r.Id,r.Id_Odeljenja.Id,r.Id_Roditelja.Id,r.DatumOd,r.DatumDo));
            return pred;
        }

        public Predstavlja vratiPredstavljanje(int id)
        {
            ISession s = DataLayer.GetSession();

            return s.Query<Predstavlja>()
                .Where(v => v.Id == id).Select(p => p).FirstOrDefault();
        }

        public PredstavljaView vratiPredstavljaView(int id)
        {

            ISession s = DataLayer.GetSession();

            Predstavlja u = s.Query<Predstavlja>()
                .Where(v => v.Id == id).Select(p => p).FirstOrDefault();

            if (u == null) return new PredstavljaView();

            return new PredstavljaView(u);
        }

        public int dodajPredstavljanje(Predstavlja u)
        {
            try
            {
                ISession s = DataLayer.GetSession();
                Odeljenje o = s.Load<Odeljenje>(1);
                Roditelj r = s.Load<Roditelj>(36);
                u.Id_Roditelja = r;
                u.Id_Odeljenja = o;
                s.Save(u);
                s.Flush();
                s.Close();

                return 1;
            }
            catch (Exception e)
            {
                return -1;
            }
        }

        public int ukloniPredstavljanje(int id)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                Predstavlja u = s.Load<Predstavlja>(id);

                s.Delete(u);
                s.Flush();
                s.Close();

                return 1;
            }
            catch (Exception e)
            {
                return -1;
            }
        }

        public int azurirajPredstavljanje(int id)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                Predstavlja u = s.Load<Predstavlja>(id);

                u.DatumDo=DateTime.Now;
                s.Update(u);
                s.Flush();
                s.Close();

                return 1;
            }
            catch (Exception e)
            {
                return -1;
            }
        }
        #endregion

        #region Korisnici
        #region Administrator
        public IEnumerable<AdministratorPogled> vratiSveAdministratore()
        {
            ISession s = DataLayer.GetSession();
            IEnumerable<Administrator> administratori = s.Query<Administrator>().Select(p => p);
            List<AdministratorPogled> admini = new List<AdministratorPogled>();
            foreach (Administrator a in administratori)
                admini.Add(new AdministratorPogled(a.Id, a.Ime, a.Prezime, a.DatumRodjenja, a.Username, a.Password, a.FAdministrator, a.FUcenik, a.FNastavnik, a.FRoditelj,a.Pol,a.JMBG));
            return admini;
        }

        public Administrator vratiAdministratora(int id)
        {
            ISession s = DataLayer.GetSession();

            return s.Query<Administrator>()
                .Where(v => v.Id == id).Select(p => p).FirstOrDefault();
        }

        public AdministratorView vratiAdministratorView(int id)
        {

            ISession s = DataLayer.GetSession();

            Administrator u = s.Query<Administrator>()
                .Where(v => v.Id == id).Select(p => p).FirstOrDefault();

            if (u == null) return new AdministratorView();

            return new AdministratorView(u);
        }

        public int dodajAdministratora(Administrator u)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                s.Save(u);
                s.Flush();
                s.Close();

                return 1;
            }
            catch (Exception e)
            {
                return -1;
            }
        }

        public int ukloniAdministratora(int id)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                Administrator u = s.Load<Administrator>(id);

                s.Delete(u);
                s.Flush();
                s.Close();

                return 1;
            }
            catch (Exception e)
            {
                return -1;
            }
        }

        public int azurirajAdministratora(int id)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                Administrator u = s.Load<Administrator>(id);

                u.Ime = "Neko";
                u.Prezime = "Novi";
                s.Update(u);
                s.Flush();
                s.Close();

                return 1;
            }
            catch (Exception e)
            {
                return -1;
            }
        }
        #endregion

        #region Nastavnik
        public IEnumerable<NastavnikPogled> vratiSveNastavnike()
        {
            ISession s = DataLayer.GetSession();
            IEnumerable<Nastavnik> nastavnici = s.Query<Nastavnik>().Select(p => p);
            List<NastavnikPogled> nas = new List<NastavnikPogled>();
            foreach (Nastavnik a in nastavnici)
                nas.Add(new NastavnikPogled(a.Id, a.Ime, a.Prezime, a.DatumRodjenja, a.Username, a.Password, a.FAdministrator, a.FUcenik, a.FNastavnik, a.FRoditelj,a.SSSpreme, a.Pol, a.JMBG));
            return nas;
        }

        public Nastavnik vratiNastavnika(int id)
        {
            ISession s = DataLayer.GetSession();

            return s.Query<Nastavnik>()
                .Where(v => v.Id == id).Select(p => p).FirstOrDefault();
        }

        public NastavnikView vratiNastavnikView(int id)
        {

            ISession s = DataLayer.GetSession();

            Nastavnik u = s.Query<Nastavnik>()
                .Where(v => v.Id == id).Select(p => p).FirstOrDefault();

            if (u == null) return new NastavnikView();

            return new NastavnikView(u);
        }

        public int dodajNastavnika(Nastavnik u)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                s.Save(u);
                s.Flush();
                s.Close();

                return 1;
            }
            catch (Exception e)
            {
                return -1;
            }
        }

        public int ukloniNastavnika(int id)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                Nastavnik u = s.Load<Nastavnik>(id);

                s.Delete(u);
                s.Flush();
                s.Close();

                return 1;
            }
            catch (Exception e)
            {
                return -1;
            }
        }

        public int azurirajNastavnika(int id)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                Nastavnik u = s.Load<Nastavnik>(id);

                u.SSSpreme="osnovni";
                s.Update(u);
                s.Flush();
                s.Close();

                return 1;
            }
            catch (Exception e)
            {
                return -1;
            }
        }
        #endregion

        #region Roditelj
        public IEnumerable<RoditeljPogled> vratiSveRoditelje()
        {
            ISession s = DataLayer.GetSession();
            IEnumerable<Roditelj> roditelji = s.Query<Roditelj>().Select(p => p);
            List<RoditeljPogled> rod = new List<RoditeljPogled>();
            foreach (Roditelj a in roditelji)
                rod.Add(new RoditeljPogled(a.Id, a.Ime, a.Prezime, a.DatumRodjenja, a.Username, a.Password, a.FAdministrator, a.FUcenik, a.FNastavnik, a.FRoditelj, a.Pol, a.JMBG));
            return rod;
        }

        public Roditelj vratiRoditelja(int id)
        {
            ISession s = DataLayer.GetSession();

            return s.Query<Roditelj>()
                .Where(v => v.Id == id).Select(p => p).FirstOrDefault();
        }

        public RoditeljView vratiRoditeljView(int id)
        {

            ISession s = DataLayer.GetSession();

            Roditelj u = s.Query<Roditelj>()
                .Where(v => v.Id == id).Select(p => p).FirstOrDefault();

            if (u == null) return new RoditeljView();

            return new RoditeljView(u);
        }

        public int dodajRoditelja(Roditelj u)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                s.Save(u);
                s.Flush();
                s.Close();

                return 1;
            }
            catch (Exception e)
            {
                return -1;
            }
        }

        public int ukloniRoditelja(int id)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                Roditelj u = s.Load<Roditelj>(id);

                s.Delete(u);
                s.Flush();
                s.Close();

                return 1;
            }
            catch (Exception e)
            {
                return -1;
            }
        }

        public int azurirajRoditelja(int id)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                Roditelj u = s.Load<Roditelj>(id);

                u.Ime = "Milorad";
                u.Prezime = "Miloradovic";
                u.Pol = Convert.ToChar("M");
                s.Update(u);
                s.Flush();
                s.Close();

                return 1;
            }
            catch (Exception e)
            {
                return -1;
            }
        }
        #endregion

        #region Ucenik
        public IEnumerable<UcenikPogled> vratiSveUcenike()
        {
            ISession s = DataLayer.GetSession();
            IEnumerable<Ucenik> ucenici = s.Query<Ucenik>().Select(p => p);
            List<UcenikPogled> uc = new List<UcenikPogled>();
            foreach (Ucenik a in ucenici)
                uc.Add(new UcenikPogled(a.Id, a.Ime, a.Prezime, a.DatumRodjenja, a.Username, a.Password, a.FAdministrator, a.FUcenik, a.FNastavnik, a.FRoditelj,a.PripadaOdeljenju.Id,a.BrOpravdanih,a.BrNeopravdanih,a.OcenaVladanje, a.Pol, a.JMBG));
            return uc;
        }

        public Ucenik vratiUcenika(int id)
        {
            ISession s = DataLayer.GetSession();

            return s.Query<Ucenik>()
                .Where(v => v.Id == id).Select(p => p).FirstOrDefault();
        }

        public UcenikView vratiUcenikView(int id)
        {

            ISession s = DataLayer.GetSession();

            Ucenik u = s.Query<Ucenik>()
                .Where(v => v.Id == id).Select(p => p).FirstOrDefault();

            if (u == null) return new UcenikView();

            return new UcenikView(u);
        }

        public int dodajUcenika(Ucenik u)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                s.Save(u);
                s.Flush();
                s.Close();

                return 1;
            }
            catch (Exception e)
            {
                return -1;
            }
        }

        public int ukloniUcenika(int id)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                Ucenik u = s.Load<Ucenik>(id);

                s.Delete(u);
                s.Flush();
                s.Close();

                return 1;
            }
            catch (Exception e)
            {
                return -1;
            }
        }

        public int azurirajUcenika(int id)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                Ucenik u = s.Load<Ucenik>(id);

                u.OcenaVladanje = 4;
                u.BrOpravdanih = 20;
                u.BrNeopravdanih = 10;
                s.Update(u);
                s.Flush();
                s.Close();

                return 1;
            }
            catch (Exception e)
            {
                return -1;
            }
        }
        #endregion

        #region NastavnikRoditelj
        public IEnumerable<NastavnikRoditeljPogled> vratiSveNastavnikRoditelje()
        {
            ISession s = DataLayer.GetSession();
            IEnumerable<NastavnikRoditelj> nasrod = s.Query<NastavnikRoditelj>().Select(p => p);
            List<NastavnikRoditeljPogled> nas = new List<NastavnikRoditeljPogled>();
            foreach (NastavnikRoditelj a in nasrod)
                nas.Add(new NastavnikRoditeljPogled(a.Id, a.Ime, a.Prezime, a.DatumRodjenja, a.Username, a.Password, a.FAdministrator, a.FUcenik, a.FNastavnik, a.FRoditelj, a.SSSpreme, a.Pol, a.JMBG));
            return nas;
        }

        public NastavnikRoditelj vratiNastavnikRoditelja(int id)
        {
            ISession s = DataLayer.GetSession();

            return s.Query<NastavnikRoditelj>()
                .Where(v => v.Id == id).Select(p => p).FirstOrDefault();
        }

        public NastavnikRoditeljView vratiNastavnikRoditeljView(int id)
        {

            ISession s = DataLayer.GetSession();

            NastavnikRoditelj u = s.Query<NastavnikRoditelj>()
                .Where(v => v.Id == id).Select(p => p).FirstOrDefault();

            if (u == null) return new NastavnikRoditeljView();

            return new NastavnikRoditeljView(u);
        }

        public int dodajNastavnikRoditelja(NastavnikRoditelj u)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                s.Save(u);
                s.Flush();
                s.Close();

                return 1;
            }
            catch (Exception e)
            {
                return -1;
            }
        }

        public int ukloniNastavnikRoditelja(int id)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                NastavnikRoditelj u = s.Load<NastavnikRoditelj>(id);

                s.Delete(u);
                s.Flush();
                s.Close();

                return 1;
            }
            catch (Exception e)
            {
                return -1;
            }
        }

        public int azurirajNastavnikRoditelja(int id)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                NastavnikRoditelj u = s.Load<NastavnikRoditelj>(id);

                u.SSSpreme = "najvisa";
                s.Update(u);
                s.Flush();
                s.Close();

                return 1;
            }
            catch (Exception e)
            {
                return -1;
            }
        }
        #endregion

        #endregion

        #region JeRazredni
        public IEnumerable<jeRazredniPogled> vratiSveJeRazredni()
        {
            ISession s = DataLayer.GetSession();
            IEnumerable<JeRazredni> jerazred = s.Query<JeRazredni>().Select(p => p);
            List<jeRazredniPogled> ras = new List<jeRazredniPogled>();

            foreach (JeRazredni r in jerazred)
                ras.Add(new jeRazredniPogled(r.Id,r.Id_Nastavnika.Id,r.Id_Odeljenja.Id,r.DatumOd,r.DatumDo));
            return ras;
        }

        public JeRazredni vratiJeRazredni(int id)
        {
            ISession s = DataLayer.GetSession();

            return s.Query<JeRazredni>()
                .Where(v => v.Id == id).Select(p => p).FirstOrDefault();
        }

        public JeRazredniView vratiJeRazredniView(int id)
        {

            ISession s = DataLayer.GetSession();

            JeRazredni u = s.Query<JeRazredni>()
                .Where(v => v.Id == id).Select(p => p).FirstOrDefault();

            if (u == null) return new JeRazredniView();

            return new JeRazredniView(u);
        }

        public int dodajJeRazredni(JeRazredni u)
        {
            try
            {
                ISession s = DataLayer.GetSession();
                Nastavnik n = s.Load<Nastavnik>(40);
                Odeljenje o = s.Load<Odeljenje>(1);
                u.Id_Nastavnika = n;
                u.Id_Odeljenja = o;
                s.Save(u);
                s.Flush();
                s.Close();

                return 1;
            }
            catch (Exception e)
            {
                return -1;
            }
        }

        public int ukloniJeRazredni(int id)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                JeRazredni u = s.Load<JeRazredni>(id);

                s.Delete(u);
                s.Flush();
                s.Close();

                return 1;
            }
            catch (Exception e)
            {
                return -1;
            }
        }

        public int azurirajJeRazredni(int id)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                JeRazredni u = s.Load<JeRazredni>(id);

                u.DatumDo = DateTime.Now;
                s.Update(u);
                s.Flush();
                s.Close();

                return 1;
            }
            catch (Exception e)
            {
                return -1;
            }
        }
        #endregion

        #region ImaOcenu
        public IEnumerable<ImaOcenuPogled> vratiSveImaOcenu()
        {
            ISession s = DataLayer.GetSession();
            IEnumerable<ImaOcenu> ocene = s.Query<ImaOcenu>().Select(p => p);
            List<ImaOcenuPogled> ras = new List<ImaOcenuPogled>();

            foreach (ImaOcenu r in ocene)
                ras.Add(new ImaOcenuPogled(r.Id,r.Id_Ucenika.Id,r.Id_Predmeta.Id,r.TipOcene,r.Vrednost,r.Opis));
            return ras;
        }

        public ImaOcenu vratiImaOcenu(int id)
        {
            ISession s = DataLayer.GetSession();

            return s.Query<ImaOcenu>()
                .Where(v => v.Id == id).Select(p => p).FirstOrDefault();
        }

        public ImaOcenuView vratiImaOcenuView(int id)
        {

            ISession s = DataLayer.GetSession();

            ImaOcenu u = s.Query<ImaOcenu>()
                .Where(v => v.Id == id).Select(p => p).FirstOrDefault();

            if (u == null) return new ImaOcenuView();

            return new ImaOcenuView(u);
        }

        public int dodajImaOcenu(ImaOcenu u)
        {
            try
            {
                ISession s = DataLayer.GetSession();
                Ucenik uu= s.Load<Ucenik>(116);
                Predmet p = s.Load<Predmet>(6);
                u.Id_Predmeta = p;
                u.Id_Ucenika = uu;
                s.Save(u);
                s.Flush();
                s.Close();

                return 1;
            }
            catch (Exception e)
            {
                return -1;
            }
        }

        public int ukloniImaOcenu(int id)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                ImaOcenu u = s.Load<ImaOcenu>(id);

                s.Delete(u);
                s.Flush();
                s.Close();

                return 1;
            }
            catch (Exception e)
            {
                return -1;
            }
        }

        public int azurirajImaOcenu(int id)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                ImaOcenu u = s.Load<ImaOcenu>(id);

                u.TipOcene="BROJCANA";
                u.Vrednost = 1;
                u.Opis = null;
                s.Update(u);
                s.Flush();
                s.Close();

                return 1;
            }
            catch (Exception e)
            {
                return -1;
            }
        }
        #endregion

        #region Funkcija
        public IEnumerable<FunkcijaPogled> vratiSveFunkcije()
        {
            ISession s = DataLayer.GetSession();
            IEnumerable<Funkcija> funkcije = s.Query<Funkcija>().Select(p => p);
            List<FunkcijaPogled> fun = new List<FunkcijaPogled>();

            foreach (Funkcija f in funkcije)
                fun.Add(new FunkcijaPogled(f.Id,f.VrsiFunkciju.Id,f.DatumOd,f.DatumDo,f.Tip));
            return fun;
        }

        public Funkcija vratiFunkciju(int id)
        {
            ISession s = DataLayer.GetSession();

            return s.Query<Funkcija>()
                .Where(v => v.Id == id).Select(p => p).FirstOrDefault();
        }

        public FunkcijaView vratiFunkcijaView(int id)
        {

            ISession s = DataLayer.GetSession();

            Funkcija u = s.Query<Funkcija>()
                .Where(v => v.Id == id).Select(p => p).FirstOrDefault();

            if (u == null) return new FunkcijaView();

            return new FunkcijaView(u);
        }

        public int dodajFunkciju(Funkcija u)
        {
            try
            {
                ISession s = DataLayer.GetSession();
                Roditelj r = s.Load<Roditelj>(36);
                u.VrsiFunkciju = r;
                s.Save(u);
                s.Flush();
                s.Close();

                return 1;
            }
            catch (Exception e)
            {
                return -1;
            }
        }

        public int ukloniFunkciju(int id)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                Funkcija u = s.Load<Funkcija>(id);

                s.Delete(u);
                s.Flush();
                s.Close();

                return 1;
            }
            catch (Exception e)
            {
                return -1;
            }
        }

        public int azurirajFunkciju(int id)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                Funkcija u = s.Load<Funkcija>(id);

                u.DatumDo = DateTime.Now;
                u.Tip = "BLAGAJNIK";
                s.Update(u);
                s.Flush();
                s.Close();

                return 1;
            }
            catch (Exception e)
            {
                return -1;
            }
        }
        #endregion

        #region DrziCas
        public IEnumerable<DrziCasPogled> vratiSveDrziCas()
        {
            ISession s = DataLayer.GetSession();
            IEnumerable<DrziCas> casnaspred = s.Query<DrziCas>().Select(p => p);
            List<DrziCasPogled> fun = new List<DrziCasPogled>();

            foreach (DrziCas f in casnaspred)
                fun.Add(new DrziCasPogled(f.Id, f.Id_Nastavnika.Id,f.Id_Predmeta.Id,f.Id_Odeljenja.Id));
            return fun;
        }

        public DrziCas vratiDrziCas(int id)
        {
            ISession s = DataLayer.GetSession();

            return s.Query<DrziCas>()
                .Where(v => v.Id == id).Select(p => p).FirstOrDefault();
        }

        public DrziCasView vratiDrziCasView(int id)
        {

            ISession s = DataLayer.GetSession();

            DrziCas u = s.Query<DrziCas>()
                .Where(v => v.Id == id).Select(p => p).FirstOrDefault();

            if (u == null) return new DrziCasView();

            return new DrziCasView(u);
        }

        public int dodajDrziCas(DrziCas u)
        {
            try
            {
                ISession s = DataLayer.GetSession();
                u.Id_Nastavnika = s.Load<Nastavnik>(41);
                u.Id_Odeljenja = s.Load<Odeljenje>(1);
                u.Id_Predmeta = s.Load<Predmet>(1);

                s.Save(u);
                s.Flush();
                s.Close();

                return 1;
            }
            catch (Exception e)
            {
                return -1;
            }
        }

        public int ukloniDrziCas(int id)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                DrziCas u = s.Load<DrziCas>(id);

                s.Delete(u);
                s.Flush();
                s.Close();

                return 1;
            }
            catch (Exception e)
            {
                return -1;
            }
        }

        public int azurirajDrziCas(int id)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                DrziCas u = s.Load<DrziCas>(id);

                u.Id_Nastavnika = s.Load<Nastavnik>(44);
                u.Id_Odeljenja = s.Load<Odeljenje>(1);
                u.Id_Predmeta = s.Load<Predmet>(4);
                s.Update(u);
                s.Flush();
                s.Close();

                return 1;
            }
            catch (Exception e)
            {
                return -1;
            }
        }
        #endregion
    }
}
