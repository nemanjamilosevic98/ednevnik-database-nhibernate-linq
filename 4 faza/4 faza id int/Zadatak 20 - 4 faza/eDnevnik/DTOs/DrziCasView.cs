﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using eDnevnik.Entiteti;
using NHibernate;
using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;

namespace eDnevnik.DTOs
{
    public class DrziCasView
    {
        public virtual int Id_Nastavnika { get; set; }
        public virtual int Id_Predmeta { get; set; }
        public virtual int Id_Odeljenja { get; set; }

        public DrziCasView()
        {
        }

        public DrziCasView(DrziCas dr)
        {
            this.Id_Nastavnika = dr.Id_Nastavnika.Id;
            this.Id_Predmeta = dr.Id_Predmeta.Id;
            this.Id_Odeljenja = dr.Id_Odeljenja.Id;
        }
    }
}
