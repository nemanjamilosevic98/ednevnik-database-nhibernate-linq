﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using eDnevnik.Entiteti;
using NHibernate;
using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;

namespace eDnevnik.DTOs
{
    public class PredstavljaView
    {
        public virtual int Id_Odeljenja { get; set; }
        public virtual int Id_Roditelja { get; set; }
        public virtual DateTime DatumOd { get; set; }
        public virtual DateTime? DatumDo { get; set; }

        public PredstavljaView()
        {

        }

        public PredstavljaView(Predstavlja pr)
        {
            this.Id_Odeljenja = pr.Id_Odeljenja.Id;
            this.Id_Roditelja = pr.Id_Roditelja.Id;
            this.DatumOd = pr.DatumOd;
            this.DatumDo = pr.DatumDo;
        }
    }
}
