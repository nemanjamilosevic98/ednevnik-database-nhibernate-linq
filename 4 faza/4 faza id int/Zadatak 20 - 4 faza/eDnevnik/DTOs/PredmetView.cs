﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using eDnevnik.Entiteti;
using NHibernate;
using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;

namespace eDnevnik.DTOs
{
    public class PredmetView
    {
        public virtual string Naziv { get; set; }
        public virtual string Opis { get; set; }
        public virtual int BrojCasova { get; set; }
        public virtual string TipPredmeta { get; set; }
        public virtual int MinBrojUcenika { get; set; }
        public virtual string BlokNastava { get; set; }
        public virtual string SpecijalanKabinet { get; set; }

        public PredmetView()
        {
        }

        public PredmetView(Predmet p)
        {
            this.Naziv = p.Naziv;
            this.Opis = p.Opis;
            this.BrojCasova = p.BrojCasova;
            this.TipPredmeta = p.TipPredmeta;
            this.MinBrojUcenika = p.MinBrojUcenika;
            this.BlokNastava = p.BlokNastava;
            this.SpecijalanKabinet = p.SpecijalanKabinet;
        }

    }
}
