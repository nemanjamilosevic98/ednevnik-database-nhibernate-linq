﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using eDnevnik.Entiteti;
using NHibernate;
using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;

namespace eDnevnik.DTOs
{
    public class RasporedView
    {
        public virtual int Id_Odeljenja { get; set; }
        public virtual int Id_Predmeta { get; set; }
        public virtual string Dan { get; set; }
        public virtual int Cas { get; set; }

        public RasporedView()
        {

        }

        public RasporedView(Raspored r)
        {
            this.Id_Odeljenja = r.Id_Odeljenja.Id;
            this.Id_Predmeta = r.Id_Predmeta.Id;
            this.Cas = r.Cas;
            this.Dan = r.Dan;
        }
    }
}
