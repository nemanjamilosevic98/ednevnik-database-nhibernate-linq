﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using eDnevnik.Entiteti;
using NHibernate;
using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;

namespace eDnevnik.DTOs
{
    public class BrojTelefonaView
    {
        public virtual string BrojTelefonaKorisnika { get; set; }
        public virtual int PripadaKorisniku { get; set; }

        public BrojTelefonaView()
        {

        }

        public BrojTelefonaView (BrojTelefona br)
        {
            this.BrojTelefonaKorisnika = br.BrojTelefonaKorisnika;
            this.PripadaKorisniku = br.PripadaKorisniku.Id;
        }
    }
}
