﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using eDnevnik.Entiteti;
using NHibernate;
using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;

namespace eDnevnik.DTOs
{
    public class KorisnikView
    {
        public virtual string Ime { get; set; }
        public virtual string Prezime { get; set; }
        public virtual DateTime DatumRodjenja { get; set; }
        public virtual char Pol { get; set; }
        public virtual string JMBG { get; set; }
        public virtual string Username { get; set; }
        public virtual string Password { get; set; }
        public virtual int FAdministrator { get; set; }
        public virtual int FRoditelj { get; set; }
        public virtual int FNastavnik { get; set; }
        public virtual int FUcenik { get; set; }

        public KorisnikView()
        {
        }

        public KorisnikView(Korisnik k)
        {
            this.Ime = k.Ime;
            this.Prezime = k.Prezime;
            this.DatumRodjenja = k.DatumRodjenja;
            this.Pol = k.Pol;
            this.JMBG = k.JMBG;
            this.Username = k.Username;
            this.Password = k.Password;
            this.FAdministrator = k.FAdministrator;
            this.FRoditelj = k.FRoditelj;
            this.FNastavnik = k.FNastavnik;
            this.FUcenik = k.FUcenik;
        }
    }

    public class AdministratorView: KorisnikView
    {
        public AdministratorView() : base()
        {

        }

        public AdministratorView(Administrator admin):base(admin)
        {

        }
    }

    public class NastavnikView : KorisnikView
    {
        public virtual string SSSpreme { get; set; }

        public NastavnikView() : base()
        {

        }

        public NastavnikView(Nastavnik nas) : base(nas)
        {
            this.SSSpreme = nas.SSSpreme;
        }
    }

    public class RoditeljView : KorisnikView
    {
        public RoditeljView() : base()
        {

        }

        public RoditeljView(Roditelj rod) : base(rod)
        {

        }
    }

    public class UcenikView : KorisnikView
    {
        public virtual int BrOpravdanih { get; set; }
        public virtual int BrNeopravdanih { get; set; }
        public virtual int OcenaVladanje { get; set; }

        public UcenikView() : base()
        {
           
        }

        public UcenikView(Ucenik uc): base(uc)
        {
            this.BrOpravdanih = uc.BrOpravdanih;
            this.BrNeopravdanih = uc.BrNeopravdanih;
            this.OcenaVladanje = uc.OcenaVladanje;
        }
    }

    public class NastavnikRoditeljView : KorisnikView
    {
        public virtual string SSSpreme { get; set; }

        public NastavnikRoditeljView() : base()
        {

        }

        public NastavnikRoditeljView(NastavnikRoditelj nasrod) : base(nasrod)
        {

        }
    }


}
