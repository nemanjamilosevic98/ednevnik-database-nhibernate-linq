﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using eDnevnik.Entiteti;
using NHibernate;
using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;

namespace eDnevnik.DTOs
{
    public class GodinaView
    {
        public virtual int NaGodini { get; set; }
        public virtual int PredmeT { get; protected set; }

        public GodinaView()
        {
        }

        public GodinaView(Godina g)
        {
            this.NaGodini = g.NaGodini;
            this.PredmeT = g.PredmeT.Id;
        }
    }
}
