﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using eDnevnik.Entiteti;
using NHibernate;
using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;

namespace eDnevnik.DTOs
{
    public class JeRazredniView
    {
        public virtual int Id_Odeljenja { get; set; }
        public virtual int Id_Nastavnika { get; set; }
        public virtual DateTime DatumOd { get; set; }
        public virtual DateTime? DatumDo { get; set; }

        public JeRazredniView()
        {

        }

        public JeRazredniView(JeRazredni jeraz)
        {
            this.Id_Nastavnika = jeraz.Id_Nastavnika.Id;
            this.Id_Odeljenja =jeraz.Id_Odeljenja.Id;
            this.DatumOd = jeraz.DatumOd;
            this.DatumDo = jeraz.DatumDo;
        }

    }
}
