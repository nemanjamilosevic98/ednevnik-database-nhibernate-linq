﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using eDnevnik.Entiteti;
using NHibernate;
using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;

namespace eDnevnik.DTOs
{
    public class ImaOcenuView
    {
        public virtual int Id_Ucenika { get; set; }
        public virtual int Id_Predmeta { get; set; }
        public virtual string TipOcene { get; set; }
        public virtual int Vrednost { get; set; }
        public virtual string Opis { get; set; }

        public ImaOcenuView()
        {
        }

        public ImaOcenuView(ImaOcenu ioc)
        {
            this.Id_Ucenika = ioc.Id_Ucenika.Id;
            this.Id_Predmeta = ioc.Id_Predmeta.Id;
            this.TipOcene = ioc.TipOcene;
            this.Vrednost = ioc.Vrednost;
            this.Opis = ioc.Opis;
        }
    }
}
