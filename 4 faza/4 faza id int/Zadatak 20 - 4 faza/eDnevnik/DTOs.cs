﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eDnevnik
{

    public class ImaOcenuPogled
    {
        public int id;
        public int UcenikId;
        public int PredmetId;
        public string TipOcene;
        public int Vrednost;
        public string Opis;

        public ImaOcenuPogled()
        {

        }

        public ImaOcenuPogled(int ImaOcenuId, int UcenikId, int PredmetId, string TipOcene, int Vrednost, string Opis)
        {
            this.id = ImaOcenuId;
            this.UcenikId = UcenikId;
            this.PredmetId = PredmetId;
            this.TipOcene = TipOcene;
            this.Vrednost = Vrednost;
            this.Opis = Opis;
        }
    }

    public class jeRazredniPogled
    {
        public int id;
        public int NastavnikId;
        public int OdeljenjeId;
        public DateTime DatumOd;
        public DateTime? DatumDo;

        public jeRazredniPogled()
        {

        }

        public jeRazredniPogled(int jeRazredniId, int NastavnikId, int OdeljenjeId, DateTime datumOd, DateTime? datumDo)
        {
            this.id = jeRazredniId;
            this.NastavnikId = NastavnikId;
            this.OdeljenjeId = OdeljenjeId;
            this.DatumDo = datumDo;
            this.DatumOd = datumOd;
        }
    }

    public class RasporedPogled
    {
        public int id;
        public int OdeljenjeId;
        public int PredmetId;
        public string Dan;
        public int Cas;

        public RasporedPogled()
        {

        }

        public RasporedPogled(int RasporedId, int OdeljenjeId, int PredmetId, string Dan, int Cas)
        {
            this.id = RasporedId;
            this.OdeljenjeId = OdeljenjeId;
            this.PredmetId = PredmetId;
            this.Dan = Dan;
            this.Cas = Cas;
        }
    }

    public class FunkcijaPogled
    {
        public int id;
        public int RoditeljId;
        public DateTime DatumOd;
        public DateTime? DatumDo;
        public string Tip;

        public FunkcijaPogled()
        {

        }

        public FunkcijaPogled(int FunkcijaId, int RoditeljId, DateTime DatumOd, DateTime? DatumDo, string TipFunkcije)
        {
            this.id = FunkcijaId;
            this.RoditeljId = RoditeljId;
            this.DatumOd = DatumOd;
            this.DatumDo = DatumDo;
            this.Tip = TipFunkcije;
        }
    }

    public class PredstavljaPogled
    {
        public int id;
        public int OdeljenjeId;
        public int RoditeljId;
        public DateTime DatumOd;
        public DateTime? DatumDo;

        public PredstavljaPogled()
        {

        }

        public PredstavljaPogled(int PredstavljaId, int OdeljenjeId, int RoditeljId, DateTime DatumOd, DateTime? DatumDo)
        {
            this.id = PredstavljaId;
            this.OdeljenjeId = OdeljenjeId;
            this.RoditeljId = RoditeljId;
            this.DatumOd = DatumOd;
            this.DatumDo = DatumDo;
        }

    }

    public class GodinaPogled
    {
        public int id;
        public int PredmetId;
        public int NaGodini;

        public GodinaPogled()
        {

        }

        public GodinaPogled(int godinaId, int predmetId, int godina)
        {
            this.id = godinaId;
            this.PredmetId = predmetId;
            this.NaGodini = godina;
        }
    }

    public class BrojTelefonaPogled
    {
        public int id;
        public int KorisnikId;
        public string BrojTelefonaKorisnika;

        public BrojTelefonaPogled()
        {

        }

        public BrojTelefonaPogled(int Id, int KorisnikId, string broj)
        {
            this.id = Id;
            this.KorisnikId = KorisnikId;
            this.BrojTelefonaKorisnika = broj;
        }
    }

    public class UcenikPogled
    {
        public int id { get; set; }
        public string Ime { get; set; }
        public string Prezime { get; set; }
        public DateTime DatumRodjenja { get; set; }
        public char Pol { get; set; }
        public string JMBG { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public int FAdministrator { get; set; }
        public int FUcenik { get; set; }
        public int FNastavnik { get; set; }
        public int FRoditelj { get; set; }
        public int BrOpravdanih { get; set; }
        public int BrNeopravdanih { get; set; }
        public int OdeljenjeId { get; set; }
        public int OcenaVladanje { get; set; }

        public UcenikPogled(int KorisnikId, string Ime, string Prezime, DateTime DatumRodjenja, string KorisnickoIme, string Sifra, int FAdministrator,
            int FUcenik, int FNastavnik, int FRoditelj,int OdeljenjeId, int Opravdani, int Neopravdani, int OcenaVladanje,char pol, string jmbg)
        {
            this.id = KorisnikId;
            this.Ime = Ime;
            this.Prezime = Prezime;
            this.DatumRodjenja = DatumRodjenja;
            this.Username = KorisnickoIme;
            this.Password = Sifra;
            this.FAdministrator = FAdministrator;
            this.FUcenik = FUcenik;
            this.FNastavnik = FNastavnik;
            this.FRoditelj = FRoditelj;
            this.OdeljenjeId = OdeljenjeId;
            this.BrOpravdanih = Opravdani;
            this.BrNeopravdanih = Neopravdani;
            this.OcenaVladanje = OcenaVladanje;
            this.Pol = pol;
            this.JMBG = jmbg;
        }

        public UcenikPogled()
        {

        }
    }

    public class NastavnikPogled
    {
        public int id { get; set; }
        public string Ime { get; set; }
        public string Prezime { get; set; }
        public DateTime DatumRodjenja { get; set; }
        public char Pol { get; set; }
        public string JMBG { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public int FAdministrator { get; set; }
        public int FUcenik { get; set; }
        public int FNastavnik { get; set; }
        public int FRoditelj { get; set; }
        public string SSSpreme { get; set; }

        public NastavnikPogled(int KorisnikId, string Ime, string Prezime, DateTime DatumRodjenja, string KorisnickoIme, string Sifra, int FAdministrator,
            int FUcenik, int FNastavnik, int FRoditelj, string StrucnaSprema, char pol, string jmbg)
        {
            this.id = KorisnikId;
            this.Ime = Ime;
            this.Prezime = Prezime;
            this.DatumRodjenja = DatumRodjenja;
            this.Username = KorisnickoIme;
            this.Password = Sifra;
            this.FAdministrator = FAdministrator;
            this.FUcenik = FUcenik;
            this.FNastavnik = FNastavnik;
            this.FRoditelj = FRoditelj;
            this.SSSpreme = StrucnaSprema;
            this.Pol = pol;
            this.JMBG = jmbg;
        }

        public NastavnikPogled()
        {

        }
    }

    public class RoditeljPogled
    {
        public int id { get; set; }
        public string Ime { get; set; }
        public string Prezime { get; set; }
        public DateTime DatumRodjenja { get; set; }
        public char Pol { get; set; }
        public string JMBG { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public int FAdministrator { get; set; }
        public int FUcenik { get; set; }
        public int FNastavnik { get; set; }
        public int FRoditelj { get; set; }

        public RoditeljPogled(int KorisnikId, string Ime, string Prezime, DateTime DatumRodjenja, string KorisnickoIme, string Sifra, int FAdministrator,
            int FUcenik, int FNastavnik, int FRoditelj,char pol, string jmbg)
        {
            this.id = KorisnikId;
            this.Ime = Ime;
            this.Prezime = Prezime;
            this.DatumRodjenja = DatumRodjenja;
            this.Username = KorisnickoIme;
            this.Password = Sifra;
            this.FAdministrator = FAdministrator;
            this.FUcenik = FUcenik;
            this.FNastavnik = FNastavnik;
            this.FRoditelj = FRoditelj;
            this.Pol = pol;
            this.JMBG = jmbg;
        }

        public RoditeljPogled()
        {

        }
    }

    public class AdministratorPogled
    {
        public int id { get; set; }
        public string Ime { get; set; }
        public string Prezime { get; set; }
        public DateTime DatumRodjenja { get; set; }
        public char Pol { get; set; }
        public string JMBG { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public int FAdministrator { get; set; }
        public int FUcenik { get; set; }
        public int FNastavnik { get; set; }
        public int FRoditelj { get; set; }

        public AdministratorPogled(int KorisnikId, string Ime, string Prezime, DateTime DatumRodjenja, string KorisnickoIme, string Sifra, int FAdministrator,
            int FUcenik, int FNastavnik, int FRoditelj,char pol, string jmbg)
        {
            this.id = KorisnikId;
            this.Ime = Ime;
            this.Prezime = Prezime;
            this.DatumRodjenja = DatumRodjenja;
            this.Username = KorisnickoIme;
            this.Password = Sifra;
            this.FAdministrator = FAdministrator;
            this.FUcenik = FUcenik;
            this.FNastavnik = FNastavnik;
            this.FRoditelj = FRoditelj;
            this.Pol = pol;
            this.JMBG = jmbg;
        }

        public AdministratorPogled()
        {

        }
    }

    public class NastavnikRoditeljPogled
    {
        public int id { get; set; }
        public string Ime { get; set; }
        public string Prezime { get; set; }
        public DateTime DatumRodjenja { get; set; }
        public char Pol { get; set; }
        public string JMBG { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public int FAdministrator { get; set; }
        public int FUcenik { get; set; }
        public int FNastavnik { get; set; }
        public int FRoditelj { get; set; }
        public string StrucnaSprema { get; set; }

        public NastavnikRoditeljPogled(int KorisnikId, string Ime, string Prezime, DateTime DatumRodjenja, string KorisnickoIme, string Sifra, int FAdministrator,
            int FUcenik, int FNastavnik, int FRoditelj, string StrucnaSprema,char pol, string jmbg)
        {
            this.id = KorisnikId;
            this.Ime = Ime;
            this.Prezime = Prezime;
            this.DatumRodjenja = DatumRodjenja;
            this.Username = KorisnickoIme;
            this.Password = Sifra;
            this.FAdministrator = FAdministrator;
            this.FUcenik = FUcenik;
            this.FNastavnik = FNastavnik;
            this.FRoditelj = FRoditelj;
            this.StrucnaSprema = StrucnaSprema;
            this.Pol = pol;
            this.JMBG = jmbg;
        }

        public NastavnikRoditeljPogled()
        {

        }
    }

    public class PredmetPogled
    {
        public virtual int Id { get; set; }
        public virtual string Naziv { get; set; }
        public virtual string Opis { get; set; }
        public virtual int BrojCasova { get; set; }
        public virtual string TipPredmeta { get; set; }
        public virtual int MinBrojUcenika { get; set; }
        public virtual string BlokNastava { get; set; }
        public virtual string SpecijalanKabinet { get; set; }

        public PredmetPogled()
        {

        }

        public PredmetPogled(int id, string naziv, string opis, int brcas, string tip, int minUcen, string blok, string spec)
        {
            this.Id = id;
            this.Naziv = naziv;
            this.Opis = opis;
            this.BrojCasova = brcas;
            this.TipPredmeta = tip;
            this.MinBrojUcenika = minUcen;
            this.BlokNastava = blok;
            this.SpecijalanKabinet = spec;
        }
    }

    public class OdeljenjePogled
    {
        public virtual int Id { get; set; }
        public virtual string Naziv { get; set; }
        public virtual string Smer { get; set; }

        public OdeljenjePogled() { }

        public OdeljenjePogled(int id, string naziv, string smer)
        {
            this.Id = id;
            this.Naziv = naziv;
            this.Smer = smer;

        }
    }

    public class DrziCasPogled
    {
        public int id { get; set; }
        public int NastavnikId { get; set; }
        public int PredmetId { get; set; }
        public int OdeljenjeId { get; set; }

        public DrziCasPogled()
        {

        }

        public DrziCasPogled(int DrziCasId, int NastavnikId, int PredmetId, int OdeljenjeId)
        {
            this.id = DrziCasId;
            this.NastavnikId = NastavnikId;
            this.PredmetId = PredmetId;
            this.OdeljenjeId = OdeljenjeId;
        }

    }

    public class jeRoditeljPogled
    {
        public int id;
        public int RoditeljId;
        public int UcenikId;

        public jeRoditeljPogled()
        {

        }

        public jeRoditeljPogled(int jeRoditeljId, int RoditeljId, int UcenikId)
        {
            this.id = jeRoditeljId;
            this.RoditeljId = RoditeljId;
            this.UcenikId = UcenikId;
        }
    }

}