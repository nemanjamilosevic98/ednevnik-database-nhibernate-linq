﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Net;
using System.Net.Http;
using System.Web.Http;//instaliran Microsoft.AspNet.WebApi.Core 
using eDnevnik;
using eDnevnik.DTOs;
using eDnevnik.Entiteti;

namespace WebApplication.Controllers
{
    public class DrziCasController : ApiController
    {
        // GET api/DrziCas
        public IEnumerable<DrziCasPogled> Get()
        {
            DataProvider provider = new DataProvider();
            IEnumerable<DrziCasPogled> drzecasove = provider.vratiSveDrziCas();
            return drzecasove;
        }

        // GET api/DrziCas/id
        public DrziCasView Get(int id)
        {
            DataProvider provider = new DataProvider();
            return provider.vratiDrziCasView(id);
        }

        // POST api/DrziCas
        public int Post([FromBody]DrziCas drz)
        {
            DataProvider provider = new DataProvider();
            return provider.dodajDrziCas(drz);
        }

        // PUT api/DrziCas/id
        public int Put(int id)
        {
            DataProvider provider = new DataProvider();
            return provider.azurirajDrziCas(id);
        }

        // DELETE api/DrziCas/id
        public int Delete(int id)
        {
            DataProvider provider = new DataProvider();
            return provider.ukloniDrziCas(id);
        }
    }
}