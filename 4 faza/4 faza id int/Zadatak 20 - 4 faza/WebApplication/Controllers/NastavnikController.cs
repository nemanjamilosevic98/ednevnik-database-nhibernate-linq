﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Net;
using System.Net.Http;
using System.Web.Http;//instaliran Microsoft.AspNet.WebApi.Core 
using eDnevnik;
using eDnevnik.DTOs;
using eDnevnik.Entiteti;

namespace WebApplication.Controllers
{
    public class NastavnikController : ApiController
    {
        // GET api/Nastavnik
        public IEnumerable<NastavnikPogled> Get()
        {
            DataProvider provider = new DataProvider();
            IEnumerable<NastavnikPogled> nastavnici = provider.vratiSveNastavnike();
            return nastavnici;
        }

        // GET api/Nastavnik/id
        public NastavnikView Get(int id)
        {
            DataProvider provider = new DataProvider();
            return provider.vratiNastavnikView(id);
        }

        // POST api/Nastavnik
        public int Post([FromBody]Nastavnik ad)
        {
            DataProvider provider = new DataProvider();
            return provider.dodajNastavnika(ad);
        }

        // PUT api/Nastavnik/id
        public int Put(int id)
        {
            DataProvider provider = new DataProvider();
            return provider.azurirajNastavnika(id);
        }

        // DELETE api/Nastavnik/id
        public int Delete(int id)
        {
            DataProvider provider = new DataProvider();
            return provider.ukloniNastavnika(id);
        }
    }
}