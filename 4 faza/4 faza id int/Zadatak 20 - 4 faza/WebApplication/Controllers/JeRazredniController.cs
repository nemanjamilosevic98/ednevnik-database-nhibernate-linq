﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Net;
using System.Net.Http;
using System.Web.Http;//instaliran Microsoft.AspNet.WebApi.Core 
using eDnevnik;
using eDnevnik.DTOs;
using eDnevnik.Entiteti;

namespace WebApplication.Controllers
{
    public class JeRazredniController : ApiController
    {
        // GET api/JeRazredni
        public IEnumerable<jeRazredniPogled> Get()
        {
            DataProvider provider = new DataProvider();
            IEnumerable<jeRazredniPogled> jerazr = provider.vratiSveJeRazredni();
            return jerazr;
        }

        // GET api/JeRazredni/id
        public JeRazredniView Get(int id)
        {
            DataProvider provider = new DataProvider();
            return provider.vratiJeRazredniView(id);
        }

        // POST api/JeRazredni
        public int Post([FromBody]JeRazredni Od)
        {
            DataProvider provider = new DataProvider();
            return provider.dodajJeRazredni(Od);
        }

        // PUT api/JeRazredni/id
        public int Put(int id)
        {
            DataProvider provider = new DataProvider();
            return provider.azurirajJeRazredni(id);
        }

        // DELETE api/JeRazredni/id
        public int Delete(int id)
        {
            DataProvider provider = new DataProvider();
            return provider.ukloniJeRazredni(id);
        }
    }
}