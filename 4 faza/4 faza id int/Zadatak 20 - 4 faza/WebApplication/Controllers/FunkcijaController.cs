﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Net;
using System.Net.Http;
using System.Web.Http;//instaliran Microsoft.AspNet.WebApi.Core 
using eDnevnik;
using eDnevnik.DTOs;
using eDnevnik.Entiteti;

namespace WebApplication.Controllers
{
    public class FunkcijaController : ApiController
    {
        // GET api/Funkcija
        public IEnumerable<FunkcijaPogled> Get()
        {
            DataProvider provider = new DataProvider();
            IEnumerable<FunkcijaPogled> funkcije = provider.vratiSveFunkcije();
            return funkcije;
        }

        // GET api/Funkcija/id
        public FunkcijaView Get(int id)
        {
            DataProvider provider = new DataProvider();
            return provider.vratiFunkcijaView(id);
        }

        // POST api/Funkcija
        public int Post([FromBody]Funkcija Od)
        {
            DataProvider provider = new DataProvider();
            return provider.dodajFunkciju(Od);
        }

        // PUT api/Funkcija/id
        public int Put(int id)
        {
            DataProvider provider = new DataProvider();
            return provider.azurirajFunkciju(id);
        }

        // DELETE api/Funkcija/id
        public int Delete(int id)
        {
            DataProvider provider = new DataProvider();
            return provider.ukloniFunkciju(id);
        }
    }
}