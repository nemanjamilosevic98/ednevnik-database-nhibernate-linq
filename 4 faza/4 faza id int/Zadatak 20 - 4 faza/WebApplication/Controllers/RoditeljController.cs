﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Net;
using System.Net.Http;
using System.Web.Http;//instaliran Microsoft.AspNet.WebApi.Core 
using eDnevnik;
using eDnevnik.DTOs;
using eDnevnik.Entiteti;

namespace WebApplication.Controllers
{
    public class RoditeljController : ApiController
    {
        // GET api/Roditelj
        public IEnumerable<RoditeljPogled> Get()
        {
            DataProvider provider = new DataProvider();
            IEnumerable<RoditeljPogled> roditelji = provider.vratiSveRoditelje();
            return roditelji;
        }

        // GET api/Roditelj/id
        public RoditeljView Get(int id)
        {
            DataProvider provider = new DataProvider();
            return provider.vratiRoditeljView(id);
        }

        // POST api/Roditelj
        public int Post([FromBody]Roditelj ad)
        {
            DataProvider provider = new DataProvider();
            return provider.dodajRoditelja(ad);
        }

        // PUT api/Roditelj/id
        public int Put(int id)
        {
            DataProvider provider = new DataProvider();
            return provider.azurirajRoditelja(id);
        }

        // DELETE api/Roditelj/id
        public int Delete(int id)
        {
            DataProvider provider = new DataProvider();
            return provider.ukloniRoditelja(id);
        }
    }
}