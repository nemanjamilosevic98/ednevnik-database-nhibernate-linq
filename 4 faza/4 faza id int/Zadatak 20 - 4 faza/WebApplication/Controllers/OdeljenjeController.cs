﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Net;
using System.Net.Http;
using System.Web.Http;//instaliran Microsoft.AspNet.WebApi.Core 
using eDnevnik;
using eDnevnik.DTOs;
using eDnevnik.Entiteti;

namespace WebApplication.Controllers
{
    public class OdeljenjeController : ApiController
    {
        // GET api/Odeljenje
        public IEnumerable<OdeljenjePogled> Get()
        {
            DataProvider provider = new DataProvider();
            IEnumerable<OdeljenjePogled> Odeljenja = provider.vratiSvaOdeljenja();
            return Odeljenja;
        }

        // GET api/Odeljenje/id
        public OdeljenjeView Get(int id)
        {
            DataProvider provider = new DataProvider();
            return provider.vratiOdeljenjeView(id);
        }

        // POST api/Odeljenje
        public int Post([FromBody]Odeljenje Od)
        {
            DataProvider provider = new DataProvider();
            return provider.dodajOdeljenje(Od);
        }

        // PUT api/Odeljenje/id
        public int Put(int id)
        {
            DataProvider provider = new DataProvider();
            return provider.azurirajOdeljenje(id);
        }

        // DELETE api/Odeljenje/id
        public int Delete(int id)
        {
            DataProvider provider = new DataProvider();
            return provider.ukloniOdeljenje(id);
        }
    }
}