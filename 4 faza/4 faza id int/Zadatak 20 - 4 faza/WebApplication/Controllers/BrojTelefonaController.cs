﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Net;
using System.Net.Http;
using System.Web.Http;//instaliran Microsoft.AspNet.WebApi.Core 
using eDnevnik;
using eDnevnik.DTOs;
using eDnevnik.Entiteti;

namespace WebApplication.Controllers
{
    public class BrojTelefonaController : ApiController
    {
        // GET api/BrojTelefona
        public IEnumerable<BrojTelefonaPogled> Get()
        {
            DataProvider provider = new DataProvider();
            IEnumerable<BrojTelefonaPogled> brojevi = provider.vratiSveBrojeveTelefona();
            return brojevi;
        }

        // GET api/BrojTelefona/id
        public BrojTelefonaView Get(int id)
        {
            DataProvider provider = new DataProvider();
            return provider.vratiBrojTelefonaView(id);
        }

        // POST api/BrojTelefona
        public int Post([FromBody]BrojTelefona Od)
        {
            DataProvider provider = new DataProvider();
            return provider.dodajBrojTelefona(Od);
        }

        // PUT api/BrojTelefona/id
        public int Put(int id)
        {
            DataProvider provider = new DataProvider();
            return provider.azurirajBrojTelefona(id);
        }

        // DELETE api/BrojTelefona/id
        public int Delete(int id)
        {
            DataProvider provider = new DataProvider();
            return provider.ukloniBrojTelefona(id);
        }
    }
}