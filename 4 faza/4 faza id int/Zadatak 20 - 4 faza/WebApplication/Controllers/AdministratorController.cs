﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Net;
using System.Net.Http;
using System.Web.Http;//instaliran Microsoft.AspNet.WebApi.Core 
using eDnevnik;
using eDnevnik.DTOs;
using eDnevnik.Entiteti;

namespace WebApplication.Controllers
{
    public class AdministratorController : ApiController
    {
        // GET api/Administrator
        public IEnumerable<AdministratorPogled> Get()
        {
            DataProvider provider = new DataProvider();
            IEnumerable<AdministratorPogled> administratori = provider.vratiSveAdministratore();
            return administratori;
        }

        // GET api/Administrator/id
        public AdministratorView Get(int id)
        {
            DataProvider provider = new DataProvider();
            return provider.vratiAdministratorView(id);
        }

        // POST api/Administrator
        public int Post([FromBody]Administrator ad)
        {
            DataProvider provider = new DataProvider();
            return provider.dodajAdministratora(ad);
        }

        // PUT api/Administrator/id
        public int Put(int id)
        {
            DataProvider provider = new DataProvider();
            return provider.azurirajAdministratora(id);
        }

        // DELETE api/Administrator/id
        public int Delete(int id)
        {
            DataProvider provider = new DataProvider();
            return provider.ukloniAdministratora(id);
        }
    }
}