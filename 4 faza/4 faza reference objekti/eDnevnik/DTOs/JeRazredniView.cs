﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using eDnevnik.Entiteti;
using NHibernate;
using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;

namespace eDnevnik.DTOs
{
    public class JeRazredniView
    {
        public virtual OdeljenjeView Id_Odeljenja { get; set; }
        public virtual NastavnikView Id_Nastavnika { get; set; }
        public virtual DateTime DatumOd { get; set; }
        public virtual DateTime? DatumDo { get; set; }

        public JeRazredniView()
        {

        }

        public JeRazredniView(JeRazredni jeraz)
        {
            this.Id_Nastavnika = new NastavnikView(jeraz.Id_Nastavnika);
            this.Id_Odeljenja = new OdeljenjeView(jeraz.Id_Odeljenja);
            this.DatumOd = jeraz.DatumOd;
            this.DatumDo = jeraz.DatumDo;
        }

    }
}
