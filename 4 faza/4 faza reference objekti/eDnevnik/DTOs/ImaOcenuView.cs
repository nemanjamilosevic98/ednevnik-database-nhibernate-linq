﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using eDnevnik.Entiteti;
using NHibernate;
using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;

namespace eDnevnik.DTOs
{
    public class ImaOcenuView
    {
        public virtual UcenikView Id_Ucenika { get; set; }
        public virtual PredmetView Id_Predmeta { get; set; }
        public virtual string TipOcene { get; set; }
        public virtual int Vrednost { get; set; }
        public virtual string Opis { get; set; }

        public ImaOcenuView()
        {
        }

        public ImaOcenuView(ImaOcenu ioc)
        {
            this.Id_Ucenika = new UcenikView(ioc.Id_Ucenika);
            this.Id_Predmeta = new PredmetView(ioc.Id_Predmeta);
            this.TipOcene = ioc.TipOcene;
            this.Vrednost = ioc.Vrednost;
            this.Opis = ioc.Opis;
        }
    }
}
