﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using eDnevnik.Entiteti;
using NHibernate;
using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;

namespace eDnevnik.DTOs
{
    public class PredstavljaView
    {
        public virtual OdeljenjeView Id_Odeljenja { get; set; }
        public virtual RoditeljView Id_Roditelja { get; set; }
        public virtual DateTime DatumOd { get; set; }
        public virtual DateTime? DatumDo { get; set; }

        public PredstavljaView()
        {

        }

        public PredstavljaView(Predstavlja pr)
        {
            this.Id_Odeljenja = new OdeljenjeView(pr.Id_Odeljenja);
            this.Id_Roditelja = new RoditeljView(pr.Id_Roditelja);
            this.DatumOd = pr.DatumOd;
            this.DatumDo = pr.DatumDo;
        }
    }
}
