﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using eDnevnik.Entiteti;
using NHibernate;
using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;

namespace eDnevnik.DTOs
{
    public class RasporedView
    {
        public virtual OdeljenjeView Id_Odeljenja { get; set; }
        public virtual PredmetView Id_Predmeta { get; set; }
        public virtual string Dan { get; set; }
        public virtual int Cas { get; set; }

        public RasporedView()
        {

        }

        public RasporedView(Raspored r)
        {
            this.Id_Odeljenja = new OdeljenjeView(r.Id_Odeljenja);
            this.Id_Predmeta = new PredmetView(r.Id_Predmeta);
            this.Cas = r.Cas;
            this.Dan = r.Dan;
        }
    }
}
