﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using eDnevnik.Entiteti;
using NHibernate;
using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;

namespace eDnevnik.DTOs
{
    public class FunkcijaView
    {
        public virtual DateTime DatumOd { get; set; }
        public virtual DateTime DatumDo { get; set; }
        public virtual string Tip { get; set; }

        public virtual RoditeljView VrsiFunkciju { get; set; }

        public FunkcijaView()
        {

        }

        public FunkcijaView(Funkcija fun)
        {
            this.DatumOd = fun.DatumOd;
            this.DatumDo = fun.DatumDo;
            this.Tip = fun.Tip;
            this.VrsiFunkciju = new RoditeljView(fun.VrsiFunkciju);
        }
    }
}
