﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using eDnevnik.Entiteti;
using NHibernate;
using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;

namespace eDnevnik.DTOs
{
    public class OdeljenjeView
    {      
        public virtual string Naziv { get; set; }
        public virtual string Smer { get; set; }

        public OdeljenjeView()
        {
        }

        public OdeljenjeView(Odeljenje o)
        {
            this.Naziv = o.Naziv;
            this.Smer = o.Smer;
        }
    }
}
