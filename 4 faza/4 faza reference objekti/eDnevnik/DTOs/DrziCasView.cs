﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using eDnevnik.Entiteti;
using NHibernate;
using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;

namespace eDnevnik.DTOs
{
    public class DrziCasView
    {
        public virtual NastavnikView Id_Nastavnika { get; set; }
        public virtual PredmetView Id_Predmeta { get; set; }
        public virtual OdeljenjeView Id_Odeljenja { get; set; }

        public DrziCasView()
        {
        }

        public DrziCasView(DrziCas dr)
        {
            this.Id_Nastavnika = new NastavnikView(dr.Id_Nastavnika);
            this.Id_Predmeta = new PredmetView(dr.Id_Predmeta);
            this.Id_Odeljenja = new OdeljenjeView(dr.Id_Odeljenja);
        }
    }
}
