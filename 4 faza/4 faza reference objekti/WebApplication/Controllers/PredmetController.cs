﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Net;
using System.Net.Http;
using System.Web.Http;//instaliran Microsoft.AspNet.WebApi.Core 
using eDnevnik;
using eDnevnik.DTOs;
using eDnevnik.Entiteti;

namespace WebApplication.Controllers
{
    public class PredmetController : ApiController
    {
        // GET api/Predmet
        public IEnumerable<PredmetPogled> Get()
        {
            DataProvider provider = new DataProvider();
            IEnumerable<PredmetPogled> Predmeti = provider.vratiSvePredmete();
            return Predmeti;
        }

        // GET api/Predmet/id
        public PredmetView Get(int id)
        {
            DataProvider provider = new DataProvider();
            return provider.vratiPredmetView(id);
        }

        // POST api/Predmet
        public int Post([FromBody]Predmet SK)
        {
            DataProvider provider = new DataProvider();
            return provider.dodajPredmet(SK);
        }

        // PUT api/Predmet/id
        public int Put(int id)
        {
            DataProvider provider = new DataProvider();
            return provider.azurirajPredmet(id);
        }

        // DELETE api/Predmet/id
        public int Delete(int id)
        {
            DataProvider provider = new DataProvider();
            return provider.ukloniPredmet(id);
        }

    }
}