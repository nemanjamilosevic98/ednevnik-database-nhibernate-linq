﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Zadatak16_StambeniKompleks;
using Zadatak16_StambeniKompleks.Entiteti;
using Zadatak16_StambeniKompleks.DTOs;

namespace WebApplication.Controllers
{
    public class TeretniLiftController : ApiController
    {
        // GET api/TeretniLift
        public IEnumerable<TeretniLift> Get()
        {
            DataProvider provider = new DataProvider();
            IEnumerable<TeretniLift> teretniLiftovi = provider.vratiSveTeretniLifte();
            return teretniLiftovi;
        }

        // GET api/TeretniLift/id
        public TeretniLiftView Get(int id)
        {
            DataProvider provider = new DataProvider();
            return provider.vratiTeretniLiftView(id);
        }

        // POST api/TeretniLift
        public int Post([FromBody]TeretniLift SK)
        {
            DataProvider provider = new DataProvider();
            return provider.dodajTeretniLift(SK);
        }

        // PUT api/TeretniLift/id
        public int Put(int id)
        {
            DataProvider provider = new DataProvider();
            return provider.azurirajTeretniLift(id);
        }

        // DELETE api/TeretniLift/id
        public int Delete(int id)
        {
            DataProvider provider = new DataProvider();
            return provider.ukloniTeretniLift(id);
        }
    }
}
