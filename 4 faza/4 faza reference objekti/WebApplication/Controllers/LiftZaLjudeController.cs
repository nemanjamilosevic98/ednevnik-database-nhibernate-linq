﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Zadatak16_StambeniKompleks;
using Zadatak16_StambeniKompleks.Entiteti;
using Zadatak16_StambeniKompleks.DTOs;

namespace WebApplication.Controllers
{
    public class LiftZaLjudeController : ApiController
    {

        // GET api/LiftZaLjude
        public IEnumerable<LiftZaLjude> Get()
        {
            DataProvider provider = new DataProvider();
            IEnumerable<LiftZaLjude> liftoviZaLjude = provider.vratiSveLiftZaLjudee();
            return liftoviZaLjude;
        }

        // GET api/LiftZaLjude/id
        public LiftZaLjudeView Get(int id)
        {
            DataProvider provider = new DataProvider();
            return provider.vratiLiftZaLjudeView(id);
        }

        // POST api/LiftZaLjude
        public int Post([FromBody]LiftZaLjude SK)
        {
            DataProvider provider = new DataProvider();
            return provider.dodajLiftZaLjude(SK);
        }

        // PUT api/LiftZaLjude/id
        public int Put(int id)
        {
            DataProvider provider = new DataProvider();
            return provider.azurirajLiftZaLjude(id);
        }

        // DELETE api/LiftZaLjude/id
        public int Delete(int id)
        {
            DataProvider provider = new DataProvider();
            return provider.ukloniLiftZaLjude(id);
        }
    }
}
