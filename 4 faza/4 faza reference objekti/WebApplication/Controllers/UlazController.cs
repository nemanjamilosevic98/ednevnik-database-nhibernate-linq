﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Zadatak16_StambeniKompleks;
using Zadatak16_StambeniKompleks.Entiteti;
using Zadatak16_StambeniKompleks.DTOs;


namespace WebApplication.Controllers
{
    public class UlazController : ApiController
    {
        // GET api/Ulaz
        public IEnumerable<Ulaz> Get()
        {
            DataProvider provider = new DataProvider();
            IEnumerable<Ulaz> ulazi = provider.vratiSveUlaze();
            return ulazi;
        }

        // GET api/Ulaz/id
        public UlazView Get(int id)
        {
            DataProvider provider = new DataProvider();
            return provider.vratiUlazView(id);
        }

        // POST api/Ulaz
        public int Post([FromBody]Ulaz SK)
        {
            DataProvider provider = new DataProvider();
            return provider.dodajUlaz(SK);
        }

        // PUT api/Ulaz/id
        public int Put(int id)
        {
            DataProvider provider = new DataProvider();
            return provider.azurirajUlaz(id);
        }

        // DELETE api/Ulaz/id
        public int Delete(int id)
        {
            DataProvider provider = new DataProvider();
            return provider.ukloniUlaz(id);
        }
    }
}
