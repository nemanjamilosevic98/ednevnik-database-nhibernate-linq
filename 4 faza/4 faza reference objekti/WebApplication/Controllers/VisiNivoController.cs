﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Zadatak16_StambeniKompleks;
using Zadatak16_StambeniKompleks.Entiteti;
using Zadatak16_StambeniKompleks.DTOs;

namespace WebApplication.Controllers
{
    public class VisiNivoController : ApiController
    {

        // GET api/VisiNivo
        public IEnumerable<Nivo> Get()
        {
            DataProvider provider = new DataProvider();
            IEnumerable<Nivo> nivoi = provider.vratiSveVisiNivoe();
            return nivoi;
        }

        // GET api/VisiNivo/id
        public VisiNivoView Get(int id)
        {
            DataProvider provider = new DataProvider();
            return provider.vratiVisiNivoView(id);
        }

        // POST api/VisiNivo
        public int Post([FromBody]VisiNivo SK)
        {
            DataProvider provider = new DataProvider();
            return provider.dodajVisiNivo(SK);
        }

        // PUT api/VisiNivo/id
        public int Put(int id)
        {
            DataProvider provider = new DataProvider();
            return provider.azurirajVisiNivo(id);
        }

        // DELETE api/VisiNivo/id
        public int Delete(int id)
        {
            DataProvider provider = new DataProvider();
            return provider.ukloniVisiNivo(id);
        }
    }
}
