﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Net;
using System.Net.Http;
using System.Web.Http;//instaliran Microsoft.AspNet.WebApi.Core 
using eDnevnik;
using eDnevnik.DTOs;
using eDnevnik.Entiteti;

namespace WebApplication.Controllers
{
    public class ImaOcenuController : ApiController
    {
        // GET api/ImaOcenu
        public IEnumerable<ImaOcenuPogled> Get()
        {
            DataProvider provider = new DataProvider();
            IEnumerable<ImaOcenuPogled> ocene = provider.vratiSveImaOcenu();
            return ocene;
        }

        // GET api/ImaOcenu/id
        public ImaOcenuView Get(int id)
        {
            DataProvider provider = new DataProvider();
            return provider.vratiImaOcenuView(id);
        }

        // POST api/ImaOcenu
        public int Post([FromBody]ImaOcenu Od)
        {
            DataProvider provider = new DataProvider();
            return provider.dodajImaOcenu(Od);
        }

        // PUT api/ImaOcenu/id
        public int Put(int id)
        {
            DataProvider provider = new DataProvider();
            return provider.azurirajImaOcenu(id);
        }

        // DELETE api/ImaOcenu/id
        public int Delete(int id)
        {
            DataProvider provider = new DataProvider();
            return provider.ukloniImaOcenu(id);
        }
    }
}