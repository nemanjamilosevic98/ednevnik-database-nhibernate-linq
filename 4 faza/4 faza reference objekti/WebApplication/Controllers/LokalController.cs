﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Zadatak16_StambeniKompleks;
using Zadatak16_StambeniKompleks.Entiteti;
using Zadatak16_StambeniKompleks.DTOs;

namespace WebApplication.Controllers
{
    public class LokalController : ApiController
    {

        // GET api/Lokal
        public IEnumerable<Lokal> Get()
        {
            DataProvider provider = new DataProvider();
            IEnumerable<Lokal> lokali = provider.vratiSveLokale();
            return lokali;
        }

        // GET api/Lokal/id
        public LokalView Get(int id)
        {
            DataProvider provider = new DataProvider();
            return provider.vratiLokalView(id);
        }

        // POST api/Lokal
        public int Post([FromBody]Lokal SK)
        {
            DataProvider provider = new DataProvider();
            return provider.dodajLokal(SK);
        }

        // PUT api/Lokal/id
        public int Put(int id)
        {
            DataProvider provider = new DataProvider();
            return provider.azurirajLokal(id);
        }

        // DELETE api/Lokal/id
        public int Delete(int id)
        {
            DataProvider provider = new DataProvider();
            return provider.ukloniLokal(id);
        }
    }
}
