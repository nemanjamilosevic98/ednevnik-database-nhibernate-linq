﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Zadatak16_StambeniKompleks;
using Zadatak16_StambeniKompleks.Entiteti;
using Zadatak16_StambeniKompleks.DTOs;

namespace WebApplication.Controllers
{
    public class NiziNivoController : ApiController
    {
        // GET api/NiziNivo
        public IEnumerable<Nivo> Get()
        {
            DataProvider provider = new DataProvider();
            IEnumerable<Nivo> nivoi = provider.vratiSveNiziNivoe();
            return nivoi;
        }

        // GET api/NiziNivo/id
        public NiziNivoView Get(int id)
        {
            DataProvider provider = new DataProvider();
            return provider.vratiNiziNivoView(id);
        }

        // POST api/NiziNivo
        public int Post([FromBody]NiziNivo SK)
        {
            DataProvider provider = new DataProvider();
            return provider.dodajNiziNivo(SK);
        }

        // PUT api/NiziNivo/id
        public int Put(int id)
        {
            DataProvider provider = new DataProvider();
            return provider.azurirajNiziNivo(id);
        }

        // DELETE api/NiziNivo/id
        public int Delete(int id)
        {
            DataProvider provider = new DataProvider();
            return provider.ukloniNiziNivo(id);
        }
    }
}
