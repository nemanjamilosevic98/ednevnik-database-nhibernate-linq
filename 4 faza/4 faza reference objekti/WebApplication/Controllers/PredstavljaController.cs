﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Net;
using System.Net.Http;
using System.Web.Http;//instaliran Microsoft.AspNet.WebApi.Core 
using eDnevnik;
using eDnevnik.DTOs;
using eDnevnik.Entiteti;

namespace WebApplication.Controllers
{
    public class PredstavljaController : ApiController
    {
        // GET api/Predstavlja
        public IEnumerable<PredstavljaPogled> Get()
        {
            DataProvider provider = new DataProvider();
            IEnumerable<PredstavljaPogled> predstavljanja = provider.vratiSvaPredstavljanja();
            return predstavljanja;
        }

        // GET api/Predstavlja/id
        public PredstavljaView Get(int id)
        {
            DataProvider provider = new DataProvider();
            return provider.vratiPredstavljaView(id);
        }

        // POST api/Predstavlja
        public int Post([FromBody]Predstavlja pr)
        {
            DataProvider provider = new DataProvider();
            return provider.dodajPredstavljanje(pr);
        }

        // PUT api/Predstavlja/id
        public int Put(int id)
        {
            DataProvider provider = new DataProvider();
            return provider.azurirajPredstavljanje(id);
        }

        // DELETE api/Predstavlja/id
        public int Delete(int id)
        {
            DataProvider provider = new DataProvider();
            return provider.ukloniPredstavljanje(id);
        }
    }
}