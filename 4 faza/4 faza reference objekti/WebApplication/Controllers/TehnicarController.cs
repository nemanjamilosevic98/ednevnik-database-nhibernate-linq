﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Zadatak16_StambeniKompleks;
using Zadatak16_StambeniKompleks.Entiteti;
using Zadatak16_StambeniKompleks.DTOs;

namespace WebApplication.Controllers
{
    public class TehnicarController : ApiController
    {

        // GET api/Tehnicar
        public IEnumerable<Tehnicar> Get()
        {
            DataProvider provider = new DataProvider();
            IEnumerable<Tehnicar> tehnicari = provider.vratiSveTehnicare();
            return tehnicari;
        }

        // GET api/Tehnicar/id
        public TehnicarView Get(int id)
        {
            DataProvider provider = new DataProvider();
            return provider.vratiTehnicarView(id);
        }

        // POST api/Tehnicar
        public int Post([FromBody]Tehnicar SK)
        {
            DataProvider provider = new DataProvider();
            return provider.dodajTehnicar(SK);
        }

        // PUT api/Tehnicar/id
        public int Put(int id)
        {
            DataProvider provider = new DataProvider();
            return provider.azurirajTehnicar(id);
        }

        // DELETE api/Tehnicar/id
        public int Delete(int id)
        {
            DataProvider provider = new DataProvider();
            return provider.ukloniTehnicar(id);
        }
    }
}
