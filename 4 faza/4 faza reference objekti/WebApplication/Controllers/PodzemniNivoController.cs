﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Zadatak16_StambeniKompleks;
using Zadatak16_StambeniKompleks.Entiteti;
using Zadatak16_StambeniKompleks.DTOs;


namespace WebApplication.Controllers
{
    public class PodzemniNivoController : ApiController
    {

        // GET api/PodzemniNivo
        public IEnumerable<Nivo> Get()
        {
            DataProvider provider = new DataProvider();
            IEnumerable<Nivo> nivoi= provider.vratiSvePodzemniNivoe();
            return nivoi;
        }

        // GET api/PodzemniNivo/id
        public PodzemniNivoView Get(int id)
        {
            DataProvider provider = new DataProvider();
            return provider.vratiPodzemniNivoView(id);
        }

        // POST api/PodzemniNivo
        public int Post([FromBody]PodzemniNivo SK)
        {
            DataProvider provider = new DataProvider();
            return provider.dodajPodzemniNivo(SK);
        }

        // PUT api/PodzemniNivo/id
        public int Put(int id)
        {
            DataProvider provider = new DataProvider();
            return provider.azurirajPodzemniNivo(id);
        }

        // DELETE api/PodzemniNivo/id
        public int Delete(int id)
        {
            DataProvider provider = new DataProvider();
            return provider.ukloniPodzemniNivo(id);
        }
    }
}
