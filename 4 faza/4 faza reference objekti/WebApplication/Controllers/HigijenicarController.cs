﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Zadatak16_StambeniKompleks;
using Zadatak16_StambeniKompleks.Entiteti;
using Zadatak16_StambeniKompleks.DTOs;

namespace WebApplication.Controllers
{
    public class HigijenicarController : ApiController
    {

        // GET api/Higijenicar
        public IEnumerable<Higijenicar> Get()
        {
            DataProvider provider = new DataProvider();
            IEnumerable<Higijenicar> ulazi = provider.vratiSveHigijenicare();
            return ulazi;
        }

        // GET api/Higijenicar/id
        public HigijenicarView Get(int id)
        {
            DataProvider provider = new DataProvider();
            return provider.vratiHigijenicarView(id);
        }

        // POST api/Higijenicar
        public int Post([FromBody]Higijenicar SK)
        {
            DataProvider provider = new DataProvider();
            return provider.dodajHigijenicar(SK);
        }

        // PUT api/Higijenicar/id
        public int Put(int id)
        {
            DataProvider provider = new DataProvider();
            return provider.azurirajHigijenicar(id);
        }

        // DELETE api/Higijenicar/id
        public int Delete(int id)
        {
            DataProvider provider = new DataProvider();
            return provider.ukloniHigijenicar(id);
        }
    }
}
