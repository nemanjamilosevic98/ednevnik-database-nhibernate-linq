﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Zadatak16_StambeniKompleks;
using Zadatak16_StambeniKompleks.Entiteti;
using Zadatak16_StambeniKompleks.DTOs;

namespace WebApplication.Controllers
{
    public class VrstaPoslovaController : ApiController
    {
        // GET api/VrstaPoslova
        public IEnumerable<VrstaPoslova> Get()
        {
            DataProvider provider = new DataProvider();
            IEnumerable<VrstaPoslova> poslovi = provider.vratiSveVrstaPoslovae();
            return poslovi;
        }

        // GET api/VrstaPoslova/id
        public VrstaPoslovaView Get(int id)
        {
            DataProvider provider = new DataProvider();
            return provider.vratiVrstaPoslovaView(id);
        }

        // POST api/VrstaPoslova
        public int Post([FromBody]VrstaPoslova SK)
        {
            DataProvider provider = new DataProvider();
            return provider.dodajVrstaPoslova(SK);
        }

        // PUT api/VrstaPoslova/id
        public int Put(int id)
        {
            DataProvider provider = new DataProvider();
            return provider.azurirajVrstaPoslova(id);
        }

        // DELETE api/VrstaPoslova/id
        public int Delete(int id)
        {
            DataProvider provider = new DataProvider();
            return provider.ukloniVrstaPoslova(id);
        }
    }
}
