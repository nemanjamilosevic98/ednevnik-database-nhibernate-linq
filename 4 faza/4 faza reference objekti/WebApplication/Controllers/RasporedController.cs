﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Net;
using System.Net.Http;
using System.Web.Http;//instaliran Microsoft.AspNet.WebApi.Core 
using eDnevnik;
using eDnevnik.DTOs;
using eDnevnik.Entiteti;

namespace WebApplication.Controllers
{
    public class RasporedController : ApiController
    {
        // GET api/Raspored
        public IEnumerable<RasporedPogled> Get()
        {
            DataProvider provider = new DataProvider();
            IEnumerable<RasporedPogled> rasporedi = provider.vratiSveRasporede();
            return rasporedi;
        }

        // GET api/Raspored/id
        public RasporedView Get(int id)
        {
            DataProvider provider = new DataProvider();
            return provider.vratiRasporedView(id);
        }

        // POST api/Raspored
        public int Post([FromBody]Raspored ra)
        {
            DataProvider provider = new DataProvider();
            return provider.dodajRaspored(ra);
        }

        // PUT api/Raspored/id
        public int Put(int id)
        {
            DataProvider provider = new DataProvider();
            return provider.azurirajRaspored(id);
        }

        // DELETE api/Raspored/id
        public int Delete(int id)
        {
            DataProvider provider = new DataProvider();
            return provider.ukloniRaspored(id);
        }
    }
}