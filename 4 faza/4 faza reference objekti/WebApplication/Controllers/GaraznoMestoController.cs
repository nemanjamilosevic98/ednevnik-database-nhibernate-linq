﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Zadatak16_StambeniKompleks;
using Zadatak16_StambeniKompleks.Entiteti;
using Zadatak16_StambeniKompleks.DTOs;

namespace WebApplication.Controllers
{
    public class GaraznoMestoController : ApiController
    {

        // GET api/GaraznoMesto
        public IEnumerable<GaraznoMesto> Get()
        {
            DataProvider provider = new DataProvider();
            IEnumerable<GaraznoMesto> mesta = provider.vratiSveGaraznoMestoe();
            return mesta;
        }

        // GET api/GaraznoMesto/id
        public GaraznoMestoView Get(int id)
        {
            DataProvider provider = new DataProvider();
            return provider.vratiGaraznoMestoView(id);
        }

        // POST api/GaraznoMesto
        public int Post([FromBody]GaraznoMesto SK)
        {
            DataProvider provider = new DataProvider();
            return provider.dodajGaraznoMesto(SK);
        }

        // PUT api/GaraznoMesto/id
        public int Put(int id)
        {
            DataProvider provider = new DataProvider();
            return provider.azurirajGaraznoMesto(id);
        }

        // DELETE api/GaraznoMesto/id
        public int Delete(int id)
        {
            DataProvider provider = new DataProvider();
            return provider.ukloniGaraznoMesto(id);
        }
    }
}
