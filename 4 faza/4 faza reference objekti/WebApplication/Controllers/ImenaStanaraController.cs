﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Zadatak16_StambeniKompleks;
using Zadatak16_StambeniKompleks.Entiteti;
using Zadatak16_StambeniKompleks.DTOs;

namespace WebApplication.Controllers
{
    public class ImenaStanaraController : ApiController
    {

        // GET api/ImenaStanara
        public IEnumerable<ImenaStanara> Get()
        {
            DataProvider provider = new DataProvider();
            IEnumerable<ImenaStanara> imena = provider.vratiSveImenaStanarae();
            return imena;
        }

        // GET api/ImenaStanara/id
        public ImenaStanaraView Get(int id)
        {
            DataProvider provider = new DataProvider();
            return provider.vratiImenaStanaraView(id);
        }

        // POST api/ImenaStanara
        public int Post([FromBody]ImenaStanara SK)
        {
            DataProvider provider = new DataProvider();
            return provider.dodajImenaStanara(SK);
        }

        // PUT api/ImenaStanara/id
        public int Put(int id)
        {
            DataProvider provider = new DataProvider();
            return provider.azurirajImenaStanara(id);
        }

        // DELETE api/ImenaStanara/id
        public int Delete(int id)
        {
            DataProvider provider = new DataProvider();
            return provider.ukloniImenaStanara(id);
        }
    }
}
