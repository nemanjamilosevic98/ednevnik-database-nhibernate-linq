﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Zadatak16_StambeniKompleks;
using Zadatak16_StambeniKompleks.Entiteti;
using Zadatak16_StambeniKompleks.DTOs;

namespace WebApplication.Controllers
{
    public class StanController : ApiController
    {
        // GET api/Stan
        public IEnumerable<Stan> Get()
        {
            DataProvider provider = new DataProvider();
            IEnumerable<Stan> stanovi = provider.vratiSveStane();
            return stanovi;
        }

        // GET api/Stan/id
        public StanView Get(int id)
        {
            DataProvider provider = new DataProvider();
            return provider.vratiStanView(id);
        }

        // POST api/Stan
        public int Post([FromBody]Stan SK)
        {
            DataProvider provider = new DataProvider();
            return provider.dodajStan(SK);
        }

        // PUT api/Stan/id
        public int Put(int id)
        {
            DataProvider provider = new DataProvider();
            return provider.azurirajStan(id);
        }

        // DELETE api/Stan/id
        public int Delete(int id)
        {
            DataProvider provider = new DataProvider();
            return provider.ukloniStan(id);
        }
    }
}
