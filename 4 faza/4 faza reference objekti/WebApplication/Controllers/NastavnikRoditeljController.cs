﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Net;
using System.Net.Http;
using System.Web.Http;//instaliran Microsoft.AspNet.WebApi.Core 
using eDnevnik;
using eDnevnik.DTOs;
using eDnevnik.Entiteti;

namespace WebApplication.Controllers
{
    public class NastavnikRoditeljController : ApiController
    {
        // GET api/NastavnikRoditelj
        public IEnumerable<NastavnikRoditeljPogled> Get()
        {
            DataProvider provider = new DataProvider();
            IEnumerable<NastavnikRoditeljPogled> nasrod = provider.vratiSveNastavnikRoditelje();
            return nasrod;
        }

        // GET api/NastavnikRoditelj/id
        public NastavnikRoditeljView Get(int id)
        {
            DataProvider provider = new DataProvider();
            return provider.vratiNastavnikRoditeljView(id);
        }

        // POST api/NastavnikRoditelj
        public int Post([FromBody]NastavnikRoditelj ad)
        {
            DataProvider provider = new DataProvider();
            return provider.dodajNastavnikRoditelja(ad);
        }

        // PUT api/NastavnikRoditelj/id
        public int Put(int id)
        {
            DataProvider provider = new DataProvider();
            return provider.azurirajNastavnikRoditelja(id);
        }

        // DELETE api/NastavnikRoditelj/id
        public int Delete(int id)
        {
            DataProvider provider = new DataProvider();
            return provider.ukloniNastavnikRoditelja(id);
        }
    }
}