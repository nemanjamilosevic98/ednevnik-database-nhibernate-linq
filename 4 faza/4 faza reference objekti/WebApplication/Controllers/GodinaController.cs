﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Net;
using System.Net.Http;
using System.Web.Http;//instaliran Microsoft.AspNet.WebApi.Core 
using eDnevnik;
using eDnevnik.DTOs;
using eDnevnik.Entiteti;

namespace WebApplication.Controllers
{
    public class GodinaController : ApiController
    {
        // GET api/Godina
        public IEnumerable<GodinaPogled> Get()
        {
            DataProvider provider = new DataProvider();
            IEnumerable<GodinaPogled> Godine = provider.vratiSveGodine();
            return Godine;
        }

        // GET api/Godina/id
        public GodinaView Get(int id)
        {
            DataProvider provider = new DataProvider();
            return provider.vratiGodinaView(id);
        }

        // POST api/Godina
        public int Post([FromBody]Godina Od)
        {
            DataProvider provider = new DataProvider();
            return provider.dodajGodinu(Od);
        }

        // PUT api/Godina/id
        public int Put(int id)
        {
            DataProvider provider = new DataProvider();
            return provider.azurirajGodinu(id);
        }

        // DELETE api/Godina/id
        public int Delete(int id)
        {
            DataProvider provider = new DataProvider();
            return provider.ukloniGodinu(id);
        }
    }
}