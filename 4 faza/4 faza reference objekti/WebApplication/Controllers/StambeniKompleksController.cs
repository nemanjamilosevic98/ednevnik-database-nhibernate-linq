﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Zadatak16_StambeniKompleks;
using Zadatak16_StambeniKompleks.Entiteti;
using Zadatak16_StambeniKompleks.DTOs;

namespace WebApplication.Controllers
{
    public class StambeniKompleksController : ApiController
    {

        // GET api/StambeniKompleks
        public IEnumerable<StambeniKompleks> Get()
        {
            DataProvider provider = new DataProvider();
            IEnumerable<StambeniKompleks> stambeniKompleksi = provider.vratiSveStambeneKomplekse();
            return stambeniKompleksi;
        }

        // GET api/StambeniKompleks/id
        public StambeniKompleksView Get(int id)
        {
            DataProvider provider = new DataProvider();
            return provider.vratiStambeniKompleksView(id);
        }

        // POST api/StambeniKompleks
        public int Post([FromBody]StambeniKompleks SK)
        {
            DataProvider provider = new DataProvider();
            return provider.dodajStambeniKompleks(SK);
        }

        // PUT api/StambeniKompleks/id
        public int Put(int id)
        {
            DataProvider provider = new DataProvider();
            return provider.azurirajStambeniKompleks(id);
        }

        // DELETE api/StambeniKompleks/id
        public int Delete(int id)
        {
            DataProvider provider = new DataProvider();
            return provider.ukloniStambeniKompleks(id);
        }
    }
}
