﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Net;
using System.Net.Http;
using System.Web.Http;//instaliran Microsoft.AspNet.WebApi.Core 
using eDnevnik;
using eDnevnik.DTOs;
using eDnevnik.Entiteti;

namespace WebApplication.Controllers
{
    public class UcenikController : ApiController
    {
        // GET api/Ucenik
        public IEnumerable<UcenikPogled> Get()
        {
            DataProvider provider = new DataProvider();
            IEnumerable<UcenikPogled> ucenici = provider.vratiSveUcenike();
            return ucenici;
        }

        // GET api/Ucenik/id
        public UcenikView Get(int id)
        {
            DataProvider provider = new DataProvider();
            return provider.vratiUcenikView(id);
        }

        // POST api/Ucenik
        public int Post([FromBody]Ucenik ad)
        {
            DataProvider provider = new DataProvider();
            return provider.dodajUcenika(ad);
        }

        // PUT api/Ucenik/id
        public int Put(int id)
        {
            DataProvider provider = new DataProvider();
            return provider.azurirajUcenika(id);
        }

        // DELETE api/Ucenik/id
        public int Delete(int id)
        {
            DataProvider provider = new DataProvider();
            return provider.ukloniUcenika(id);
        }
    }
}